import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './modulos/Dashboard/dashboard/dashboard.component';
import { IconosComponent } from './modulos/nomenclaturaGrafica/iconos/iconos.component';
import { PdfComponent } from './modulos/nomenclaturaGrafica/pdf/pdf.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatPaginatorModule} from '@angular/material/paginator';
import { Error404Component } from './modulos/error404/error404.component';
import {SafePipe} from './safe.pipe';
import {LoginComponent} from './login/login.component';
import {TiModule} from './modulos/ti/ti.module';
import { ActualizacionesComponent } from './modulos/actualizaciones/actualizaciones.component';
import {RecursosHumanosModule} from './modulos/recursos-humanos/recursos-humanos.module';
import {SoporteModule} from './modulos/soporte/soporte.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCardModule} from '@angular/material/card';
import {MatSortModule} from '@angular/material/sort';
import { EjemplosComponent } from './Ejemplos-de-Vista/ejemplos/ejemplos.component';
import { TableActionsSheetComponent } from './modulos/nomenclaturaGrafica/table-actions-sheet/table-actions-sheet.component';
import {MatLineModule} from '@angular/material/core';
import {MatListModule} from '@angular/material/list';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MensajesComponent } from './modulos/mensajes/mensajes.component';
import {_MatMenuDirectivesModule, MatMenuModule} from "@angular/material/menu";
import { MensajesTablaComponent } from './modulos/mensajes/mensajes-tabla/mensajes-tabla.component';
import { ModuloLlamadasComponent } from './modulos/modulo-llamadas/modulo-llamadas.component';
import {MatExpansionModule} from "@angular/material/expansion";
import { LoaderComponent } from './loader/loader.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    DashboardComponent,
    AppComponent,
    IconosComponent,
    PdfComponent,
    Error404Component,
    LoginComponent,
    SafePipe,
    AppComponent,
    PdfComponent,
    ActualizacionesComponent,
    EjemplosComponent,
    TableActionsSheetComponent,
    ToolbarComponent,
    MensajesComponent,
    MensajesTablaComponent,
    ModuloLlamadasComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTableModule,
    MatInputModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatCardModule,
    MatSortModule,
    MatLineModule,
    MatListModule,
    _MatMenuDirectivesModule,
    MatMenuModule,
    MatExpansionModule,
    HttpClientModule,
  ],
  providers: [
    RecursosHumanosModule,
    TiModule,
    SoporteModule,
    MatBottomSheet,
  ],
  exports: [
    ToolbarComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
