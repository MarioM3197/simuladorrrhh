import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {AuthService} from '../@core/AuthService';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  userSessionStorage = 'Usuario Session';
  public loaderUp = false;
  public menuOpen = false;
  public carouselLeft = 0;
  public themeDark = false; // VARIABLE PARA EL DARK MODE
  public fontSize = 'font-size-2';
  public cardDisplay = true;
  public lector = false;
  public myEmailModel: any;
  public menuPrincipal = false;
  public isLoggedIn: Observable<boolean>;

  constructor(
    private router: Router,
    private authService: AuthService,
  ) {}

  ngOnInit(): void {
    // setTimeout (this.removeUploader(), 200);
    this.isLoggedIn = this.authService.isLoggedIn;
    this.myEmailModel = 'patito@duckmail.com';
  }

  toggleMenu() {
    this.menuOpen = !this.menuOpen;
    const sections = document.querySelectorAll('.subMenu-activo');
    for (let i = 0; i < sections.length; i++) {
      sections[i].classList.remove('subMenu-activo');
    }
  }

  openHijo(event) {
    if (event.target.classList.value === 'elementos-del-menu-1') {
      const sectionsfather = document.querySelectorAll('.menuPrincipal .elementos-del-menu-1');
      for (let i = 0; i < sectionsfather.length; i++) {
        sectionsfather[i].classList.remove('subMenu-activo');
      }
    }
    const sections = document.querySelectorAll('.menu-hijo .subMenu-activo');
    for (let i = 0; i < sections.length; i++) {
      sections[i].classList.remove('subMenu-activo');
    }
    event.target.classList.toggle('subMenu-activo');
  }

  logOut() {
    Swal.fire({
      title: '¿Desea cerrar sesión?',
      text: '',
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true,
      cancelButtonColor: '#C33764',
      cancelButtonText: 'No',
      confirmButtonColor: '#1D2671',
      confirmButtonText: 'Si',
      allowOutsideClick: false,
      allowEscapeKey: false,
    }).then(tkn => {
      if (tkn.value) {
        Swal.fire({
          title: 'Sesión cerrada',
          text: '',
          icon: 'success',
          confirmButtonColor: '#1D2671',
          confirmButtonText: 'Ok',
          allowOutsideClick: false,
          allowEscapeKey: false,
        }).then(resp => {
          if (resp.value) {
            this.authService.logout();
            this.router.navigate(['login']);
          }
        });
      }
    });
  }

  carouselMove(paso: number) {
    console.log(document.querySelectorAll('.caroussel-element').length);
    if (this.carouselLeft * (paso - 1) >= -1 * (256 * ((document.querySelectorAll('.caroussel-element').length)))) {
      this.carouselLeft = -1 * (256 * (paso - 1));
    }
  }

  activarHablar() {
    this.lector = !this.lector;
    if (this.lector) {
      window.addEventListener('mouseup', this.texToSpeach);
    } else {
    }
  }

  texToSpeach = () => {
    if (this.lector && window.speechSynthesis && window.getSelection()) {
      const availableVoices = window.speechSynthesis.getVoices();
      let spanishVoice: any;

      // find voice by language locale "en-US"
      // if not then select the first voice
      for (let i = 0; i < availableVoices.length; i++) {
        if (availableVoices[i].lang === 'es-MX') {
          spanishVoice = availableVoices[i];
          break;
        }
      }
      if (spanishVoice === null) {
        spanishVoice = availableVoices[0];
      }
      window.speechSynthesis.cancel();
      const boquita = new SpeechSynthesisUtterance();
      boquita.rate = 1;
      boquita.pitch = 1;
      boquita.voice = spanishVoice;
      boquita.text = window.getSelection().toString();
      window.speechSynthesis.speak(boquita);
    }
  }
}
