import {Component, OnInit} from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-mensajes',
  templateUrl: './mensajes.component.html',
  styleUrls: ['./mensajes.component.scss']
})
export class MensajesComponent implements OnInit {
  estadoContacto = 1;
  nombreEstado: string;
  intervalId;
  counter = 30;
  enviado = false;
  recibido = false;
  visto = true;

  constructor() {
  }
  ngOnInit() {
    this.activeInavctiveContact();
    this.estadoMensaje();
  }
  actionsToRecorder() {
    document.getElementById('microphone').style.display = 'none';
    document.getElementById('recordOptions').style.display = 'inline-flex';

    this.intervalId = setInterval(() => {
      this.counter = this.counter - 1;
      if (this.counter === 0) {
        clearInterval(this.intervalId);
        this.counter = 30;
        document.getElementById('microphone').style.display = 'block';
        document.getElementById('recordOptions').style.display = 'none';
      }
    }, 1000);
  }

  cancelAudio() {
    clearInterval(this.intervalId);
    document.getElementById('microphone').style.display = 'block';
    document.getElementById('recordOptions').style.display = 'none';
    this.counter = 30;
  }

  sendAudio() {
    clearInterval(this.intervalId);
    document.getElementById('microphone').style.display = 'block';
    document.getElementById('recordOptions').style.display = 'none';
    this.counter = 30;
  }

  // AREA DE CONTACTOS
  activeInavctiveContact() {
    if (this.estadoContacto === 1) {
      this.nombreEstado = 'En línea';
      document.getElementById('statusBallId').style.backgroundColor = '#a7e775';
    } else if (this.estadoContacto === 0) {
      this.nombreEstado = 'Inactivo';
      document.getElementById('statusBallId').style.backgroundColor = '#e77575';
    }
  }

  estadoMensaje() {
    if (this.enviado === true) {
      document.getElementById('estados1').style.display = 'inline-flex';
    } else if (this.recibido === true) {
      document.getElementById('estados2').style.display = 'inline-flex';
    } else if (this.visto === true) {
      document.getElementById('estados3').style.display = 'inline-flex';
    } else {
      document.getElementById('esperandoRed').style.display = 'inline-flex';
    }
  }
}
