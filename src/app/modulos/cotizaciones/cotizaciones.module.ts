import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CotizacionesRoutingModule } from './cotizaciones-routing.module';
import { CotizacionesComponent } from './cotizaciones.component';
import { Ejemplo1Component } from './ejemplo1/ejemplo1.component';


@NgModule({
  declarations: [CotizacionesComponent,
    Ejemplo1Component

  ],
  imports: [
    CommonModule,
    CotizacionesRoutingModule
  ]
})
export class CotizacionesModule { }
