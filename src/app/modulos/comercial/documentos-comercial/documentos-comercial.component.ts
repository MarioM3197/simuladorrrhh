import { Component, OnInit } from '@angular/core';
import {DocumentosComercialService} from '../../../@core/data/services/comerciales/documentos-comercial.service';

@Component({
  selector: 'app-documentos-comercial',
  templateUrl: './documentos-comercial.component.html',
  styleUrls: ['./documentos-comercial.component.scss']
})
export class DocumentosComercialComponent implements OnInit {

  constructor(private _DocumentosComercialService: DocumentosComercialService) { }

  ngOnInit(): void {
  }

}
