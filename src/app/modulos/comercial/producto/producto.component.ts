import { Component, OnInit } from '@angular/core';
import {ProductoSociosService} from '../../../@core/data/services/comerciales/producto-socios.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent implements OnInit {

  constructor(private _ProductoSociosService: ProductoSociosService) { }

  ngOnInit(): void {
  }

}
