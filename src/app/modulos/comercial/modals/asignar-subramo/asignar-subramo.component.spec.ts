import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarSubramoComponent } from './asignar-subramo.component';

describe('AsignarSubramoComponent', () => {
  let component: AsignarSubramoComponent;
  let fixture: ComponentFixture<AsignarSubramoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarSubramoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarSubramoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
