import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosRamoComponent } from './datos-ramo.component';

describe('DatosRamoComponent', () => {
  let component: DatosRamoComponent;
  let fixture: ComponentFixture<DatosRamoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosRamoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosRamoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
