import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarAnalisisCompetenciaComponent } from './asignar-analisis-competencia.component';

describe('AsignarAnalisisCompetenciaComponent', () => {
  let component: AsignarAnalisisCompetenciaComponent;
  let fixture: ComponentFixture<AsignarAnalisisCompetenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarAnalisisCompetenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarAnalisisCompetenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
