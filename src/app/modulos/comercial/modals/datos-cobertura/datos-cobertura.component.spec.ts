import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosCoberturaComponent } from './datos-cobertura.component';

describe('DatosCoberturaComponent', () => {
  let component: DatosCoberturaComponent;
  let fixture: ComponentFixture<DatosCoberturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosCoberturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosCoberturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
