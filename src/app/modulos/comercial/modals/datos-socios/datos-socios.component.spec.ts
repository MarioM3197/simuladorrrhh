import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosSociosComponent } from './datos-socios.component';

describe('DatosSociosComponent', () => {
  let component: DatosSociosComponent;
  let fixture: ComponentFixture<DatosSociosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosSociosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosSociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
