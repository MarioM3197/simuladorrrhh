import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosSubramoComponent } from './datos-subramo.component';

describe('DatosSubramoComponent', () => {
  let component: DatosSubramoComponent;
  let fixture: ComponentFixture<DatosSubramoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosSubramoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosSubramoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
