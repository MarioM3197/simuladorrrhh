import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosAnalisisCompetenciaComponent } from './datos-analisis-competencia.component';

describe('DatosAnalisisCompetenciaComponent', () => {
  let component: DatosAnalisisCompetenciaComponent;
  let fixture: ComponentFixture<DatosAnalisisCompetenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosAnalisisCompetenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosAnalisisCompetenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
