import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarCompetenciaComponent } from './asignar-competencia.component';

describe('AsignarCompetenciaComponent', () => {
  let component: AsignarCompetenciaComponent;
  let fixture: ComponentFixture<AsignarCompetenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarCompetenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarCompetenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
