import { Component, OnInit } from '@angular/core';
import {EjecutivoCuentaService} from '../../../@core/data/services/comerciales/ejecutivo-cuenta.service';

@Component({
  selector: 'app-ejecutivo-cuenta',
  templateUrl: './ejecutivo-cuenta.component.html',
  styleUrls: ['./ejecutivo-cuenta.component.scss']
})
export class EjecutivoCuentaComponent implements OnInit {

  constructor(private _EjecutivoCuentaService: EjecutivoCuentaService) { }

  ngOnInit(): void {
  }

}
