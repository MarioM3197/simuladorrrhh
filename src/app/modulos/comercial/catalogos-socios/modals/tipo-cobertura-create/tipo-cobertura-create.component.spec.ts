import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoCoberturaCreateComponent } from './tipo-cobertura-create.component';

describe('TipoCoberturaCreateComponent', () => {
  let component: TipoCoberturaCreateComponent;
  let fixture: ComponentFixture<TipoCoberturaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoCoberturaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoCoberturaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
