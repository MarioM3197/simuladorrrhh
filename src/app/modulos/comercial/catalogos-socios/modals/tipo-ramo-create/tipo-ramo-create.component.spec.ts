import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoRamoCreateComponent } from './tipo-ramo-create.component';

describe('TipoRamoCreateComponent', () => {
  let component: TipoRamoCreateComponent;
  let fixture: ComponentFixture<TipoRamoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoRamoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoRamoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
