import { Component, OnInit } from '@angular/core';
import {TipoSubramoService} from '../../../../@core/data/services/comerciales/catalogo/tipo-subramo.service';

@Component({
  selector: 'app-tipo-subramo',
  templateUrl: './tipo-subramo.component.html',
  styleUrls: ['./tipo-subramo.component.scss']
})
export class TipoSubramoComponent implements OnInit {

  constructor(private _TipoSubramoService: TipoSubramoService) { }

  ngOnInit(): void {
  }

}
