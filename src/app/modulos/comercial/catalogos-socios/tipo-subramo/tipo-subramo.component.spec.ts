import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoSubramoComponent } from './tipo-subramo.component';

describe('TipoSubramoComponent', () => {
  let component: TipoSubramoComponent;
  let fixture: ComponentFixture<TipoSubramoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoSubramoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoSubramoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
