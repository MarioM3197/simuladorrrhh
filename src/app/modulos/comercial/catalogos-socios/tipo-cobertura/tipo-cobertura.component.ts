import { Component, OnInit } from '@angular/core';
import {TipoCoberturaService} from '../../../../@core/data/services/comerciales/catalogo/tipo-cobertura.service';

@Component({
  selector: 'app-tipo-cobertura',
  templateUrl: './tipo-cobertura.component.html',
  styleUrls: ['./tipo-cobertura.component.scss']
})
export class TipoCoberturaComponent implements OnInit {

  constructor(private _TipoCoberturaService: TipoCoberturaService) { }

  ngOnInit(): void {
  }

}
