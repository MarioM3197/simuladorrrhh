import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogosSociosRoutingModule } from './catalogos-socios-routing.module';
import { CatalogosSociosComponent } from './catalogos-socios.component';
import { BajaSociosComponent } from './baja-socios/baja-socios.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { DivisasComponent } from './divisas/divisas.component';
import { SubCategoriaComponent } from './sub-categoria/sub-categoria.component';
import { TipoCategoriaComponent } from './tipo-categoria/tipo-categoria.component';
import { TipoCoberturaComponent } from './tipo-cobertura/tipo-cobertura.component';
import { TipoCompetenciaComponent } from './tipo-competencia/tipo-competencia.component';
import { TipoProductoComponent } from './tipo-producto/tipo-producto.component';
import { TipoRamoComponent } from './tipo-ramo/tipo-ramo.component';
import { TipoSubramoComponent } from './tipo-subramo/tipo-subramo.component';
import { CategoriaCreateComponent } from './modals/categoria-create/categoria-create.component';
import { DivisaCreateComponent } from './modals/divisa-create/divisa-create.component';
import { SubcategoriaCreateComponent } from './modals/subcategoria-create/subcategoria-create.component';
import { TipoBajaSociosCreateComponent } from './modals/tipo-baja-socios-create/tipo-baja-socios-create.component';
import { TipoCategoriaCreateComponent } from './modals/tipo-categoria-create/tipo-categoria-create.component';
import { TipoCoberturaCreateComponent } from './modals/tipo-cobertura-create/tipo-cobertura-create.component';
import { TipoCompetenciaCreateComponent } from './modals/tipo-competencia-create/tipo-competencia-create.component';
import { TipoProductoCreateComponent } from './modals/tipo-producto-create/tipo-producto-create.component';
import { TipoRamoCreateComponent } from './modals/tipo-ramo-create/tipo-ramo-create.component';
import { TipoSubramoCreateComponent } from './modals/tipo-subramo-create/tipo-subramo-create.component';
import {SociosBajaService} from '../../../@core/data/services/comerciales/catalogo/socios-baja.service';
import {CategoriaService} from '../../../@core/data/services/comerciales/catalogo/categoria.service';
import {DivisasService} from '../../../@core/data/services/comerciales/catalogo/divisas.service';
import {SubCategoriaService} from '../../../@core/data/services/comerciales/catalogo/sub-categoria.service';
import {TipoCategoriaService} from '../../../@core/data/services/comerciales/catalogo/tipo-categoria.service';
import {TipoCoberturaService} from '../../../@core/data/services/comerciales/catalogo/tipo-cobertura.service';
import {TipoCompetenciaService} from '../../../@core/data/services/comerciales/catalogo/tipo-competencia.service';
import {TipoProductoService} from '../../../@core/data/services/comerciales/catalogo/tipo-producto.service';
import {TipoRamoService} from '../../../@core/data/services/comerciales/catalogo/tipo-ramo.service';
import {TipoSubramoService} from '../../../@core/data/services/comerciales/catalogo/tipo-subramo.service';


@NgModule({
  declarations: [
    CatalogosSociosComponent,
    BajaSociosComponent,
    CategoriaComponent,
    DivisasComponent,
    SubCategoriaComponent,
    TipoCategoriaComponent,
    TipoCoberturaComponent,
    TipoCompetenciaComponent,
    TipoProductoComponent,
    TipoRamoComponent,
    TipoSubramoComponent,
    CategoriaCreateComponent,
    DivisaCreateComponent,
    SubcategoriaCreateComponent,
    TipoBajaSociosCreateComponent,
    TipoCategoriaCreateComponent,
    TipoCoberturaCreateComponent,
    TipoCompetenciaCreateComponent,
    TipoProductoCreateComponent,
    TipoRamoCreateComponent,
    TipoSubramoCreateComponent

  ],
  imports: [
    CommonModule,
    CatalogosSociosRoutingModule
  ],

  providers: [
    SociosBajaService,
    CategoriaService,
    DivisasService,
    SubCategoriaService,
    TipoCategoriaService,
    TipoCoberturaService,
    TipoCompetenciaService,
    TipoProductoService,
    TipoRamoService,
    TipoSubramoService

  ],
})
export class CatalogosSociosModule { }
