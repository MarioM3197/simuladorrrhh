import { Component, OnInit } from '@angular/core';
import {TipoProductoService} from '../../../../@core/data/services/comerciales/catalogo/tipo-producto.service';

@Component({
  selector: 'app-tipo-producto',
  templateUrl: './tipo-producto.component.html',
  styleUrls: ['./tipo-producto.component.scss']
})
export class TipoProductoComponent implements OnInit {

  constructor(private _TipoProductoService: TipoProductoService) { }

  ngOnInit(): void {
  }

}
