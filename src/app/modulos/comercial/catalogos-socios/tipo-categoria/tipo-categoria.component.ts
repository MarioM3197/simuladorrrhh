import { Component, OnInit } from '@angular/core';
import {TipoCategoriaService} from '../../../../@core/data/services/comerciales/catalogo/tipo-categoria.service';

@Component({
  selector: 'app-tipo-categoria',
  templateUrl: './tipo-categoria.component.html',
  styleUrls: ['./tipo-categoria.component.scss']
})
export class TipoCategoriaComponent implements OnInit {

  constructor(private _TipoCategoriaService: TipoCategoriaService) { }

  ngOnInit(): void {
  }

}
