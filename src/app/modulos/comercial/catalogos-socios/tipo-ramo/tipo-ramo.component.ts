import { Component, OnInit } from '@angular/core';
import {TipoRamoService} from '../../../../@core/data/services/comerciales/catalogo/tipo-ramo.service';

@Component({
  selector: 'app-tipo-ramo',
  templateUrl: './tipo-ramo.component.html',
  styleUrls: ['./tipo-ramo.component.scss']
})
export class TipoRamoComponent implements OnInit {

  constructor(private _TipoRamoService: TipoRamoService) { }

  ngOnInit(): void {
  }

}
