import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoRamoComponent } from './tipo-ramo.component';

describe('TipoRamoComponent', () => {
  let component: TipoRamoComponent;
  let fixture: ComponentFixture<TipoRamoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoRamoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoRamoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
