import { Component, OnInit } from '@angular/core';
import {SubCategoriaService} from '../../../../@core/data/services/comerciales/catalogo/sub-categoria.service';

@Component({
  selector: 'app-sub-categoria',
  templateUrl: './sub-categoria.component.html',
  styleUrls: ['./sub-categoria.component.scss']
})
export class SubCategoriaComponent implements OnInit {

  constructor(private _SubCategoriaService: SubCategoriaService) { }

  ngOnInit(): void {
  }

}
