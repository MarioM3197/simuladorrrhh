import { Component, OnInit } from '@angular/core';
import {SociosBajaService} from '../../../../@core/data/services/comerciales/catalogo/socios-baja.service';

@Component({
  selector: 'app-baja-socios',
  templateUrl: './baja-socios.component.html',
  styleUrls: ['./baja-socios.component.scss']
})
export class BajaSociosComponent implements OnInit {

  constructor(private _SociosBajaService: SociosBajaService) { }

  ngOnInit(): void {
  }

}
