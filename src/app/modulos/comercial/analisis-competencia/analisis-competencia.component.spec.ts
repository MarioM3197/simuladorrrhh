import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalisisCompetenciaComponent } from './analisis-competencia.component';

describe('AnalisisCompetenciaComponent', () => {
  let component: AnalisisCompetenciaComponent;
  let fixture: ComponentFixture<AnalisisCompetenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalisisCompetenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalisisCompetenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
