import { Component, OnInit } from '@angular/core';
import {PresupuestoService} from '../../../@core/data/services/comerciales/presupuesto.service';

@Component({
  selector: 'app-presupuesto',
  templateUrl: './presupuesto.component.html',
  styleUrls: ['./presupuesto.component.scss']
})
export class PresupuestoComponent implements OnInit {

  constructor(private _PresupuestoService: PresupuestoService) { }

  ngOnInit(): void {
  }

}
