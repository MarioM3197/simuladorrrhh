import { Component, OnInit } from '@angular/core';
import {ContratosService} from '../../../@core/data/services/comerciales/contratos.service';

@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.scss']
})
export class ContratosComponent implements OnInit {

  constructor(private _ContratosService: ContratosService) { }

  ngOnInit(): void {
  }

}
