import { Component, OnInit } from '@angular/core';
import {AsignarUrlService} from '../../../@core/data/services/comerciales/asignar-url.service';

@Component({
  selector: 'app-asignar-url',
  templateUrl: './asignar-url.component.html',
  styleUrls: ['./asignar-url.component.scss']
})
export class AsignarUrlComponent implements OnInit {

  constructor(private _AsignarUrlService: AsignarUrlService) { }

  ngOnInit(): void {
  }

}
