import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesPolizasComponent } from './detalles-polizas.component';

describe('DetallesPolizasComponent', () => {
  let component: DetallesPolizasComponent;
  let fixture: ComponentFixture<DetallesPolizasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesPolizasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesPolizasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
