import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoSiniestroCreateComponent } from './tipo-siniestro-create.component';

describe('TipoSiniestroCreateComponent', () => {
  let component: TipoSiniestroCreateComponent;
  let fixture: ComponentFixture<TipoSiniestroCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoSiniestroCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoSiniestroCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
