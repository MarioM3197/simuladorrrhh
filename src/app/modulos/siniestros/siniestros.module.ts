import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SiniestrosRoutingModule } from './siniestros-routing.module';
import { SiniestrosComponent } from './siniestros.component';
import { AdministracionPolizasComponent } from './administracion-polizas/administracion-polizas.component';
import { TipoSiniestroCreateComponent } from './catalogos/modals/tipo-siniestro-create/tipo-siniestro-create.component';
import { TipoSiniestroComponent } from './catalogos/tipo-siniestro/tipo-siniestro.component';
import { DetallesPolizasComponent } from './modals/detalles-polizas/detalles-polizas.component';
import { SiniestroCreateComponent } from './modals/siniestro-create/siniestro-create.component';
import { SiniestroComponent } from './siniestro/siniestro.component';


@NgModule({
  declarations: [SiniestrosComponent, AdministracionPolizasComponent, TipoSiniestroCreateComponent, TipoSiniestroComponent, DetallesPolizasComponent, SiniestroCreateComponent, SiniestroComponent],
  imports: [
    CommonModule,
    SiniestrosRoutingModule
  ]
})
export class SiniestrosModule { }
