import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RenovacionesRoutingModule } from './renovaciones-routing.module';
import { RenovacionesComponent } from './renovaciones.component';


@NgModule({
  declarations: [RenovacionesComponent],
  imports: [
    CommonModule,
    RenovacionesRoutingModule
  ]
})
export class RenovacionesModule { }
