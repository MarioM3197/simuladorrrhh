import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenTicketsComponent } from './resumen-tickets.component';

describe('ResumenTicketsComponent', () => {
  let component: ResumenTicketsComponent;
  let fixture: ComponentFixture<ResumenTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
