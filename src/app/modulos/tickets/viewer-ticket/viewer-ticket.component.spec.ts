import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewerTicketComponent } from './viewer-ticket.component';

describe('ViewerTicketComponent', () => {
  let component: ViewerTicketComponent;
  let fixture: ComponentFixture<ViewerTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewerTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewerTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
