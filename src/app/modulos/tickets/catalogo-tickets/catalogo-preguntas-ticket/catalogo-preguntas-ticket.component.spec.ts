import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoPreguntasTicketComponent } from './catalogo-preguntas-ticket.component';

describe('CatalogoPreguntasTicketComponent', () => {
  let component: CatalogoPreguntasTicketComponent;
  let fixture: ComponentFixture<CatalogoPreguntasTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoPreguntasTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoPreguntasTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
