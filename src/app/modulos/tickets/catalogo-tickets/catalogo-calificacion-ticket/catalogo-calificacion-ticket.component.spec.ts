import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoCalificacionTicketComponent } from './catalogo-calificacion-ticket.component';

describe('CatalogoCalificacionTicketComponent', () => {
  let component: CatalogoCalificacionTicketComponent;
  let fixture: ComponentFixture<CatalogoCalificacionTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoCalificacionTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoCalificacionTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
