import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMotivoComponent } from './create-motivo.component';

describe('CreateMotivoComponent', () => {
  let component: CreateMotivoComponent;
  let fixture: ComponentFixture<CreateMotivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMotivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMotivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
