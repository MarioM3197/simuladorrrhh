import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePrioridadComponent } from './create-prioridad.component';

describe('CreatePrioridadComponent', () => {
  let component: CreatePrioridadComponent;
  let fixture: ComponentFixture<CreatePrioridadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePrioridadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePrioridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
