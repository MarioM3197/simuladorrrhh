import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEstadoPreguntaComponent } from './create-estado-pregunta.component';

describe('CreateEstadoPreguntaComponent', () => {
  let component: CreateEstadoPreguntaComponent;
  let fixture: ComponentFixture<CreateEstadoPreguntaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEstadoPreguntaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEstadoPreguntaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
