import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasignarTicketComponent } from './reasignar-ticket.component';

describe('ReasignarTicketComponent', () => {
  let component: ReasignarTicketComponent;
  let fixture: ComponentFixture<ReasignarTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReasignarTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasignarTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
