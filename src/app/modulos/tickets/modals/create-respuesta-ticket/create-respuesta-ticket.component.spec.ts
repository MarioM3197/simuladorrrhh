import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRespuestaTicketComponent } from './create-respuesta-ticket.component';

describe('CreateRespuestaTicketComponent', () => {
  let component: CreateRespuestaTicketComponent;
  let fixture: ComponentFixture<CreateRespuestaTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRespuestaTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRespuestaTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
