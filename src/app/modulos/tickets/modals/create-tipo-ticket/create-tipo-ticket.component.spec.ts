import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTipoTicketComponent } from './create-tipo-ticket.component';

describe('CreateTipoTicketComponent', () => {
  let component: CreateTipoTicketComponent;
  let fixture: ComponentFixture<CreateTipoTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTipoTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTipoTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
