import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCalificacionComponent } from './create-calificacion.component';

describe('CreateCalificacionComponent', () => {
  let component: CreateCalificacionComponent;
  let fixture: ComponentFixture<CreateCalificacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCalificacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCalificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
