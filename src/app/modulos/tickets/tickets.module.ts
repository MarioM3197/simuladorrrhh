import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TicketsRoutingModule } from './tickets-routing.module';
import { TicketsComponent } from './tickets.component';
import { ActivacionEmpleadosComponent } from './activacion-empleados/activacion-empleados.component';
import { AdminsitracionTicketsComponent } from './adminsitracion-tickets/adminsitracion-tickets.component';
import { PreguntasFrecuentesComponent } from './preguntas-frecuentes/preguntas-frecuentes.component';
import { ResumenTicketsComponent } from './resumen-tickets/resumen-tickets.component';
import { TicketsAbiertosComponent } from './tickets-abiertos/tickets-abiertos.component';
import { TicketsCerradosComponent } from './tickets-cerrados/tickets-cerrados.component';
import { ViewerTicketComponent } from './viewer-ticket/viewer-ticket.component';
import { CatalogoCalificacionTicketComponent } from './catalogo-tickets/catalogo-calificacion-ticket/catalogo-calificacion-ticket.component';
import { CatalogoEstadoPreguntaComponent } from './catalogo-tickets/catalogo-estado-pregunta/catalogo-estado-pregunta.component';
import { CatalogoEstadoTicketComponent } from './catalogo-tickets/catalogo-estado-ticket/catalogo-estado-ticket.component';
import { CatalogoMotivoTicketComponent } from './catalogo-tickets/catalogo-motivo-ticket/catalogo-motivo-ticket.component';
import { CatalogoOpcionTicketComponent } from './catalogo-tickets/catalogo-opcion-ticket/catalogo-opcion-ticket.component';
import { CatalogoPreguntasTicketComponent } from './catalogo-tickets/catalogo-preguntas-ticket/catalogo-preguntas-ticket.component';
import { CatalogoPrioridadTicketComponent } from './catalogo-tickets/catalogo-prioridad-ticket/catalogo-prioridad-ticket.component';
import { CatalogoRespuestaTicketComponent } from './catalogo-tickets/catalogo-respuesta-ticket/catalogo-respuesta-ticket.component';
import { CatalogoTipoTicketComponent } from './catalogo-tickets/catalogo-tipo-ticket/catalogo-tipo-ticket.component';
import { CalificarTicketComponent } from './modals/calificar-ticket/calificar-ticket.component';
import { CreateCalificacionComponent } from './modals/create-calificacion/create-calificacion.component';
import { CreateEstadoComponent } from './modals/create-estado/create-estado.component';
import { CreateEstadoPreguntaComponent } from './modals/create-estado-pregunta/create-estado-pregunta.component';
import { CreateMotivoComponent } from './modals/create-motivo/create-motivo.component';
import { CreateOpcionComponent } from './modals/create-opcion/create-opcion.component';
import { CreatePreguntaTicketComponent } from './modals/create-pregunta-ticket/create-pregunta-ticket.component';
import { CreatePrioridadComponent } from './modals/create-prioridad/create-prioridad.component';
import { CreateRespuestaTicketComponent } from './modals/create-respuesta-ticket/create-respuesta-ticket.component';
import { CreateTipoTicketComponent } from './modals/create-tipo-ticket/create-tipo-ticket.component';
import { EditarTicketComponent } from './modals/editar-ticket/editar-ticket.component';
import { ReasignarTicketComponent } from './modals/reasignar-ticket/reasignar-ticket.component';


@NgModule({
  declarations: [
    TicketsComponent,
    ActivacionEmpleadosComponent,
    AdminsitracionTicketsComponent,
    PreguntasFrecuentesComponent,
    ResumenTicketsComponent,
    TicketsAbiertosComponent,
    TicketsCerradosComponent,
    ViewerTicketComponent,
    CatalogoCalificacionTicketComponent,
    CatalogoEstadoPreguntaComponent,
    CatalogoEstadoTicketComponent,
    CatalogoMotivoTicketComponent,
    CatalogoOpcionTicketComponent,
    CatalogoPreguntasTicketComponent,
    CatalogoPrioridadTicketComponent,
    CatalogoRespuestaTicketComponent,
    CatalogoTipoTicketComponent,
    CalificarTicketComponent,
    CreateCalificacionComponent,
    CreateEstadoComponent,
    CreateEstadoPreguntaComponent,
    CreateMotivoComponent,
    CreateOpcionComponent,
    CreatePreguntaTicketComponent,
    CreatePrioridadComponent,
    CreateRespuestaTicketComponent,
    CreateTipoTicketComponent,
    EditarTicketComponent,
    ReasignarTicketComponent
  ],
  imports: [
    CommonModule,
    TicketsRoutingModule
  ]
})
export class TicketsModule { }
