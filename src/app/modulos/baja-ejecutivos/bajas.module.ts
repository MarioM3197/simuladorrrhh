import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BajasRoutingModule } from './bajas-routing.module';
import { BajasComponent } from './bajas.component';
import { PreEjecutivosComponent } from './modals/pre-ejecutivos/pre-ejecutivos.component';
import { RegistroPreBajaComponent } from './modals/registro-pre-baja/registro-pre-baja.component';


@NgModule({
  declarations: [
    BajasComponent,
    PreEjecutivosComponent,
    RegistroPreBajaComponent
  ],
  imports: [
    CommonModule,
    BajasRoutingModule
  ]
})
export class BajasModule { }
