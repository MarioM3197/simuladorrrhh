import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroPreBajaComponent } from './registro-pre-baja.component';

describe('RegistroPreBajaComponent', () => {
  let component: RegistroPreBajaComponent;
  let fixture: ComponentFixture<RegistroPreBajaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroPreBajaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroPreBajaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
