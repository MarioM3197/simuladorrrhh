import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearModoSeguroComponent } from './crear-modo-seguro.component';

describe('CrearModoSeguroComponent', () => {
  let component: CrearModoSeguroComponent;
  let fixture: ComponentFixture<CrearModoSeguroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearModoSeguroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearModoSeguroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
