import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarExtensionComponent } from './modificar-extension.component';

describe('ModificarExtensionComponent', () => {
  let component: ModificarExtensionComponent;
  let fixture: ComponentFixture<ModificarExtensionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarExtensionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarExtensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
