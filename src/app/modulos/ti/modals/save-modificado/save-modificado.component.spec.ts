import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveModificadoComponent } from './save-modificado.component';

describe('SaveModificadoComponent', () => {
  let component: SaveModificadoComponent;
  let fixture: ComponentFixture<SaveModificadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveModificadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveModificadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
