import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearExtensionComponent } from './crear-extension.component';

describe('CrearExtensionComponent', () => {
  let component: CrearExtensionComponent;
  let fixture: ComponentFixture<CrearExtensionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearExtensionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearExtensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
