import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearCpuComponent } from './crear-cpu.component';

describe('CrearCpuComponent', () => {
  let component: CrearCpuComponent;
  let fixture: ComponentFixture<CrearCpuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearCpuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearCpuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
