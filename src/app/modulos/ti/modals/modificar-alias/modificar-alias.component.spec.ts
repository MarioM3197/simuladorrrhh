import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarAliasComponent } from './modificar-alias.component';

describe('ModificarAliasComponent', () => {
  let component: ModificarAliasComponent;
  let fixture: ComponentFixture<ModificarAliasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarAliasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarAliasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
