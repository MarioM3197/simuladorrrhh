import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearEjecutivoComponent } from './crear-ejecutivo.component';

describe('CrearEjecutivoComponent', () => {
  let component: CrearEjecutivoComponent;
  let fixture: ComponentFixture<CrearEjecutivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearEjecutivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearEjecutivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
