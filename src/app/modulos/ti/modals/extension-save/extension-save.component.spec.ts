import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtensionSaveComponent } from './extension-save.component';

describe('ExtensionSaveComponent', () => {
  let component: ExtensionSaveComponent;
  let fixture: ComponentFixture<ExtensionSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtensionSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtensionSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
