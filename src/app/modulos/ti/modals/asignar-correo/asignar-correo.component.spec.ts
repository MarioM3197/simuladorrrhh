import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarCorreoComponent } from './asignar-correo.component';

describe('AsignarCorreoComponent', () => {
  let component: AsignarCorreoComponent;
  let fixture: ComponentFixture<AsignarCorreoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarCorreoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarCorreoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
