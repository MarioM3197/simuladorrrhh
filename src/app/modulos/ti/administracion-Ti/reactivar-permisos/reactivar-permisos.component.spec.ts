import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactivarPermisosComponent } from './reactivar-permisos.component';

describe('ReactivarPermisosComponent', () => {
  let component: ReactivarPermisosComponent;
  let fixture: ComponentFixture<ReactivarPermisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactivarPermisosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactivarPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
