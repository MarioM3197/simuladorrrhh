import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizadorTabsComponent } from './cotizador-tabs.component';

describe('CotizadorTabsComponent', () => {
  let component: CotizadorTabsComponent;
  let fixture: ComponentFixture<CotizadorTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizadorTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizadorTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
