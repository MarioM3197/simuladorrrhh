import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepCotizadorComponent } from './step-cotizador.component';

describe('StepCotizadorComponent', () => {
  let component: StepCotizadorComponent;
  let fixture: ComponentFixture<StepCotizadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepCotizadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepCotizadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
