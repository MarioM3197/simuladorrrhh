import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CobranzaSubSecuentesComponent } from './cobranza-sub-secuentes.component';

describe('CobranzaSubSecuentesComponent', () => {
  let component: CobranzaSubSecuentesComponent;
  let fixture: ComponentFixture<CobranzaSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CobranzaSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CobranzaSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
