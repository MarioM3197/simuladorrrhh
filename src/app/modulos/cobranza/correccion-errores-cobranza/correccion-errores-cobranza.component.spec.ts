import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorreccionErroresCobranzaComponent } from './correccion-errores-cobranza.component';

describe('CorreccionErroresCobranzaComponent', () => {
  let component: CorreccionErroresCobranzaComponent;
  let fixture: ComponentFixture<CorreccionErroresCobranzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorreccionErroresCobranzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorreccionErroresCobranzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
