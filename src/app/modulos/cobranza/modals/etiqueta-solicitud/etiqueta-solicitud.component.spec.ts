import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtiquetaSolicitudComponent } from './etiqueta-solicitud.component';

describe('EtiquetaSolicitudComponent', () => {
  let component: EtiquetaSolicitudComponent;
  let fixture: ComponentFixture<EtiquetaSolicitudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtiquetaSolicitudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtiquetaSolicitudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
