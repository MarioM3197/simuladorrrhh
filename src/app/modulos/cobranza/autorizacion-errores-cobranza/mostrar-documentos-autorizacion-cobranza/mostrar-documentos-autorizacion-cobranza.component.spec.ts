import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarDocumentosAutorizacionCobranzaComponent } from './mostrar-documentos-autorizacion-cobranza.component';

describe('MostrarDocumentosAutorizacionCobranzaComponent', () => {
  let component: MostrarDocumentosAutorizacionCobranzaComponent;
  let fixture: ComponentFixture<MostrarDocumentosAutorizacionCobranzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarDocumentosAutorizacionCobranzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarDocumentosAutorizacionCobranzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
