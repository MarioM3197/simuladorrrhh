import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosCorreccionCobranzaComponent } from './documentos-correccion-cobranza.component';

describe('DocumentosCorreccionCobranzaComponent', () => {
  let component: DocumentosCorreccionCobranzaComponent;
  let fixture: ComponentFixture<DocumentosCorreccionCobranzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentosCorreccionCobranzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosCorreccionCobranzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
