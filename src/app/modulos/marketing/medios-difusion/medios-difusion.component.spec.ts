import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediosDifusionComponent } from './medios-difusion.component';

describe('MediosDifusionComponent', () => {
  let component: MediosDifusionComponent;
  let fixture: ComponentFixture<MediosDifusionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediosDifusionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediosDifusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
