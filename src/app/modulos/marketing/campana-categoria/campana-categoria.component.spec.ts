import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampanaCategoriaComponent } from './campana-categoria.component';

describe('CampanaCategoriaComponent', () => {
  let component: CampanaCategoriaComponent;
  let fixture: ComponentFixture<CampanaCategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampanaCategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampanaCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
