import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarketingRoutingModule } from './marketing-routing.module';
import { MarketingComponent } from './marketing.component';
import { AseguradorasComponent } from './aseguradoras/aseguradoras.component';
import { CampanaComponent } from './campana/campana.component';
import { CampanaCategoriaComponent } from './campana-categoria/campana-categoria.component';
import { CampanaSubareaComponent } from './campana-subarea/campana-subarea.component';
import { CostoCampanaComponent } from './costo-campana/costo-campana.component';
import { MediosDifusionComponent } from './medios-difusion/medios-difusion.component';
import { MetasCampanaComponent } from './metas-campana/metas-campana.component';
import { PaginaComponent } from './pagina/pagina.component';
import { PresupuesoCampanaComponent } from './presupueso-campana/presupueso-campana.component';
import { ActualizarCampanaComponent } from './modals/actualizar-campana/actualizar-campana.component';
import { ActualizarCampanaCategoriaComponent } from './modals/actualizar-campana-categoria/actualizar-campana-categoria.component';
import { ActualizarCamposNodoComponent } from './modals/actualizar-campos-nodo/actualizar-campos-nodo.component';
import { ActualizarMediosDifusionComponent } from './modals/actualizar-medios-difusion/actualizar-medios-difusion.component';
import { ActualizarPaginaComponent } from './modals/actualizar-pagina/actualizar-pagina.component';
import { AgregarAseguradoraComponent } from './modals/agregar-aseguradora/agregar-aseguradora.component';
import { AgregarCamposComponent } from './modals/agregar-campos/agregar-campos.component';
import { AgregarCamposNodoComponent } from './modals/agregar-campos-nodo/agregar-campos-nodo.component';
import { AgregarNodoComponent } from './modals/agregar-nodo/agregar-nodo.component';
import { AgregarTipoComponent } from './modals/agregar-tipo/agregar-tipo.component';
import { AsignarCategoriaCampanaComponent } from './modals/asignar-categoria-campana/asignar-categoria-campana.component';
import { AsignarMediosDifusionComponent } from './modals/asignar-medios-difusion/asignar-medios-difusion.component';
import { CampanaMarketingCreateComponent } from './modals/campana-marketing-create/campana-marketing-create.component';
import { CreateCampanaSubareaComponent } from './modals/create-campana-subarea/create-campana-subarea.component';
import { CreateCostoCampanaComponent } from './modals/create-costo-campana/create-costo-campana.component';
import { CreateMetasCampanaComponent } from './modals/create-metas-campana/create-metas-campana.component';
import { CreatePresupuestoCampanaComponent } from './modals/create-presupuesto-campana/create-presupuesto-campana.component';
import { DatosNodosComponent } from './modals/datos-nodos/datos-nodos.component';
import { GuardadoComponent } from './modals/guardado/guardado.component';
import { ModificarDatosComponent } from './modals/modificar-datos/modificar-datos.component';
import { ProveedoresLoadCreateComponent } from './modals/proveedores-load-create/proveedores-load-create.component';
import { VerConfiguracionComponent } from './modals/ver-configuracion/ver-configuracion.component';
import { ReporteCampanaComponent } from './reportes/reporte-campana/reporte-campana.component';


@NgModule({
  declarations: [
    MarketingComponent,
    AseguradorasComponent,
    CampanaComponent,
    CampanaCategoriaComponent,
    CampanaSubareaComponent,
    CostoCampanaComponent,
    MediosDifusionComponent,
    MetasCampanaComponent,
    PaginaComponent,
    PresupuesoCampanaComponent,
    ActualizarCampanaComponent,
    ActualizarCampanaCategoriaComponent,
    ActualizarCamposNodoComponent,
    ActualizarMediosDifusionComponent,
    ActualizarPaginaComponent,
    AgregarAseguradoraComponent,
    AgregarCamposComponent,
    AgregarCamposNodoComponent,
    AgregarNodoComponent,
    AgregarTipoComponent,
    AsignarCategoriaCampanaComponent,
    AsignarMediosDifusionComponent,
    CampanaMarketingCreateComponent,
    CreateCampanaSubareaComponent,
    CreateCostoCampanaComponent,
    CreateMetasCampanaComponent,
    CreatePresupuestoCampanaComponent,
    DatosNodosComponent,
    GuardadoComponent,
    ModificarDatosComponent,
    ProveedoresLoadCreateComponent,
    VerConfiguracionComponent,
    ReporteCampanaComponent
  ],
  imports: [
    CommonModule,
    MarketingRoutingModule
  ]
})
export class MarketingModule { }
