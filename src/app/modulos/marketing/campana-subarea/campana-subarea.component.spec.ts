import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampanaSubareaComponent } from './campana-subarea.component';

describe('CampanaSubareaComponent', () => {
  let component: CampanaSubareaComponent;
  let fixture: ComponentFixture<CampanaSubareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampanaSubareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampanaSubareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
