import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePresupuestoCampanaComponent } from './create-presupuesto-campana.component';

describe('CreatePresupuestoCampanaComponent', () => {
  let component: CreatePresupuestoCampanaComponent;
  let fixture: ComponentFixture<CreatePresupuestoCampanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePresupuestoCampanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePresupuestoCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
