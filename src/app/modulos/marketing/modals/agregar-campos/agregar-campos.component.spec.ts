import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarCamposComponent } from './agregar-campos.component';

describe('AgregarCamposComponent', () => {
  let component: AgregarCamposComponent;
  let fixture: ComponentFixture<AgregarCamposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarCamposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarCamposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
