import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarCamposNodoComponent } from './actualizar-campos-nodo.component';

describe('ActualizarCamposNodoComponent', () => {
  let component: ActualizarCamposNodoComponent;
  let fixture: ComponentFixture<ActualizarCamposNodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarCamposNodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarCamposNodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
