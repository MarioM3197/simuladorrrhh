import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarMediosDifusionComponent } from './asignar-medios-difusion.component';

describe('AsignarMediosDifusionComponent', () => {
  let component: AsignarMediosDifusionComponent;
  let fixture: ComponentFixture<AsignarMediosDifusionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarMediosDifusionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarMediosDifusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
