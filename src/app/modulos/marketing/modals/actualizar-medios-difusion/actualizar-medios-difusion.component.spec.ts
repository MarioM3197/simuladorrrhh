import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarMediosDifusionComponent } from './actualizar-medios-difusion.component';

describe('ActualizarMediosDifusionComponent', () => {
  let component: ActualizarMediosDifusionComponent;
  let fixture: ComponentFixture<ActualizarMediosDifusionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarMediosDifusionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarMediosDifusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
