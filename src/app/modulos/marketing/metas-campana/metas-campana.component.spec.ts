import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetasCampanaComponent } from './metas-campana.component';

describe('MetasCampanaComponent', () => {
  let component: MetasCampanaComponent;
  let fixture: ComponentFixture<MetasCampanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetasCampanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetasCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
