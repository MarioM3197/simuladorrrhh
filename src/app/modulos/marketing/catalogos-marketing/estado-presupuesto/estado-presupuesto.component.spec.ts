import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoPresupuestoComponent } from './estado-presupuesto.component';

describe('EstadoPresupuestoComponent', () => {
  let component: EstadoPresupuestoComponent;
  let fixture: ComponentFixture<EstadoPresupuestoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoPresupuestoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoPresupuestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
