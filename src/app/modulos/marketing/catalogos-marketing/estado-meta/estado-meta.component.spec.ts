import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoMetaComponent } from './estado-meta.component';

describe('EstadoMetaComponent', () => {
  let component: EstadoMetaComponent;
  let fixture: ComponentFixture<EstadoMetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoMetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoMetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
