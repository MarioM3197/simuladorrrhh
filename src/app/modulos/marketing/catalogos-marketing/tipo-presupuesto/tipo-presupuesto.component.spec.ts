import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPresupuestoComponent } from './tipo-presupuesto.component';

describe('TipoPresupuestoComponent', () => {
  let component: TipoPresupuestoComponent;
  let fixture: ComponentFixture<TipoPresupuestoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoPresupuestoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPresupuestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
