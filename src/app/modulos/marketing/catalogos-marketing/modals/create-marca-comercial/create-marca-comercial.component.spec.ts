import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMarcaComercialComponent } from './create-marca-comercial.component';

describe('CreateMarcaComercialComponent', () => {
  let component: CreateMarcaComercialComponent;
  let fixture: ComponentFixture<CreateMarcaComercialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMarcaComercialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMarcaComercialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
