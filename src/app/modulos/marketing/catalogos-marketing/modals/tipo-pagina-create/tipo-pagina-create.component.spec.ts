import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPaginaCreateComponent } from './tipo-pagina-create.component';

describe('TipoPaginaCreateComponent', () => {
  let component: TipoPaginaCreateComponent;
  let fixture: ComponentFixture<TipoPaginaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoPaginaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPaginaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
