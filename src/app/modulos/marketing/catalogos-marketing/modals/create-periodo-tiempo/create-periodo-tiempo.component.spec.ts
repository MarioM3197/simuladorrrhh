import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePeriodoTiempoComponent } from './create-periodo-tiempo.component';

describe('CreatePeriodoTiempoComponent', () => {
  let component: CreatePeriodoTiempoComponent;
  let fixture: ComponentFixture<CreatePeriodoTiempoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePeriodoTiempoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePeriodoTiempoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
