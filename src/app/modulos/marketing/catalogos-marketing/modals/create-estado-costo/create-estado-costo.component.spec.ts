import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEstadoCostoComponent } from './create-estado-costo.component';

describe('CreateEstadoCostoComponent', () => {
  let component: CreateEstadoCostoComponent;
  let fixture: ComponentFixture<CreateEstadoCostoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEstadoCostoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEstadoCostoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
