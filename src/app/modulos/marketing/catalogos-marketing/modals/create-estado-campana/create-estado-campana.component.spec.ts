import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEstadoCampanaComponent } from './create-estado-campana.component';

describe('CreateEstadoCampanaComponent', () => {
  let component: CreateEstadoCampanaComponent;
  let fixture: ComponentFixture<CreateEstadoCampanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEstadoCampanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEstadoCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
