import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProveedorLeadComponent } from './proveedor-lead.component';

describe('ProveedorLeadComponent', () => {
  let component: ProveedorLeadComponent;
  let fixture: ComponentFixture<ProveedorLeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProveedorLeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProveedorLeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
