import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoCostoComponent } from './tipo-costo.component';

describe('TipoCostoComponent', () => {
  let component: TipoCostoComponent;
  let fixture: ComponentFixture<TipoCostoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoCostoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoCostoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
