import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresupuestoMarketingComponent } from './presupuesto-marketing.component';

describe('PresupuestoMarketingComponent', () => {
  let component: PresupuestoMarketingComponent;
  let fixture: ComponentFixture<PresupuestoMarketingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresupuestoMarketingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresupuestoMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
