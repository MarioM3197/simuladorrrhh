import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcaComercialComponent } from './marca-comercial.component';

describe('MarcaComercialComponent', () => {
  let component: MarcaComercialComponent;
  let fixture: ComponentFixture<MarcaComercialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcaComercialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcaComercialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
