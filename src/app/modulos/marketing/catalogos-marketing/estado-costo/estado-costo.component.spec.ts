import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoCostoComponent } from './estado-costo.component';

describe('EstadoCostoComponent', () => {
  let component: EstadoCostoComponent;
  let fixture: ComponentFixture<EstadoCostoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoCostoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoCostoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
