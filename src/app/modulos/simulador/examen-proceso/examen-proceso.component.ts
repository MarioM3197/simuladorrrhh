import {Component, OnInit} from '@angular/core';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';

@Component({
    selector: 'app-examen-proceso',
    templateUrl: './examen-proceso.component.html',
    styleUrls: ['./examen-proceso.component.css']
})
export class ExamenProcesoComponent implements OnInit {
    preguntas = [
        {
            pregunta: 'Elije un color',
            opciones: [
                {
                    opcion: 'Rojo',
                    puntaje: 30,
                    correcta: true,
                    recomendacion: ''
                },
                {
                    opcion: 'Este casi es un color',
                    puntaje: 20,
                    correcta: false,
                    recomendacion: 'Casi es un color pero no lo es',
                },
                {
                    opcion: 'Gato',
                    puntaje: 0,
                    correcta: false,
                    recomendacion: 'Un gato definitivamente no es un color'
                }
            ]
        },
        {
            pregunta: 'Elije un color otra vez',
            opciones: [
                {
                    opcion: 'Azul',
                    puntaje: 30,
                    correcta: true,
                    recomendacion: ''
                },
                {
                    opcion: 'Esta no es la correcta',
                    puntaje: 20,
                    correcta: false,
                    recomendacion: 'Casi es un color pero no lo es',
                },
                {
                    opcion: 'Perro',
                    puntaje: 0,
                    correcta: false,
                    recomendacion: 'Un gato definitivamente no es un color'
                }
            ]
        },
        {
            pregunta: 'Selecciona una compañía de seguros de auto',
            opciones: [
                {
                    opcion: 'Coca cola',
                    puntaje: 0,
                    correcta: false,
                    recomendacion: 'Esta compañía ni siquiera es del ramo'
                },
                {
                    opcion: 'Qualitas',
                    puntaje: 30,
                    correcta: true,
                    recomendacion: '',
                },
                {
                    opcion: 'Sura',
                    puntaje: 20,
                    correcta: false,
                    recomendacion: 'Esta es una página de seguros pero no de auto'
                }
            ]
        }
    ];
    opcionElegida: any;
    contador = 0;

    constructor(private router: Router) {}

    ngOnInit(): void {
    }

    validarOpcion() {
        let titulo: string;
        let icono: string;

        if (this.opcionElegida.correcta) {
            icono = 'success';
            titulo = 'Respuesta correcta';
        }
        if (this.opcionElegida.puntaje === 20) {
            icono = 'warning';
            titulo = 'Respuesta casi correcta';
        }
        if (this.opcionElegida.puntaje === 0) {
            icono = 'error';
            titulo = 'Respuesta incorrecta';
        }

        this.showSwal(titulo, icono, this.opcionElegida.recomendacion).then(() => {
            if (this.contador === this.preguntas.length - 1) {
                window.sessionStorage.setItem('examenCompleto', 'chi');
                this.router.navigate(['/simulador/examenes/pendientes']);
            } else {
                this.contador++;
                this.opcionElegida = null;
            }
        });
    }

    showSwal(titulo: string, icono, texto) {
        return Swal.fire({
            title: titulo,
            text: texto,
            icon: icono
        });
    }

}
