import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamenProcesoComponent } from './examen-proceso.component';

describe('ExamenProcesoComponent', () => {
  let component: ExamenProcesoComponent;
  let fixture: ComponentFixture<ExamenProcesoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamenProcesoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamenProcesoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
