import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ExamenesPendientesComponent} from './examenes-pendientes/examenes-pendientes.component';
import {HistoricoExamenComponent} from './historico-examen/historico-examen.component';
import {ExamenProcesoComponent} from './examen-proceso/examen-proceso.component';


const routes: Routes = [
  {
    path: 'examenes',
    children: [
      {
        path: 'pendientes',
        component: ExamenesPendientesComponent,
      },
      {
        path: 'en-proceso',
        component: ExamenProcesoComponent,
      },
      {
        path: 'historico',
        component: HistoricoExamenComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SimuladorRoutingModule { }
