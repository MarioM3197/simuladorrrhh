import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoExamenComponent } from './historico-examen.component';

describe('HistoricoExamenComponent', () => {
  let component: HistoricoExamenComponent;
  let fixture: ComponentFixture<HistoricoExamenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricoExamenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoExamenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
