import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SimuladorRoutingModule} from './simulador-routing.module';
import {ExamenComponent} from './examen/examen.component';
import {HistoricoExamenComponent} from './historico-examen/historico-examen.component';
import {ExamenesPendientesComponent} from './examenes-pendientes/examenes-pendientes.component';
import {MatCardModule} from '@angular/material/card';
import {MatTooltipModule} from '@angular/material/tooltip';
import { ExamenProcesoComponent } from './examen-proceso/examen-proceso.component';
import {MatRadioModule} from '@angular/material/radio';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';


@NgModule({
    declarations: [ExamenComponent, HistoricoExamenComponent, ExamenesPendientesComponent, ExamenProcesoComponent],
    imports: [
        CommonModule,
        SimuladorRoutingModule,
        MatCardModule,
        MatTooltipModule,
        MatRadioModule,
        FormsModule,
        MatButtonModule,
        MatFormFieldModule
    ]
})
export class SimuladorModule {
}
