import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamenesPendientesComponent } from './examenes-pendientes.component';

describe('ExamenesPendientesComponent', () => {
  let component: ExamenesPendientesComponent;
  let fixture: ComponentFixture<ExamenesPendientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamenesPendientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamenesPendientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
