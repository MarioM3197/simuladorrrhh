import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizacionestiComponent } from './actualizacionesti.component';

describe('ActualizacionestiComponent', () => {
  let component: ActualizacionestiComponent;
  let fixture: ComponentFixture<ActualizacionestiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizacionestiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizacionestiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
