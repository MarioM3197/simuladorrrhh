import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActualizacionesRoutingModule } from './actualizaciones-routing.module';
import { ActualizacionestiComponent } from './actualizacionesti/actualizacionesti.component';
import {ActualizacionesComponent} from './actualizaciones.component';
import { VerDatosComponent } from './modals/ver-datos/ver-datos.component';


@NgModule({
  declarations: [
    ActualizacionestiComponent,
    ActualizacionesComponent,
    VerDatosComponent

  ],
  imports: [
    CommonModule,
    ActualizacionesRoutingModule
  ]
})
export class ActualizacionesModule { }
