import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';

export interface EsperaDeAtencion {
  tiempoEsperaDeAtencion: string;
  numeroEsperaDeAtencion: string;
  acdEsperaDeAtencion: string;
}

export interface EnLlamada {
  numeroEnLlamada: string;
  acdEnLlamada: string;
  ejecutivoEnLlamada: string;
  tiempoEnLlamada: string;
}

export interface EstadoDeEjecutivos {
  acd: string;
  estadoEjecutivo: string;
  totalEjecutivos: string;
}

const ELEMENT_DATA1: EsperaDeAtencion[] = [
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros El Aguila'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Banorte'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Banorte'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros El Aguila'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros ABA'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros ABA'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Afirme'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros La Latino'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Afirme'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Afirme'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros ABA'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Banorte'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros El Aguila'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Banorte'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Banorte'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros El Aguila'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros ABA'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros ABA'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Afirme'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros La Latino'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Afirme'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Afirme'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros ABA'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Banorte'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros El Aguila'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Banorte'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Banorte'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros El Aguila'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros ABA'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros ABA'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Afirme'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros La Latino'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Afirme'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Afirme'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Qualitas'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros ABA'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros Banorte'},
  { tiempoEsperaDeAtencion: '02:30:45', numeroEsperaDeAtencion: '9', acdEsperaDeAtencion: 'siniestros GNP'},
];

const ELEMENT_DATA2: EnLlamada[] = [
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros ABA', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros La Latino', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Atlas', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Atlas', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros La Latino', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros ABA', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros La Latino', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros El Potosi', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Afirme', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Zura', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros ABA', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros La Latino', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Atlas', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Atlas', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros La Latino', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros ABA', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros La Latino', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros El Potosi', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Afirme', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Zura', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros ABA', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros La Latino', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Atlas', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Atlas', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros La Latino', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros ABA', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Al Aguila', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros La Latino', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros El Potosi', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Afirme', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros Zura', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
  { numeroEnLlamada: '55-55-55-55-55', acdEnLlamada: 'siniestros GNP', ejecutivoEnLlamada: 'MARCO ANTONIO SOLIS DEL VALLE', tiempoEnLlamada: '02:30:45'},
];

const ELEMENT_DATA3: EstadoDeEjecutivos[] = [
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Qalitas', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Banorte', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros GNP', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros GNP', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Afirme', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros El Potosi', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros La Latino', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros GNP', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Banorte', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros VN Qualitas Brand', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Qalitas', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Banorte', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros GNP', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros GNP', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Afirme', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros El Potosi', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros La Latino', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros GNP', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Banorte', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros VN Qualitas Brand', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Qalitas', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Banorte', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros GNP', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros GNP', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Afirme', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros El Potosi', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros La Latino', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros GNP', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Banorte', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros VN Qualitas Brand', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros ABA', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
  {acd: 'siniestros Zura', estadoEjecutivo: '1 3 2', totalEjecutivos: '6'},
];

@Component({
  selector: 'app-modulo-llamadas',
  templateUrl: './modulo-llamadas.component.html',
  styleUrls: ['./modulo-llamadas.component.scss']
})

export class ModuloLlamadasComponent implements AfterViewInit {
  panelOpenState1 = false;
  panelOpenState2 = false;
  panelOpenState3 = false;
  displayedColumns1 = [
    'tiempoLlamadasEnEspera', 'numeroLlamadasEnEspera', 'acdLlamadasEnEspera',
  ];
  displayedColumns2 = [
    'numeroEnLLamada', 'acdEnLlamada', 'ejecutivoEnLlamada', 'tiempoEnLlamada',
  ];
  displayedColumns3 = [
    'estadosACD', 'estadosEjecutivos', 'estadosTotal'
  ];
  dataSource1 = new MatTableDataSource<EsperaDeAtencion>(ELEMENT_DATA1);
  dataSource2 = new MatTableDataSource<EnLlamada>(ELEMENT_DATA2);
  dataSource3 = new MatTableDataSource<EstadoDeEjecutivos>(ELEMENT_DATA3);

  @ViewChild('paginadorEnEspera', {read: MatPaginator}) paginadorEnEspera: MatPaginator;
  @ViewChild('paginadorEnLlamada', {read: MatPaginator}) paginadorEnLlamada: MatPaginator;
  @ViewChild('paginadorEstadosEjecutivos', {read: MatPaginator}) paginadorEstadosEjecutivos: MatPaginator;
  @ViewChild('sortEnEspera', {read: MatSort}) sortEnEspera: MatSort;
  @ViewChild('sortEnLlamada', {read: MatSort}) sortEnLlamada: MatSort;
  @ViewChild('sortEstadosEjecutivos', {read: MatSort}) sortEstadosEjecutivos: MatSort;

  ngAfterViewInit() {
    this.dataSource1.sort = this.sortEnEspera;
    this.dataSource2.sort = this.sortEnLlamada;
    this.dataSource3.sort = this.sortEstadosEjecutivos;
    this.dataSource1.paginator = this.paginadorEnEspera;
    this.dataSource2.paginator = this.paginadorEnLlamada;
    this.dataSource3.paginator = this.paginadorEstadosEjecutivos;
  }
}
