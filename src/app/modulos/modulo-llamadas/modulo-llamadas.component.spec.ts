import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuloLlamadasComponent } from './modulo-llamadas.component';

describe('ModuloLlamadasComponent', () => {
  let component: ModuloLlamadasComponent;
  let fixture: ComponentFixture<ModuloLlamadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuloLlamadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuloLlamadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
