import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarPolizaComponent } from './registrar-poliza.component';

describe('RegistrarPolizaComponent', () => {
  let component: RegistrarPolizaComponent;
  let fixture: ComponentFixture<RegistrarPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
