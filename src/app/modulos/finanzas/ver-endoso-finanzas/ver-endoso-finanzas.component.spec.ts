import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerEndosoFinanzasComponent } from './ver-endoso-finanzas.component';

describe('VerEndosoFinanzasComponent', () => {
  let component: VerEndosoFinanzasComponent;
  let fixture: ComponentFixture<VerEndosoFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerEndosoFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerEndosoFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
