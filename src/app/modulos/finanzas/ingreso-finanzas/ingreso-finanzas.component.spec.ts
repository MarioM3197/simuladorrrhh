import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoFinanzasComponent } from './ingreso-finanzas.component';

describe('IngresoFinanzasComponent', () => {
  let component: IngresoFinanzasComponent;
  let fixture: ComponentFixture<IngresoFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
