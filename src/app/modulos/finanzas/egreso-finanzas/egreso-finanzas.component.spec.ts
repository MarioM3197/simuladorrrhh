import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EgresoFinanzasComponent } from './egreso-finanzas.component';

describe('EgresoFinanzasComponent', () => {
  let component: EgresoFinanzasComponent;
  let fixture: ComponentFixture<EgresoFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EgresoFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EgresoFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
