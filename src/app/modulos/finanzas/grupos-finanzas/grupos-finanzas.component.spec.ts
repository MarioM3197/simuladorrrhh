import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GruposFinanzasComponent } from './grupos-finanzas.component';

describe('GruposFinanzasComponent', () => {
  let component: GruposFinanzasComponent;
  let fixture: ComponentFixture<GruposFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GruposFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GruposFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
