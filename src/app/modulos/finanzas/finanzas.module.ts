import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanzasRoutingModule } from './finanzas-routing.module';
import { FinanzasComponent } from './finanzas.component';
import { AdministracionRecibosComponent } from './administracion-recibos/administracion-recibos.component';
import { AreasFinanzasComponent } from './areas-finanzas/areas-finanzas.component';
import { CentroCostoComponent } from './centro-costo/centro-costo.component';
import { ClientesComponent } from './clientes/clientes.component';
import { DetallesPolizaFinanzasComponent } from './detalles-poliza-finanzas/detalles-poliza-finanzas.component';
import { EgresoFinanzasComponent } from './egreso-finanzas/egreso-finanzas.component';
import { EmpresaFinanzasComponent } from './empresa-finanzas/empresa-finanzas.component';
import { GruposFinanzasComponent } from './grupos-finanzas/grupos-finanzas.component';
import { IngresoFinanzasComponent } from './ingreso-finanzas/ingreso-finanzas.component';
import { RegistrarPolizaComponent } from './registrar-poliza/registrar-poliza.component';
import { SedesFinanzasComponent } from './sedes-finanzas/sedes-finanzas.component';
import { SubAreasFinanzasComponent } from './sub-areas-finanzas/sub-areas-finanzas.component';
import { VentaNuevaPorAplicarComponent } from './venta-nueva-por-aplicar/venta-nueva-por-aplicar.component';
import { VerEndosoFinanzasComponent } from './ver-endoso-finanzas/ver-endoso-finanzas.component';
import { ViewerPolizaComponent } from './viewer-poliza/viewer-poliza.component';
import { CrearClienteComponent } from './modals/crear-cliente/crear-cliente.component';
import { CrearEndosoAplicadoFinanzasComponent } from './modals/crear-endoso-aplicado-finanzas/crear-endoso-aplicado-finanzas.component';
import { CrearEndosoFinanzasComponent } from './modals/crear-endoso-finanzas/crear-endoso-finanzas.component';
import { CreateAreaComponent } from './modals/create-area/create-area.component';
import { CreateEmpresaComponent } from './modals/create-empresa/create-empresa.component';
import { CreateGrupoComponent } from './modals/create-grupo/create-grupo.component';
import { CreateIngresoFinanzasComponent } from './modals/create-ingreso-finanzas/create-ingreso-finanzas.component';
import { CreateSedeComponent } from './modals/create-sede/create-sede.component';
import { CreateSubareaComponent } from './modals/create-subarea/create-subarea.component';
import { DetallesClienteComponent } from './modals/detalles-cliente/detalles-cliente.component';
import { ModificacionesAplicacionComponent } from './modals/modificaciones-aplicacion/modificaciones-aplicacion.component';
import { ModificarNoPolizaComponent } from './modals/modificar-no-poliza/modificar-no-poliza.component';
import { PorAplicarEndosoComponent } from './modals/por-aplicar-endoso/por-aplicar-endoso.component';
import { SuspenderPolizasComponent } from './modals/suspender-polizas/suspender-polizas.component';
import { VerDatosSedeComponent } from './modals/ver-datos-sede/ver-datos-sede.component';


@NgModule({
  declarations: [
    FinanzasComponent,
    AdministracionRecibosComponent,
    AreasFinanzasComponent,
    CentroCostoComponent,
    ClientesComponent,
    DetallesPolizaFinanzasComponent,
    EgresoFinanzasComponent,
    EmpresaFinanzasComponent,
    GruposFinanzasComponent,
    IngresoFinanzasComponent,
    RegistrarPolizaComponent,
    SedesFinanzasComponent,
    SubAreasFinanzasComponent,
    VentaNuevaPorAplicarComponent,
    VerEndosoFinanzasComponent,
    ViewerPolizaComponent,
    CrearClienteComponent,
    CrearEndosoAplicadoFinanzasComponent,
    CrearEndosoFinanzasComponent,
    CreateAreaComponent,
    CreateEmpresaComponent,
    CreateGrupoComponent,
    CreateIngresoFinanzasComponent,
    CreateSedeComponent,
    CreateSubareaComponent,
    DetallesClienteComponent,
    ModificacionesAplicacionComponent,
    ModificarNoPolizaComponent,
    PorAplicarEndosoComponent,
    SuspenderPolizasComponent,
    VerDatosSedeComponent,


  ],
  imports: [
    CommonModule,
    FinanzasRoutingModule
  ]
})
export class FinanzasModule { }
