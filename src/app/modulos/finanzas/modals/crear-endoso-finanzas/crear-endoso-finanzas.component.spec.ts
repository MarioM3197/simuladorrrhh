import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearEndosoFinanzasComponent } from './crear-endoso-finanzas.component';

describe('CrearEndosoFinanzasComponent', () => {
  let component: CrearEndosoFinanzasComponent;
  let fixture: ComponentFixture<CrearEndosoFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearEndosoFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearEndosoFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
