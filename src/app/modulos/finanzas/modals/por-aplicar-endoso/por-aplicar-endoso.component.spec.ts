import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PorAplicarEndosoComponent } from './por-aplicar-endoso.component';

describe('PorAplicarEndosoComponent', () => {
  let component: PorAplicarEndosoComponent;
  let fixture: ComponentFixture<PorAplicarEndosoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PorAplicarEndosoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PorAplicarEndosoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
