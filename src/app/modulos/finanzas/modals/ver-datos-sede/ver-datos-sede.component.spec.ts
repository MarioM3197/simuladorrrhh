import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerDatosSedeComponent } from './ver-datos-sede.component';

describe('VerDatosSedeComponent', () => {
  let component: VerDatosSedeComponent;
  let fixture: ComponentFixture<VerDatosSedeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerDatosSedeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerDatosSedeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
