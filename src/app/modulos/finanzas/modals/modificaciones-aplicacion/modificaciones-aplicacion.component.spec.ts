import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificacionesAplicacionComponent } from './modificaciones-aplicacion.component';

describe('ModificacionesAplicacionComponent', () => {
  let component: ModificacionesAplicacionComponent;
  let fixture: ComponentFixture<ModificacionesAplicacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificacionesAplicacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificacionesAplicacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
