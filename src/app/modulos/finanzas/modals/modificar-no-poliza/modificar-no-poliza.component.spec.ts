import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarNoPolizaComponent } from './modificar-no-poliza.component';

describe('ModificarNoPolizaComponent', () => {
  let component: ModificarNoPolizaComponent;
  let fixture: ComponentFixture<ModificarNoPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarNoPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarNoPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
