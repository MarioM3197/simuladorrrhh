import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuspenderPolizasComponent } from './suspender-polizas.component';

describe('SuspenderPolizasComponent', () => {
  let component: SuspenderPolizasComponent;
  let fixture: ComponentFixture<SuspenderPolizasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuspenderPolizasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuspenderPolizasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
