import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateIngresoFinanzasComponent } from './create-ingreso-finanzas.component';

describe('CreateIngresoFinanzasComponent', () => {
  let component: CreateIngresoFinanzasComponent;
  let fixture: ComponentFixture<CreateIngresoFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateIngresoFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateIngresoFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
