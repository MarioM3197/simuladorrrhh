import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearEndosoAplicadoFinanzasComponent } from './crear-endoso-aplicado-finanzas.component';

describe('CrearEndosoAplicadoFinanzasComponent', () => {
  let component: CrearEndosoAplicadoFinanzasComponent;
  let fixture: ComponentFixture<CrearEndosoAplicadoFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearEndosoAplicadoFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearEndosoAplicadoFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
