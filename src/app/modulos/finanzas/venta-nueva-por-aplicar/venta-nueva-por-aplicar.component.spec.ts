import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentaNuevaPorAplicarComponent } from './venta-nueva-por-aplicar.component';

describe('VentaNuevaPorAplicarComponent', () => {
  let component: VentaNuevaPorAplicarComponent;
  let fixture: ComponentFixture<VentaNuevaPorAplicarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentaNuevaPorAplicarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentaNuevaPorAplicarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
