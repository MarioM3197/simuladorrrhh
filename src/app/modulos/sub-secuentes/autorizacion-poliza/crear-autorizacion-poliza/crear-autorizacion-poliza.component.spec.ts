import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearAutorizacionPolizaComponent } from './crear-autorizacion-poliza.component';

describe('CrearAutorizacionPolizaComponent', () => {
  let component: CrearAutorizacionPolizaComponent;
  let fixture: ComponentFixture<CrearAutorizacionPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearAutorizacionPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearAutorizacionPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
