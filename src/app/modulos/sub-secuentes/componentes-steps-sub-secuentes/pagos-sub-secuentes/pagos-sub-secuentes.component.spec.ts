import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosSubSecuentesComponent } from './pagos-sub-secuentes.component';

describe('PagosSubSecuentesComponent', () => {
  let component: PagosSubSecuentesComponent;
  let fixture: ComponentFixture<PagosSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
