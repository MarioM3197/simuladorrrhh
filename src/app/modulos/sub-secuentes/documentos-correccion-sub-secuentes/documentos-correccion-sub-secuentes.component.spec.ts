import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosCorreccionSubSecuentesComponent } from './documentos-correccion-sub-secuentes.component';

describe('DocumentosCorreccionSubSecuentesComponent', () => {
  let component: DocumentosCorreccionSubSecuentesComponent;
  let fixture: ComponentFixture<DocumentosCorreccionSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentosCorreccionSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosCorreccionSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
