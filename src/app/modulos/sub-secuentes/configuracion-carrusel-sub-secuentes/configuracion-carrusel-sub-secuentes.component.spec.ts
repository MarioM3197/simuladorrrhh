import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiguracionCarruselSubSecuentesComponent } from './configuracion-carrusel-sub-secuentes.component';

describe('ConfiguracionCarruselSubSecuentesComponent', () => {
  let component: ConfiguracionCarruselSubSecuentesComponent;
  let fixture: ComponentFixture<ConfiguracionCarruselSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfiguracionCarruselSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfiguracionCarruselSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
