import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarDocumentosAutorizacionSubSecuentesComponent } from './mostrar-documentos-autorizacion-sub-secuentes.component';

describe('MostrarDocumentosAutorizacionSubSecuentesComponent', () => {
  let component: MostrarDocumentosAutorizacionSubSecuentesComponent;
  let fixture: ComponentFixture<MostrarDocumentosAutorizacionSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarDocumentosAutorizacionSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarDocumentosAutorizacionSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
