import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearAutorizacionSubSecuentesComponent } from './crear-autorizacion-sub-secuentes.component';

describe('CrearAutorizacionSubSecuentesComponent', () => {
  let component: CrearAutorizacionSubSecuentesComponent;
  let fixture: ComponentFixture<CrearAutorizacionSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearAutorizacionSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearAutorizacionSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
