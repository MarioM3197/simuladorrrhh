import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtiquetaSolicitudSubsComponent } from './etiqueta-solicitud-subs.component';

describe('EtiquetaSolicitudSubsComponent', () => {
  let component: EtiquetaSolicitudSubsComponent;
  let fixture: ComponentFixture<EtiquetaSolicitudSubsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtiquetaSolicitudSubsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtiquetaSolicitudSubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
