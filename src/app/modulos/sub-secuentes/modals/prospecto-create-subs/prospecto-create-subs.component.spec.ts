import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectoCreateSubsComponent } from './prospecto-create-subs.component';

describe('ProspectoCreateSubsComponent', () => {
  let component: ProspectoCreateSubsComponent;
  let fixture: ComponentFixture<ProspectoCreateSubsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProspectoCreateSubsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectoCreateSubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
