import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubSecuentesRoutingModule } from './sub-secuentes-routing.module';
import { SubSecuentesComponent } from './sub-secuentes.component';
import { ActivacionEmpleadoSubSecuentesComponent } from './activacion-empleado-sub-secuentes/activacion-empleado-sub-secuentes.component';
import { AdministracionPolizasComponent } from './administracion-polizas/administracion-polizas.component';
import { CrearAutorizacionSubSecuentesComponent } from './autorizacion-errores-sub-secuentes/crear-autorizacion-sub-secuentes/crear-autorizacion-sub-secuentes.component';
import { AutorizacionesProcesoSubSecuentesComponent } from './autorizacion-errores-sub-secuentes/autorizaciones-proceso-sub-secuentes/autorizaciones-proceso-sub-secuentes.component';
import { MostrarDocumentosAutorizacionSubSecuentesComponent } from './autorizacion-errores-sub-secuentes/mostrar-documentos-autorizacion-sub-secuentes/mostrar-documentos-autorizacion-sub-secuentes.component';
import { AutorizacionesProcesoPolizaComponent } from './autorizacion-poliza/autorizaciones-proceso-poliza/autorizaciones-proceso-poliza.component';
import { CorreccionErroresPolizaComponent } from './autorizacion-poliza/correccion-errores-poliza/correccion-errores-poliza.component';
import { CrearAutorizacionPolizaComponent } from './autorizacion-poliza/crear-autorizacion-poliza/crear-autorizacion-poliza.component';
import { DocumentosCorreccionPolizaComponent } from './autorizacion-poliza/documentos-correccion-poliza/documentos-correccion-poliza.component';
import { MostrarDocumentosPolizaComponent } from './autorizacion-poliza/mostrar-documentos-poliza/mostrar-documentos-poliza.component';
import { ClienteSubSecuentesComponent } from './componentes-steps-sub-secuentes/cliente-sub-secuentes/cliente-sub-secuentes.component';
import { DatosProdutoSubSecuentesComponent } from './componentes-steps-sub-secuentes/datos-produto-sub-secuentes/datos-produto-sub-secuentes.component';
import { InspeccionSubSecuentesComponent } from './componentes-steps-sub-secuentes/inspeccion-sub-secuentes/inspeccion-sub-secuentes.component';
import { PagosSubSecuentesComponent } from './componentes-steps-sub-secuentes/pagos-sub-secuentes/pagos-sub-secuentes.component';
import { RecibosSubSecuentesComponent } from './componentes-steps-sub-secuentes/recibos-sub-secuentes/recibos-sub-secuentes.component';
import { RegistroPolizaSubSecuentesComponent } from './componentes-steps-sub-secuentes/registro-poliza-sub-secuentes/registro-poliza-sub-secuentes.component';
import { ConfiguracionCarruselSubSecuentesComponent } from './configuracion-carrusel-sub-secuentes/configuracion-carrusel-sub-secuentes.component';
import { CorreccionErroresSubSecuentesComponent } from './correccion-errores-sub-secuentes/correccion-errores-sub-secuentes.component';
import { CotizadorInternoSubSecuentesComponent } from './cotizador-interno-sub-secuentes/cotizador-interno-sub-secuentes.component';
import { CotizadorSubSecuentesComponent } from './cotizador-sub-secuentes/cotizador-sub-secuentes.component';
import { CotizadorTabsSubSecuentesComponent } from './cotizador-tabs-sub-secuentes/cotizador-tabs-sub-secuentes.component';
import { DetallesPolizaComponent } from './detalles-poliza/detalles-poliza.component';
import { DocumentosCorreccionSubSecuentesComponent } from './documentos-correccion-sub-secuentes/documentos-correccion-sub-secuentes.component';
import { PagoRecibosComponent } from './pago-recibos/pago-recibos.component';
import { PolizasTotalesComponent } from './polizas-totales/polizas-totales.component';
import { RecibosComponent } from './recibos/recibos.component';
import { SolicitudesSubSecuentesComponent } from './solicitudes-sub-secuentes/solicitudes-sub-secuentes.component';
import { StepCotizadorSubSecuentesComponent } from './step-cotizador-sub-secuentes/step-cotizador-sub-secuentes.component';
import { ViewerPolizaComponent } from './viewer-poliza/viewer-poliza.component';
import { DatosSolicitudSubsComponent } from './modals/datos-solicitud-subs/datos-solicitud-subs.component';
import { EndosoCreateComponent } from './modals/endoso-create/endoso-create.component';
import { EtiquetaSolicitudSubsComponent } from './modals/etiqueta-solicitud-subs/etiqueta-solicitud-subs.component';
import { LlamadasMyccSubsComponent } from './modals/llamadas-mycc-subs/llamadas-mycc-subs.component';
import { ProspectoCreateSubsComponent } from './modals/prospecto-create-subs/prospecto-create-subs.component';
import { ReasignarRecibosComponent } from './modals/reasignar-recibos/reasignar-recibos.component';
import { ReasifnarSoliciudSubsComponent } from './modals/reasifnar-soliciud-subs/reasifnar-soliciud-subs.component';
import { SubirArchivoComponent } from './modals/subir-archivo/subir-archivo.component';
import { VisualizadorPagoComponent } from './modals/visualizador-pago/visualizador-pago.component';


@NgModule({
  declarations: [SubSecuentesComponent,
    ActivacionEmpleadoSubSecuentesComponent,
    AdministracionPolizasComponent,
    CrearAutorizacionSubSecuentesComponent,
    AutorizacionesProcesoSubSecuentesComponent,
    MostrarDocumentosAutorizacionSubSecuentesComponent,
    AutorizacionesProcesoPolizaComponent,
    CorreccionErroresPolizaComponent,
    CrearAutorizacionPolizaComponent,
    DocumentosCorreccionPolizaComponent,
    MostrarDocumentosPolizaComponent,
    ClienteSubSecuentesComponent,
    DatosProdutoSubSecuentesComponent,
    InspeccionSubSecuentesComponent,
    PagosSubSecuentesComponent,
    RecibosSubSecuentesComponent,
    RegistroPolizaSubSecuentesComponent,
    ConfiguracionCarruselSubSecuentesComponent,
    CorreccionErroresSubSecuentesComponent,
    CotizadorInternoSubSecuentesComponent,
    CotizadorSubSecuentesComponent,
    CotizadorTabsSubSecuentesComponent,
    DetallesPolizaComponent,
    DocumentosCorreccionSubSecuentesComponent,
    PagoRecibosComponent,
    PolizasTotalesComponent,
    RecibosComponent,
    SolicitudesSubSecuentesComponent,
    StepCotizadorSubSecuentesComponent,
    ViewerPolizaComponent,
    DatosSolicitudSubsComponent,
    EndosoCreateComponent,
    EtiquetaSolicitudSubsComponent,
    LlamadasMyccSubsComponent,
    ProspectoCreateSubsComponent,
    ReasignarRecibosComponent,
    ReasifnarSoliciudSubsComponent,
    SubirArchivoComponent,
    VisualizadorPagoComponent
  ],
  imports: [
    CommonModule,
    SubSecuentesRoutingModule
  ]
})
export class SubSecuentesModule { }
