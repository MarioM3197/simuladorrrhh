import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorreccionErroresSubSecuentesComponent } from './correccion-errores-sub-secuentes.component';

describe('CorreccionErroresSubSecuentesComponent', () => {
  let component: CorreccionErroresSubSecuentesComponent;
  let fixture: ComponentFixture<CorreccionErroresSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorreccionErroresSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorreccionErroresSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
