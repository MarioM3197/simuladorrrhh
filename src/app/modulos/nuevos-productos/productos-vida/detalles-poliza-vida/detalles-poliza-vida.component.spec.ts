import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesPolizaVidaComponent } from './detalles-poliza-vida.component';

describe('DetallesPolizaVidaComponent', () => {
  let component: DetallesPolizaVidaComponent;
  let fixture: ComponentFixture<DetallesPolizaVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesPolizaVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesPolizaVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
