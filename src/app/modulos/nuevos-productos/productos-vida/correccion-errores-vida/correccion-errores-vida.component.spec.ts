import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorreccionErroresVidaComponent } from './correccion-errores-vida.component';

describe('CorreccionErroresVidaComponent', () => {
  let component: CorreccionErroresVidaComponent;
  let fixture: ComponentFixture<CorreccionErroresVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorreccionErroresVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorreccionErroresVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
