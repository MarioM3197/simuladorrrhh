import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolizaPagosVidaComponent } from './poliza-pagos-vida.component';

describe('PolizaPagosVidaComponent', () => {
  let component: PolizaPagosVidaComponent;
  let fixture: ComponentFixture<PolizaPagosVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolizaPagosVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolizaPagosVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
