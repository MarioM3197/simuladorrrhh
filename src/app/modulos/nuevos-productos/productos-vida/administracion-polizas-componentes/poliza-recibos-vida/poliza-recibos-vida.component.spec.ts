import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolizaRecibosVidaComponent } from './poliza-recibos-vida.component';

describe('PolizaRecibosVidaComponent', () => {
  let component: PolizaRecibosVidaComponent;
  let fixture: ComponentFixture<PolizaRecibosVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolizaRecibosVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolizaRecibosVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
