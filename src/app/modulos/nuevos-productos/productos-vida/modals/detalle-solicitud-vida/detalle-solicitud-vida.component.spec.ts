import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudVidaComponent } from './detalle-solicitud-vida.component';

describe('DetalleSolicitudVidaComponent', () => {
  let component: DetalleSolicitudVidaComponent;
  let fixture: ComponentFixture<DetalleSolicitudVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
