import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudVidaCreateComponent } from './solicitud-vida-create.component';

describe('SolicitudVidaCreateComponent', () => {
  let component: SolicitudVidaCreateComponent;
  let fixture: ComponentFixture<SolicitudVidaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudVidaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudVidaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
