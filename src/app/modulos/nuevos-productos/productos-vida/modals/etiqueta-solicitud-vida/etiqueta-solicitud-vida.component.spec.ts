import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtiquetaSolicitudVidaComponent } from './etiqueta-solicitud-vida.component';

describe('EtiquetaSolicitudVidaComponent', () => {
  let component: EtiquetaSolicitudVidaComponent;
  let fixture: ComponentFixture<EtiquetaSolicitudVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtiquetaSolicitudVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtiquetaSolicitudVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
