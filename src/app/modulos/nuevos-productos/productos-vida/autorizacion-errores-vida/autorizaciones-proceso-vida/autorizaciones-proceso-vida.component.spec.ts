import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizacionesProcesoVidaComponent } from './autorizaciones-proceso-vida.component';

describe('AutorizacionesProcesoVidaComponent', () => {
  let component: AutorizacionesProcesoVidaComponent;
  let fixture: ComponentFixture<AutorizacionesProcesoVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizacionesProcesoVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizacionesProcesoVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
