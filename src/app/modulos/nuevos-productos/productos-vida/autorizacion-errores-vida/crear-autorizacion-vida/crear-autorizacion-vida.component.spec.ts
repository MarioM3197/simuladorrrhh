import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearAutorizacionVidaComponent } from './crear-autorizacion-vida.component';

describe('CrearAutorizacionVidaComponent', () => {
  let component: CrearAutorizacionVidaComponent;
  let fixture: ComponentFixture<CrearAutorizacionVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearAutorizacionVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearAutorizacionVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
