import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspeccionesVidaComponent } from './inspecciones-vida.component';

describe('InspeccionesVidaComponent', () => {
  let component: InspeccionesVidaComponent;
  let fixture: ComponentFixture<InspeccionesVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspeccionesVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspeccionesVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
