import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosVidaComponent } from './pagos-vida.component';

describe('PagosVidaComponent', () => {
  let component: PagosVidaComponent;
  let fixture: ComponentFixture<PagosVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
