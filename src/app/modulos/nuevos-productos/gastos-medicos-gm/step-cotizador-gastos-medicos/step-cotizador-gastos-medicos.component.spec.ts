import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepCotizadorGastosMedicosComponent } from './step-cotizador-gastos-medicos.component';

describe('StepCotizadorGastosMedicosComponent', () => {
  let component: StepCotizadorGastosMedicosComponent;
  let fixture: ComponentFixture<StepCotizadorGastosMedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepCotizadorGastosMedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepCotizadorGastosMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
