import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizacionesProcesoGmComponent } from './autorizaciones-proceso-gm.component';

describe('AutorizacionesProcesoGmComponent', () => {
  let component: AutorizacionesProcesoGmComponent;
  let fixture: ComponentFixture<AutorizacionesProcesoGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizacionesProcesoGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizacionesProcesoGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
