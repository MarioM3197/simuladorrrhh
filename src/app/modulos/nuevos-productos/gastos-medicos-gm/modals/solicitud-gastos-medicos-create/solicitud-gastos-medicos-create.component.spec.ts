import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudGastosMedicosCreateComponent } from './solicitud-gastos-medicos-create.component';

describe('SolicitudGastosMedicosCreateComponent', () => {
  let component: SolicitudGastosMedicosCreateComponent;
  let fixture: ComponentFixture<SolicitudGastosMedicosCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudGastosMedicosCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudGastosMedicosCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
