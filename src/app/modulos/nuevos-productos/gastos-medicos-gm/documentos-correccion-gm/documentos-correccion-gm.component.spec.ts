import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosCorreccionGmComponent } from './documentos-correccion-gm.component';

describe('DocumentosCorreccionGmComponent', () => {
  let component: DocumentosCorreccionGmComponent;
  let fixture: ComponentFixture<DocumentosCorreccionGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentosCorreccionGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosCorreccionGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
