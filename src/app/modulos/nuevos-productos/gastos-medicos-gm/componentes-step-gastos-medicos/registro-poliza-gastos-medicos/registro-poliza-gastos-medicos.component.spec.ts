import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroPolizaGastosMedicosComponent } from './registro-poliza-gastos-medicos.component';

describe('RegistroPolizaGastosMedicosComponent', () => {
  let component: RegistroPolizaGastosMedicosComponent;
  let fixture: ComponentFixture<RegistroPolizaGastosMedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroPolizaGastosMedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroPolizaGastosMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
