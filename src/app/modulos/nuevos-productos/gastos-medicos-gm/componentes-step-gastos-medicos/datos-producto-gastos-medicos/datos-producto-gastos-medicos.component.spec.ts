import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosProductoGastosMedicosComponent } from './datos-producto-gastos-medicos.component';

describe('DatosProductoGastosMedicosComponent', () => {
  let component: DatosProductoGastosMedicosComponent;
  let fixture: ComponentFixture<DatosProductoGastosMedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosProductoGastosMedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosProductoGastosMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
