import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibosGastosMedicosComponent } from './recibos-gastos-medicos.component';

describe('RecibosGastosMedicosComponent', () => {
  let component: RecibosGastosMedicosComponent;
  let fixture: ComponentFixture<RecibosGastosMedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecibosGastosMedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibosGastosMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
