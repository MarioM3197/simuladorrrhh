import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosGastosMedicosComponent } from './pagos-gastos-medicos.component';

describe('PagosGastosMedicosComponent', () => {
  let component: PagosGastosMedicosComponent;
  let fixture: ComponentFixture<PagosGastosMedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosGastosMedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosGastosMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
