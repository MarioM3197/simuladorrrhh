import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesPolizaGmComponent } from './detalles-poliza-gm.component';

describe('DetallesPolizaGmComponent', () => {
  let component: DetallesPolizaGmComponent;
  let fixture: ComponentFixture<DetallesPolizaGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesPolizaGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesPolizaGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
