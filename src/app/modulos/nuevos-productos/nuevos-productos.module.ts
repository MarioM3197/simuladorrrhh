import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NuevosProductosRoutingModule } from './nuevos-productos-routing.module';
import { NuevosProductosComponent } from './nuevos-productos.component';
import { ActivacionEmpleadoGmComponent } from './gastos-medicos-gm/activacion-empleado-gm/activacion-empleado-gm.component';
import { PagosGmComponent } from './gastos-medicos-gm/administracion-polizas-componentes/pagos-gm/pagos-gm.component';
import { RecibosGmComponent } from './gastos-medicos-gm/administracion-polizas-componentes/recibos-gm/recibos-gm.component';
import { AdministracionPolizasGmComponent } from './gastos-medicos-gm/administracion-polizas-gm/administracion-polizas-gm.component';
import { AutorizacionesProcesoGmComponent } from './gastos-medicos-gm/autorizacion-errores-gm/autorizaciones-proceso-gm/autorizaciones-proceso-gm.component';
import { CrearAutorizacionGnmComponent } from './gastos-medicos-gm/autorizacion-errores-gm/crear-autorizacion-gnm/crear-autorizacion-gnm.component';
import { MostrarDocumentosAutorizacionGmComponent } from './gastos-medicos-gm/autorizacion-errores-gm/mostrar-documentos-autorizacion-gm/mostrar-documentos-autorizacion-gm.component';
import { CreateEmisionGastosMedicosComponent } from './gastos-medicos-gm/componentes-step-gastos-medicos/create-emision-gastos-medicos/create-emision-gastos-medicos.component';
import { DatosProductoGastosMedicosComponent } from './gastos-medicos-gm/componentes-step-gastos-medicos/datos-producto-gastos-medicos/datos-producto-gastos-medicos.component';
import { InspeccionesGastosMedicosComponent } from './gastos-medicos-gm/componentes-step-gastos-medicos/inspecciones-gastos-medicos/inspecciones-gastos-medicos.component';
import { PagosGastosMedicosComponent } from './gastos-medicos-gm/componentes-step-gastos-medicos/pagos-gastos-medicos/pagos-gastos-medicos.component';
import { RecibosGastosMedicosComponent } from './gastos-medicos-gm/componentes-step-gastos-medicos/recibos-gastos-medicos/recibos-gastos-medicos.component';
import { RegistroPolizaGastosMedicosComponent } from './gastos-medicos-gm/componentes-step-gastos-medicos/registro-poliza-gastos-medicos/registro-poliza-gastos-medicos.component';
import { CorrecionErroresGmComponent } from './gastos-medicos-gm/correcion-errores-gm/correcion-errores-gm.component';
import { CotizadorGastosNedicosComponent } from './gastos-medicos-gm/cotizador-gastos-nedicos/cotizador-gastos-nedicos.component';
import { DetallesPolizaGmComponent } from './gastos-medicos-gm/detalles-poliza-gm/detalles-poliza-gm.component';
import { DocumentosCorreccionGmComponent } from './gastos-medicos-gm/documentos-correccion-gm/documentos-correccion-gm.component';
import { SolicitudesGastosMedicosComponent } from './gastos-medicos-gm/solicitudes-gastos-medicos/solicitudes-gastos-medicos.component';
import { StepCotizadorGastosMedicosComponent } from './gastos-medicos-gm/step-cotizador-gastos-medicos/step-cotizador-gastos-medicos.component';
import { DetalleSolicitudGmComponent } from './gastos-medicos-gm/modals/detalle-solicitud-gm/detalle-solicitud-gm.component';
import { EtiquetaSolicitudGmComponent } from './gastos-medicos-gm/modals/etiqueta-solicitud-gm/etiqueta-solicitud-gm.component';
import { ReasignarSolicitudGmComponent } from './gastos-medicos-gm/modals/reasignar-solicitud-gm/reasignar-solicitud-gm.component';
import { SolicitudGastosMedicosCreateComponent } from './gastos-medicos-gm/modals/solicitud-gastos-medicos-create/solicitud-gastos-medicos-create.component';
import { BajaRecibosComponent } from './modals/baja-recibos/baja-recibos.component';
import { DatepickerReciboHogarComponent } from './modals/datepicker-recibo-hogar/datepicker-recibo-hogar.component';
import { DatosSolicitudComponent } from './modals/datos-solicitud/datos-solicitud.component';
import { SolicitudEndosoCreateComponent } from './modals/solicitud-endoso-create/solicitud-endoso-create.component';
import { SubirArchivoClienteComponent } from './modals/subir-archivo-cliente/subir-archivo-cliente.component';
import { ActivacionEmpleadoHogarComponent } from './productos-hogar/activacion-empleado-hogar/activacion-empleado-hogar.component';
import { PolizaPagosHogarComponent } from './productos-hogar/administracion-polizas-componentes/poliza-pagos-hogar/poliza-pagos-hogar.component';
import { PolizaRecibosHogarComponent } from './productos-hogar/administracion-polizas-componentes/poliza-recibos-hogar/poliza-recibos-hogar.component';
import { AdministracionPolizasHogarComponent } from './productos-hogar/administracion-polizas-hogar/administracion-polizas-hogar.component';
import { AutorizacionesProcesoHogarComponent } from './productos-hogar/autorizacion-errores-hogar/autorizaciones-proceso-hogar/autorizaciones-proceso-hogar.component';
import { CrearAutorizacionHogarComponent } from './productos-hogar/autorizacion-errores-hogar/crear-autorizacion-hogar/crear-autorizacion-hogar.component';
import { MostrarDocumentosAutorizacionHogarComponent } from './productos-hogar/autorizacion-errores-hogar/mostrar-documentos-autorizacion-hogar/mostrar-documentos-autorizacion-hogar.component';
import { CreateEmisionHogarComponent } from './productos-hogar/componentes-step-hogar/create-emision-hogar/create-emision-hogar.component';
import { DatosProductoHogarComponent } from './productos-hogar/componentes-step-hogar/datos-producto-hogar/datos-producto-hogar.component';
import { InspeccionesHogarComponent } from './productos-hogar/componentes-step-hogar/inspecciones-hogar/inspecciones-hogar.component';
import { PagosHogarComponent } from './productos-hogar/componentes-step-hogar/pagos-hogar/pagos-hogar.component';
import { RecibosHogarComponent } from './productos-hogar/componentes-step-hogar/recibos-hogar/recibos-hogar.component';
import { RegistroPolizaHogarComponent } from './productos-hogar/componentes-step-hogar/registro-poliza-hogar/registro-poliza-hogar.component';
import { CorreccionErroresHogarComponent } from './productos-hogar/correccion-errores-hogar/correccion-errores-hogar.component';
import { CotizadorHogarComponent } from './productos-hogar/cotizador-hogar/cotizador-hogar.component';
import { DetallesPolizaHogarComponent } from './productos-hogar/detalles-poliza-hogar/detalles-poliza-hogar.component';
import { DocumentosCorreccionHogarComponent } from './productos-hogar/documentos-correccion-hogar/documentos-correccion-hogar.component';
import { SolicitudesHogarComponent } from './productos-hogar/solicitudes-hogar/solicitudes-hogar.component';
import { StepCotizadorHogarComponent } from './productos-hogar/step-cotizador-hogar/step-cotizador-hogar.component';
import { DetalleSolicitudHogarComponent } from './productos-hogar/modals/detalle-solicitud-hogar/detalle-solicitud-hogar.component';
import { EtiquetaSolicitudHogarComponent } from './productos-hogar/modals/etiqueta-solicitud-hogar/etiqueta-solicitud-hogar.component';
import { ReasignarSolicitudHogarComponent } from './productos-hogar/modals/reasignar-solicitud-hogar/reasignar-solicitud-hogar.component';
import { SolicitudHogarCreateComponent } from './productos-hogar/modals/solicitud-hogar-create/solicitud-hogar-create.component';
import { ActivacionEmpleadoPymeComponent } from './productos-pyme/activacion-empleado-pyme/activacion-empleado-pyme.component';
import { PolizaPagosPymeComponent } from './productos-pyme/administracion-polizas-componentes/poliza-pagos-pyme/poliza-pagos-pyme.component';
import { PolizaRecibosPymeComponent } from './productos-pyme/administracion-polizas-componentes/poliza-recibos-pyme/poliza-recibos-pyme.component';
import { AdministracionPolizasPymeComponent } from './productos-pyme/administracion-polizas-pyme/administracion-polizas-pyme.component';
import { AutorizacionesProcesoPymeComponent } from './productos-pyme/autorizacion-errores-pyme/autorizaciones-proceso-pyme/autorizaciones-proceso-pyme.component';
import { CrearAutorizacionesPymeComponent } from './productos-pyme/autorizacion-errores-pyme/crear-autorizaciones-pyme/crear-autorizaciones-pyme.component';
import { MostrarDocumentosAutorizacionPymeComponent } from './productos-pyme/autorizacion-errores-pyme/mostrar-documentos-autorizacion-pyme/mostrar-documentos-autorizacion-pyme.component';
import { CreateEmisionPymeComponent } from './productos-pyme/componentes-step-pyme/create-emision-pyme/create-emision-pyme.component';
import { DatosProductoPymeComponent } from './productos-pyme/componentes-step-pyme/datos-producto-pyme/datos-producto-pyme.component';
import { InspeccionesPymeComponent } from './productos-pyme/componentes-step-pyme/inspecciones-pyme/inspecciones-pyme.component';
import { PagosPymeComponent } from './productos-pyme/componentes-step-pyme/pagos-pyme/pagos-pyme.component';
import { RecibosPymeComponent } from './productos-pyme/componentes-step-pyme/recibos-pyme/recibos-pyme.component';
import { RegistroPolizaPymeComponent } from './productos-pyme/componentes-step-pyme/registro-poliza-pyme/registro-poliza-pyme.component';
import { CorreccionErroresPymeComponent } from './productos-pyme/correccion-errores-pyme/correccion-errores-pyme.component';
import { CotizadorPymeComponent } from './productos-pyme/cotizador-pyme/cotizador-pyme.component';
import { DetallesPolizaPymeComponent } from './productos-pyme/detalles-poliza-pyme/detalles-poliza-pyme.component';
import { DocumentosCorreccionPymeComponent } from './productos-pyme/documentos-correccion-pyme/documentos-correccion-pyme.component';
import { SolicitudesPymeComponent } from './productos-pyme/solicitudes-pyme/solicitudes-pyme.component';
import { StepCotizadorPymeComponent } from './productos-pyme/step-cotizador-pyme/step-cotizador-pyme.component';
import { DetalleSolicitudPymeComponent } from './productos-pyme/modals/detalle-solicitud-pyme/detalle-solicitud-pyme.component';
import { EtiquetaSolicitudPymeComponent } from './productos-pyme/modals/etiqueta-solicitud-pyme/etiqueta-solicitud-pyme.component';
import { ReasignarSolicitudPymeComponent } from './productos-pyme/modals/reasignar-solicitud-pyme/reasignar-solicitud-pyme.component';
import { SolicitudPymeCreateComponent } from './productos-pyme/modals/solicitud-pyme-create/solicitud-pyme-create.component';
import { ActivacionEmpleadoVidaComponent } from './productos-vida/activacion-empleado-vida/activacion-empleado-vida.component';
import { PolizaPagosVidaComponent } from './productos-vida/administracion-polizas-componentes/poliza-pagos-vida/poliza-pagos-vida.component';
import { PolizaRecibosVidaComponent } from './productos-vida/administracion-polizas-componentes/poliza-recibos-vida/poliza-recibos-vida.component';
import { AdministracionPolizasVidaComponent } from './productos-vida/administracion-polizas-vida/administracion-polizas-vida.component';
import { AutorizacionesProcesoVidaComponent } from './productos-vida/autorizacion-errores-vida/autorizaciones-proceso-vida/autorizaciones-proceso-vida.component';
import { CrearAutorizacionVidaComponent } from './productos-vida/autorizacion-errores-vida/crear-autorizacion-vida/crear-autorizacion-vida.component';
import { MostrarDocumentosAutorizacionVidaComponent } from './productos-vida/autorizacion-errores-vida/mostrar-documentos-autorizacion-vida/mostrar-documentos-autorizacion-vida.component';
import { CreateEmisionVidaComponent } from './productos-vida/componentes-step-vida/create-emision-vida/create-emision-vida.component';
import { DatosProductoVidaComponent } from './productos-vida/componentes-step-vida/datos-producto-vida/datos-producto-vida.component';
import { InspeccionesVidaComponent } from './productos-vida/componentes-step-vida/inspecciones-vida/inspecciones-vida.component';
import { PagosVidaComponent } from './productos-vida/componentes-step-vida/pagos-vida/pagos-vida.component';
import { RecibosVidaComponent } from './productos-vida/componentes-step-vida/recibos-vida/recibos-vida.component';
import { RegistroPolizaVidaComponent } from './productos-vida/componentes-step-vida/registro-poliza-vida/registro-poliza-vida.component';
import { CorreccionErroresVidaComponent } from './productos-vida/correccion-errores-vida/correccion-errores-vida.component';
import { CotizadorVidaComponent } from './productos-vida/cotizador-vida/cotizador-vida.component';
import { DetallesPolizaVidaComponent } from './productos-vida/detalles-poliza-vida/detalles-poliza-vida.component';
import { DocumentosCorreccionVidaComponent } from './productos-vida/documentos-correccion-vida/documentos-correccion-vida.component';
import { SolicitudesVidaComponent } from './productos-vida/solicitudes-vida/solicitudes-vida.component';
import { StepCotizadorVidaComponent } from './productos-vida/step-cotizador-vida/step-cotizador-vida.component';
import { DetalleSolicitudVidaComponent } from './productos-vida/modals/detalle-solicitud-vida/detalle-solicitud-vida.component';
import { EtiquetaSolicitudVidaComponent } from './productos-vida/modals/etiqueta-solicitud-vida/etiqueta-solicitud-vida.component';
import { ReasignarSolicitudVidaComponent } from './productos-vida/modals/reasignar-solicitud-vida/reasignar-solicitud-vida.component';
import { SolicitudVidaCreateComponent } from './productos-vida/modals/solicitud-vida-create/solicitud-vida-create.component';
import { ViewerPolizaComponent } from './viewer-poliza/viewer-poliza.component';


@NgModule({
  declarations: [
    NuevosProductosComponent,
    ActivacionEmpleadoGmComponent,
    PagosGmComponent,
    RecibosGmComponent,
    AdministracionPolizasGmComponent,
    AutorizacionesProcesoGmComponent,
    CrearAutorizacionGnmComponent,
    MostrarDocumentosAutorizacionGmComponent,
    CreateEmisionGastosMedicosComponent,
    DatosProductoGastosMedicosComponent,
    InspeccionesGastosMedicosComponent,
    PagosGastosMedicosComponent,
    RecibosGastosMedicosComponent,
    RegistroPolizaGastosMedicosComponent,
    CorrecionErroresGmComponent,
    CotizadorGastosNedicosComponent,
    DetallesPolizaGmComponent,
    DocumentosCorreccionGmComponent,
    SolicitudesGastosMedicosComponent,
    StepCotizadorGastosMedicosComponent,
    DetalleSolicitudGmComponent,
    EtiquetaSolicitudGmComponent,
    ReasignarSolicitudGmComponent,
    SolicitudGastosMedicosCreateComponent,
    BajaRecibosComponent,
    DatepickerReciboHogarComponent,
    DatosSolicitudComponent,
    SolicitudEndosoCreateComponent,
    SubirArchivoClienteComponent,
    ActivacionEmpleadoHogarComponent,
    PolizaPagosHogarComponent,
    PolizaRecibosHogarComponent,
    AdministracionPolizasHogarComponent,
    AutorizacionesProcesoHogarComponent,
    CrearAutorizacionHogarComponent,
    MostrarDocumentosAutorizacionHogarComponent,
    CreateEmisionHogarComponent,
    DatosProductoHogarComponent,
    InspeccionesHogarComponent,
    PagosHogarComponent,
    RecibosHogarComponent,
    RegistroPolizaHogarComponent,
    CorreccionErroresHogarComponent,
    CotizadorHogarComponent,
    DetallesPolizaHogarComponent,
    DocumentosCorreccionHogarComponent,
    SolicitudesHogarComponent,
    StepCotizadorHogarComponent,
    DetalleSolicitudHogarComponent,
    EtiquetaSolicitudHogarComponent,
    ReasignarSolicitudHogarComponent,
    SolicitudHogarCreateComponent,
    ActivacionEmpleadoPymeComponent,
    PolizaPagosPymeComponent,
    PolizaRecibosPymeComponent,
    AdministracionPolizasPymeComponent,
    AutorizacionesProcesoPymeComponent,
    CrearAutorizacionesPymeComponent,
    MostrarDocumentosAutorizacionPymeComponent,
    CreateEmisionPymeComponent,
    DatosProductoPymeComponent,
    InspeccionesPymeComponent,
    PagosPymeComponent,
    RecibosPymeComponent,
    RegistroPolizaPymeComponent,
    CorreccionErroresPymeComponent,
    CotizadorPymeComponent,
    DetallesPolizaPymeComponent,
    DocumentosCorreccionPymeComponent,
    SolicitudesPymeComponent,
    StepCotizadorPymeComponent,
    DetalleSolicitudPymeComponent,
    EtiquetaSolicitudPymeComponent,
    ReasignarSolicitudPymeComponent,
    SolicitudPymeCreateComponent,
    ActivacionEmpleadoVidaComponent,
    PolizaPagosVidaComponent,
    PolizaRecibosVidaComponent,
    AdministracionPolizasVidaComponent,
    AutorizacionesProcesoVidaComponent,
    CrearAutorizacionVidaComponent,
    MostrarDocumentosAutorizacionVidaComponent,
    CreateEmisionVidaComponent,
    DatosProductoVidaComponent,
    InspeccionesVidaComponent,
    PagosVidaComponent,
    RecibosVidaComponent,
    RegistroPolizaVidaComponent,
    CorreccionErroresVidaComponent,
    CotizadorVidaComponent,
    DetallesPolizaVidaComponent,
    DocumentosCorreccionVidaComponent,
    SolicitudesVidaComponent,
    StepCotizadorVidaComponent,
    DetalleSolicitudVidaComponent,
    EtiquetaSolicitudVidaComponent,
    ReasignarSolicitudVidaComponent,
    SolicitudVidaCreateComponent,
    ViewerPolizaComponent
  ],
  imports: [
    CommonModule,
    NuevosProductosRoutingModule
  ]
})
export class NuevosProductosModule { }
