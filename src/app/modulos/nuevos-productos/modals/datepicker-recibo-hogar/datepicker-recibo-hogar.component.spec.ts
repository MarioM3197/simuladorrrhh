import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatepickerReciboHogarComponent } from './datepicker-recibo-hogar.component';

describe('DatepickerReciboHogarComponent', () => {
  let component: DatepickerReciboHogarComponent;
  let fixture: ComponentFixture<DatepickerReciboHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatepickerReciboHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerReciboHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
