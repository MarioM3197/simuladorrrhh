import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesPolizaHogarComponent } from './detalles-poliza-hogar.component';

describe('DetallesPolizaHogarComponent', () => {
  let component: DetallesPolizaHogarComponent;
  let fixture: ComponentFixture<DetallesPolizaHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesPolizaHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesPolizaHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
