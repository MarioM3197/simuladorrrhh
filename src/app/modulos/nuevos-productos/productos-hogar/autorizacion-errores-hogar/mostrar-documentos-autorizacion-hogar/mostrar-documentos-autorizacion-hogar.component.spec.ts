import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarDocumentosAutorizacionHogarComponent } from './mostrar-documentos-autorizacion-hogar.component';

describe('MostrarDocumentosAutorizacionHogarComponent', () => {
  let component: MostrarDocumentosAutorizacionHogarComponent;
  let fixture: ComponentFixture<MostrarDocumentosAutorizacionHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarDocumentosAutorizacionHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarDocumentosAutorizacionHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
