import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearAutorizacionHogarComponent } from './crear-autorizacion-hogar.component';

describe('CrearAutorizacionHogarComponent', () => {
  let component: CrearAutorizacionHogarComponent;
  let fixture: ComponentFixture<CrearAutorizacionHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearAutorizacionHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearAutorizacionHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
