import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesHogarComponent } from './solicitudes-hogar.component';

describe('SolicitudesHogarComponent', () => {
  let component: SolicitudesHogarComponent;
  let fixture: ComponentFixture<SolicitudesHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudesHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
