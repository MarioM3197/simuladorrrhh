import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudHogarCreateComponent } from './solicitud-hogar-create.component';

describe('SolicitudHogarCreateComponent', () => {
  let component: SolicitudHogarCreateComponent;
  let fixture: ComponentFixture<SolicitudHogarCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudHogarCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudHogarCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
