import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtiquetaSolicitudHogarComponent } from './etiqueta-solicitud-hogar.component';

describe('EtiquetaSolicitudHogarComponent', () => {
  let component: EtiquetaSolicitudHogarComponent;
  let fixture: ComponentFixture<EtiquetaSolicitudHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtiquetaSolicitudHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtiquetaSolicitudHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
