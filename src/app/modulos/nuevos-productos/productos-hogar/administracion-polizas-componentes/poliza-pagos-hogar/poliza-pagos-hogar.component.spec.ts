import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolizaPagosHogarComponent } from './poliza-pagos-hogar.component';

describe('PolizaPagosHogarComponent', () => {
  let component: PolizaPagosHogarComponent;
  let fixture: ComponentFixture<PolizaPagosHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolizaPagosHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolizaPagosHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
