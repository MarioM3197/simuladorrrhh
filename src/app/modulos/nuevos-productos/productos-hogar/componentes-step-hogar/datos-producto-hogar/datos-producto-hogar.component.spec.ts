import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosProductoHogarComponent } from './datos-producto-hogar.component';

describe('DatosProductoHogarComponent', () => {
  let component: DatosProductoHogarComponent;
  let fixture: ComponentFixture<DatosProductoHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosProductoHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosProductoHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
