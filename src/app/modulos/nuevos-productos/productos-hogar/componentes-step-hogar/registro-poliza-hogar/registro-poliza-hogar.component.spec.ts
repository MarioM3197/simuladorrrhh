import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroPolizaHogarComponent } from './registro-poliza-hogar.component';

describe('RegistroPolizaHogarComponent', () => {
  let component: RegistroPolizaHogarComponent;
  let fixture: ComponentFixture<RegistroPolizaHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroPolizaHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroPolizaHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
