import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosHogarComponent } from './pagos-hogar.component';

describe('PagosHogarComponent', () => {
  let component: PagosHogarComponent;
  let fixture: ComponentFixture<PagosHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
