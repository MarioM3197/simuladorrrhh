import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosCorreccionHogarComponent } from './documentos-correccion-hogar.component';

describe('DocumentosCorreccionHogarComponent', () => {
  let component: DocumentosCorreccionHogarComponent;
  let fixture: ComponentFixture<DocumentosCorreccionHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentosCorreccionHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosCorreccionHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
