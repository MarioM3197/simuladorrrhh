import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorreccionErroresHogarComponent } from './correccion-errores-hogar.component';

describe('CorreccionErroresHogarComponent', () => {
  let component: CorreccionErroresHogarComponent;
  let fixture: ComponentFixture<CorreccionErroresHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorreccionErroresHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorreccionErroresHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
