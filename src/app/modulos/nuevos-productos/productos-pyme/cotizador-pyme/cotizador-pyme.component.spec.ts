import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizadorPymeComponent } from './cotizador-pyme.component';

describe('CotizadorPymeComponent', () => {
  let component: CotizadorPymeComponent;
  let fixture: ComponentFixture<CotizadorPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizadorPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizadorPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
