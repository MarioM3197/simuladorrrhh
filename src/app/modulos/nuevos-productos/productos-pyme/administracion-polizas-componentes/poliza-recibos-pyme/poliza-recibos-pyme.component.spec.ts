import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolizaRecibosPymeComponent } from './poliza-recibos-pyme.component';

describe('PolizaRecibosPymeComponent', () => {
  let component: PolizaRecibosPymeComponent;
  let fixture: ComponentFixture<PolizaRecibosPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolizaRecibosPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolizaRecibosPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
