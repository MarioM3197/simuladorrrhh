import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolizaPagosPymeComponent } from './poliza-pagos-pyme.component';

describe('PolizaPagosPymeComponent', () => {
  let component: PolizaPagosPymeComponent;
  let fixture: ComponentFixture<PolizaPagosPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolizaPagosPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolizaPagosPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
