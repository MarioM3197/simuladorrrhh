import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivacionEmpleadoPymeComponent } from './activacion-empleado-pyme.component';

describe('ActivacionEmpleadoPymeComponent', () => {
  let component: ActivacionEmpleadoPymeComponent;
  let fixture: ComponentFixture<ActivacionEmpleadoPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivacionEmpleadoPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivacionEmpleadoPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
