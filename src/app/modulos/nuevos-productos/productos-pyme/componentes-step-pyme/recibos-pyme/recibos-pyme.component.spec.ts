import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibosPymeComponent } from './recibos-pyme.component';

describe('RecibosPymeComponent', () => {
  let component: RecibosPymeComponent;
  let fixture: ComponentFixture<RecibosPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecibosPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibosPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
