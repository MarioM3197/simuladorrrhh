import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroPolizaPymeComponent } from './registro-poliza-pyme.component';

describe('RegistroPolizaPymeComponent', () => {
  let component: RegistroPolizaPymeComponent;
  let fixture: ComponentFixture<RegistroPolizaPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroPolizaPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroPolizaPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
