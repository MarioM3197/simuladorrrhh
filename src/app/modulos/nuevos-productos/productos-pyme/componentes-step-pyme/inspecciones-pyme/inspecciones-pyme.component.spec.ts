import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspeccionesPymeComponent } from './inspecciones-pyme.component';

describe('InspeccionesPymeComponent', () => {
  let component: InspeccionesPymeComponent;
  let fixture: ComponentFixture<InspeccionesPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspeccionesPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspeccionesPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
