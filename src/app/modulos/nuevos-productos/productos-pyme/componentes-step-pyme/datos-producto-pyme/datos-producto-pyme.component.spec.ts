import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosProductoPymeComponent } from './datos-producto-pyme.component';

describe('DatosProductoPymeComponent', () => {
  let component: DatosProductoPymeComponent;
  let fixture: ComponentFixture<DatosProductoPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosProductoPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosProductoPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
