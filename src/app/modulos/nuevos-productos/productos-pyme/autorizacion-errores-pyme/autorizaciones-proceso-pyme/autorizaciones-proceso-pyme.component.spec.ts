import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizacionesProcesoPymeComponent } from './autorizaciones-proceso-pyme.component';

describe('AutorizacionesProcesoPymeComponent', () => {
  let component: AutorizacionesProcesoPymeComponent;
  let fixture: ComponentFixture<AutorizacionesProcesoPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizacionesProcesoPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizacionesProcesoPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
