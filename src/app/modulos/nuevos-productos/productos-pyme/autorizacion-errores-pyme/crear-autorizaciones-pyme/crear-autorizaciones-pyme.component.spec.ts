import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearAutorizacionesPymeComponent } from './crear-autorizaciones-pyme.component';

describe('CrearAutorizacionesPymeComponent', () => {
  let component: CrearAutorizacionesPymeComponent;
  let fixture: ComponentFixture<CrearAutorizacionesPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearAutorizacionesPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearAutorizacionesPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
