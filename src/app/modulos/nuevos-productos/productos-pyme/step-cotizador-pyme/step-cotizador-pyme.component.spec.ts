import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepCotizadorPymeComponent } from './step-cotizador-pyme.component';

describe('StepCotizadorPymeComponent', () => {
  let component: StepCotizadorPymeComponent;
  let fixture: ComponentFixture<StepCotizadorPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepCotizadorPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepCotizadorPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
