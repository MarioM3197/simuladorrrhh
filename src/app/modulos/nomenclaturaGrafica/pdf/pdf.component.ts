import {Component, OnInit} from '@angular/core';
import {checkIfClassIsExported} from '@angular/compiler-cli/src/ngtsc/typecheck/src/ts_util';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.scss']
})

export class PdfComponent implements OnInit {
  constructor() {
  }

  public fileType = 'jpg';
  public fileUrl: string;
  public lupita = true;


  ngOnInit(): void {
    this.fileUrl = this.getFileURL();
    console.log(this.fileUrl);
  }
  showPDF(){
    this.fileUrl = 'https://www.medigraphic.com/pdfs/medlab/myl-2008/myl087-8c.pdf#toolbar=0&scrollbar=0&navpanes=1';
    this.fileType = 'pdf';
    const myiFrame = document.getElementById('myiFrame');
    console.log('pato ' + myiFrame);
    if (myiFrame){
      console.log('a Sheldon le cabe en terabyte');
    // myiFrame.contentWindow.eval('document.addEventListener("contextmenu", function (e) {e.preventDefault();}, false)');
  } else{
      console.log('a Sheldon le cabe en gigabyte');
    }
  }
  showIMG(){
    this.fileUrl = 'https://i.blogs.es/274686/inecre/1366_2000.jpg';
    this.fileType = 'jpg';
  }
  ponerLupita(){
    if (this.lupita){
      this.magnify('myimage', 2);
      this.lupita = false;
    }

  }
  ponerLupita2(){
    console.log(this.lupita);
    if (this.lupita === false){
      this.lupita = true;
    }

  }



  getFileURL() {
    let fileurl = 'http://www.sld.cu/galerias/pdf/sitios/scap/introduccion_a_la_patologia.pdf';
    fileurl = 'https://i.blogs.es/274686/inecre/1366_2000.jpg';
    return fileurl + '?toolbar=0';
  }

  magnify(imgID, zoom) {
    let img;
    let glass;
    let w;
    let h;
    let bw;
    img = document.getElementById(imgID);
    const imageParent = document.getElementById('glassContainer');

    glass = document.createElement('div');
    glass.setAttribute('class', 'img-magnifier-glass');
    imageParent.insertBefore(glass, img);

    /* Set background properties for the magnifier glass: */
    glass.style.backgroundImage = 'url(\'' + img.src + '\')';
    glass.style.backgroundRepeat = 'no-repeat';
    glass.style.backgroundSize = (img.width * zoom) + 'px ' + (img.height * zoom) + 'px';
    bw = 3;
    w = glass.offsetWidth / 2;
    h = glass.offsetHeight / 2;
    glass.addEventListener('mousemove', moveMagnifier);
    img.addEventListener('mousemove', moveMagnifier);

    /*Para que funcione en touchscreens*/
    glass.addEventListener('touchmove', moveMagnifier);
    img.addEventListener('touchmove', moveMagnifier);

    function moveMagnifier(e) {
      let pos;
      let x;
      let y;
      e.preventDefault();
      pos = getCursorPos(e);
      x = pos.x;
      y = pos.y;
      if (x > img.width - (w / zoom)) {
        x = img.width - (w / zoom);
      }
      if (x < w / zoom) {
        x = w / zoom;
      }
      if (y > img.height - (h / zoom)) {
        y = img.height - (h / zoom);
      }
      if (y < h / zoom) {
        y = h / zoom;
      }
      glass.style.left = (x - w) + 'px';
      glass.style.top = (y - h) + 'px';
      glass.style.backgroundPosition = '-' + ((x * zoom) - w + bw) + 'px -' + ((y * zoom) - h + bw) + 'px';
    }

    function getCursorPos(e) {
      let a;
      let x = 0;
      let y = 0;
      e = e || window.event;
      a = img.getBoundingClientRect();
      x = e.pageX - a.left;
      y = e.pageY - a.top;
      x = x - window.pageXOffset;
      y = y - window.pageYOffset;
      return {x, y};
    }
  }
}
