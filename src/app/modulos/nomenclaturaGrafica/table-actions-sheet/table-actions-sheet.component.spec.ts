import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableActionsSheetComponent } from './table-actions-sheet.component';

describe('TableActionsSheetComponent', () => {
  let component: TableActionsSheetComponent;
  let fixture: ComponentFixture<TableActionsSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableActionsSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableActionsSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
