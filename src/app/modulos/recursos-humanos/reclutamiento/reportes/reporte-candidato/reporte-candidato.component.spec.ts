import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteCandidatoComponent } from './reporte-candidato.component';

describe('ReporteCandidatoComponent', () => {
  let component: ReporteCandidatoComponent;
  let fixture: ComponentFixture<ReporteCandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteCandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteCandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
