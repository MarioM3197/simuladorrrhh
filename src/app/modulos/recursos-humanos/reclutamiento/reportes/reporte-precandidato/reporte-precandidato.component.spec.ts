import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportePrecandidatoComponent } from './reporte-precandidato.component';

describe('ReportePrecandidatoComponent', () => {
  let component: ReportePrecandidatoComponent;
  let fixture: ComponentFixture<ReportePrecandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportePrecandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportePrecandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
