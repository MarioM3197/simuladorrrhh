import { Component, OnInit } from '@angular/core';
import {PrecandidatosService} from '../../../../../@core/data/services/rrhh/precandidatos/precandidatos.service';

@Component({
  selector: 'app-reporte-precandidato',
  templateUrl: './reporte-precandidato.component.html',
  styleUrls: ['./reporte-precandidato.component.scss']
})
export class ReportePrecandidatoComponent implements OnInit {

  constructor(private _PrecandidatosService: PrecandidatosService) { }

  ngOnInit(): void {
  }

}
