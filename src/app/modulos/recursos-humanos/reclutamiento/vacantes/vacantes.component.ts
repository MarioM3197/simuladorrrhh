import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {VacantesService} from '../../../../@core/services/reclutamiento/vacantes.service';
import {Vacante} from '../../../../@core/interfaces/reclutamiento/vacantes/vacante';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-vacantes',
  templateUrl: './vacantes.component.html',
  styleUrls: ['./vacantes.component.scss']
})
export class VacantesComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  displayedColumns = ['nombre', 'subarea', 'sede' , 'puesto', 'estadoActual'];
  dataSource: MatTableDataSource<Vacante>;

  constructor(private bottomSheet: MatBottomSheet,
              private vacantesService: VacantesService) { }

  ngOnInit(): void {
    this.getVacantes();
  }

  getVacantes() {
    this.vacantesService.get().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
    });
  }

  iniciarProceso() {
    console.log('Proceso listo para comenzar');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
