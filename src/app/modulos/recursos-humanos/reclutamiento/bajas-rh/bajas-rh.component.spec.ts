import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BajasRHComponent } from './bajas-rh.component';

describe('BajasRHComponent', () => {
  let component: BajasRHComponent;
  let fixture: ComponentFixture<BajasRHComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BajasRHComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BajasRHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
