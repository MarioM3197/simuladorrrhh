import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosBajaRhComponent } from './datos-baja-rh.component';

describe('DatosBajaRhComponent', () => {
  let component: DatosBajaRhComponent;
  let fixture: ComponentFixture<DatosBajaRhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosBajaRhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosBajaRhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
