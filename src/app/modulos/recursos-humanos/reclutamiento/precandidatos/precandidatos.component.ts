import { Component, OnInit } from '@angular/core';
import { PrecandidatosService} from '../../../../@core/data/services/rrhh/precandidatos/precandidatos.service';

@Component({
  selector: 'app-precandidatos',
  templateUrl: './precandidatos.component.html',
  styleUrls: ['./precandidatos.component.scss']
})
export class PrecandidatosComponent implements OnInit {

  constructor(private _PrecandidatosService: PrecandidatosService) {

  }

  ngOnInit(): void {
  }

}
