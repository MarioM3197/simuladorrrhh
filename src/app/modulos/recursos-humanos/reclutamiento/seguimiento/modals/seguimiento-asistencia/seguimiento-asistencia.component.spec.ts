import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguimientoAsistenciaComponent } from './seguimiento-asistencia.component';

describe('SeguimientoAsistenciaComponent', () => {
  let component: SeguimientoAsistenciaComponent;
  let fixture: ComponentFixture<SeguimientoAsistenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguimientoAsistenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguimientoAsistenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
