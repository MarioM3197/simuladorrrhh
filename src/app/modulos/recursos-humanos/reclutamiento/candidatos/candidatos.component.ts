import { Component, OnInit } from '@angular/core';
import {CandidatosService} from '../../../../@core/data/services/rrhh/candidatos/candidatos.service';

@Component({
  selector: 'app-candidatos',
  templateUrl: './candidatos.component.html',
  styleUrls: ['./candidatos.component.scss']
})
export class CandidatosComponent implements OnInit {

  constructor(private _CandidatosService: CandidatosService) { }

  ngOnInit(): void {
  }

}
