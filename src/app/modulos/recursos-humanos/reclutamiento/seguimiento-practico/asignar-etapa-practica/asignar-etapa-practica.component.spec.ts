import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarEtapaPracticaComponent } from './asignar-etapa-practica.component';

describe('AsignarEtapaPracticaComponent', () => {
  let component: AsignarEtapaPracticaComponent;
  let fixture: ComponentFixture<AsignarEtapaPracticaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarEtapaPracticaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarEtapaPracticaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
