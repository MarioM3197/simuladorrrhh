import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarVacanteComponent } from './solicitar-vacante.component';

describe('SolicitarVacanteComponent', () => {
  let component: SolicitarVacanteComponent;
  let fixture: ComponentFixture<SolicitarVacanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitarVacanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarVacanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
