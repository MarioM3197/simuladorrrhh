import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {VacantesComponent} from './reclutamiento/vacantes/vacantes.component';


const routes: Routes = [
  {
    path: 'vacantes',
    component: VacantesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecursosHumanosRoutingModule { }
