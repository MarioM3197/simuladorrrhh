import { Component, OnInit } from '@angular/core';
import {PreEmpleadosService} from '../../../../@core/data/services/rrhh/pre-empleados/pre-empleados.service';

@Component({
  selector: 'app-pre-empleados',
  templateUrl: './pre-empleados.component.html',
  styleUrls: ['./pre-empleados.component.scss']
})
export class PreEmpleadosComponent implements OnInit {

  constructor(private _PreEmpleadosService: PreEmpleadosService) { }

  ngOnInit(): void {
  }

}
