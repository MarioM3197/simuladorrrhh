import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosPreempleadoComponent } from './datos-preempleado.component';

describe('DatosPreempleadoComponent', () => {
  let component: DatosPreempleadoComponent;
  let fixture: ComponentFixture<DatosPreempleadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosPreempleadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosPreempleadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
