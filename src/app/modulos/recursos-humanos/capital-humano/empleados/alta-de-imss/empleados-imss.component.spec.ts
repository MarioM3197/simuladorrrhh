import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleadosImssComponent } from './empleados-imss.component';

describe('EmpleadosImssComponent', () => {
  let component: EmpleadosImssComponent;
  let fixture: ComponentFixture<EmpleadosImssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpleadosImssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleadosImssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
