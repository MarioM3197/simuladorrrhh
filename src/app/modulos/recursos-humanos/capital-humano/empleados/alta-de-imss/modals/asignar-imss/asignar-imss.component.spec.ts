import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarImssComponent } from './asignar-imss.component';

describe('AsignarImssComponent', () => {
  let component: AsignarImssComponent;
  let fixture: ComponentFixture<AsignarImssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarImssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarImssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
