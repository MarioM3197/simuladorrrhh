import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactivarEmpleadoComponent } from './reactivar-empleado.component';

describe('ReactivarEmpleadoComponent', () => {
  let component: ReactivarEmpleadoComponent;
  let fixture: ComponentFixture<ReactivarEmpleadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactivarEmpleadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactivarEmpleadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
