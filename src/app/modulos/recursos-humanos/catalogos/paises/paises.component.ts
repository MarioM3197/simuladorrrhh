import { Component, OnInit } from '@angular/core';
import {PaisesService} from '../../../../@core/data/services/rrhh/catalogos-RH/paises/paises.service';

@Component({
  selector: 'app-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.scss']
})
export class PaisesComponent implements OnInit {

  constructor(private _PaisesService: PaisesService) { }

  ngOnInit(): void {
  }

}
