import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetenciasCandidatoComponent } from './competencias-candidato.component';

describe('CompetenciasCandidatoComponent', () => {
  let component: CompetenciasCandidatoComponent;
  let fixture: ComponentFixture<CompetenciasCandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompetenciasCandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetenciasCandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
