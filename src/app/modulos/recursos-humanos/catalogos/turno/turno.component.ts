import { Component, OnInit } from '@angular/core';
import {TurnoService} from '../../../../@core/data/services/rrhh/catalogos-RH/turno/turno.service';

@Component({
  selector: 'app-turno',
  templateUrl: './turno.component.html',
  styleUrls: ['./turno.component.scss']
})
export class TurnoComponent implements OnInit {

  constructor(private _TurnoService: TurnoService) { }

  ngOnInit(): void {
  }

}
