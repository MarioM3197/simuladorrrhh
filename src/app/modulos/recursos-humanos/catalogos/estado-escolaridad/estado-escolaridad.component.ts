import { Component, OnInit } from '@angular/core';
import {EstadoEscolaridadService} from '../../../../@core/data/services/rrhh/catalogos-RH/estado-escolaridad/estado-escolaridad.service';

@Component({
  selector: 'app-estado-escolaridad',
  templateUrl: './estado-escolaridad.component.html',
  styleUrls: ['./estado-escolaridad.component.scss']
})
export class EstadoEscolaridadComponent implements OnInit {

  constructor(private _EstadoEscolaridadService: EstadoEscolaridadService) { }

  ngOnInit(): void {
  }

}
