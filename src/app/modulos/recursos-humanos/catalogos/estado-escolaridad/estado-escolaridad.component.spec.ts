import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoEscolaridadComponent } from './estado-escolaridad.component';

describe('EstadoEscolaridadComponent', () => {
  let component: EstadoEscolaridadComponent;
  let fixture: ComponentFixture<EstadoEscolaridadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoEscolaridadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoEscolaridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
