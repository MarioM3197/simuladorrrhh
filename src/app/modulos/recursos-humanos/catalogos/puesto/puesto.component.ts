import { Component, OnInit } from '@angular/core';
import {PuestoService} from '../../../../@core/data/services/rrhh/catalogos-RH/puesto/puesto.service';

@Component({
  selector: 'app-puesto',
  templateUrl: './puesto.component.html',
  styleUrls: ['./puesto.component.scss']
})
export class PuestoComponent implements OnInit {

  constructor(private _PuestoService: PuestoService) { }

  ngOnInit(): void {
  }

}
