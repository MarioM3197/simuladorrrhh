import { Component, OnInit } from '@angular/core';
import {EscolaridadService} from '../../../../@core/data/services/rrhh/catalogos-RH/escolaridad/escolaridad.service';

@Component({
  selector: 'app-escolaridad',
  templateUrl: './escolaridad.component.html',
  styleUrls: ['./escolaridad.component.scss']
})
export class EscolaridadComponent implements OnInit {

  constructor(private _EscolaridadService: EscolaridadService) { }

  ngOnInit(): void {
  }

}
