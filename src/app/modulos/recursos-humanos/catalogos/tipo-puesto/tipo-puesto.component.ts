import { Component, OnInit } from '@angular/core';
import {TipoPuestoService} from '../../../../@core/data/services/rrhh/catalogos-RH/tipo-puesto/tipo-puesto.service';

@Component({
  selector: 'app-tipo-puesto',
  templateUrl: './tipo-puesto.component.html',
  styleUrls: ['./tipo-puesto.component.scss']
})
export class TipoPuestoComponent implements OnInit {

  constructor(private _TipoPuestoService: TipoPuestoService) { }

  ngOnInit(): void {
  }

}
