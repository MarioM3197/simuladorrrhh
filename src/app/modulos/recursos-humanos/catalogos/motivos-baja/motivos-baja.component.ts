import { Component, OnInit } from '@angular/core';
import {MotivosBajaService} from '../../../../@core/data/services/rrhh/catalogos-RH/motivos-baja/motivos-baja.service';

@Component({
  selector: 'app-motivos-baja',
  templateUrl: './motivos-baja.component.html',
  styleUrls: ['./motivos-baja.component.scss']
})
export class MotivosBajaComponent implements OnInit {

  constructor(private _MotivosBajaService: MotivosBajaService) { }

  ngOnInit(): void {
  }

}
