import { Component, OnInit } from '@angular/core';
import {BolsaTrabajoService} from '../../../../@core/data/services/rrhh/catalogos-RH/bolsa-trabajo/bolsa-trabajo.service';

@Component({
  selector: 'app-bolsa-trabajo',
  templateUrl: './bolsa-trabajo.component.html',
  styleUrls: ['./bolsa-trabajo.component.scss']
})
export class BolsaTrabajoComponent implements OnInit {

  constructor(private _BolsaTrabajoService: BolsaTrabajoService) { }

  ngOnInit(): void {
  }

}
