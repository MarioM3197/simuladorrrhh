import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosPrecandidatoComponent } from './datos-precandidato.component';

describe('DatosPrecandidatoComponent', () => {
  let component: DatosPrecandidatoComponent;
  let fixture: ComponentFixture<DatosPrecandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosPrecandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosPrecandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
