import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosSeguimientoPracticoComponent } from './datos-seguimiento-practico.component';

describe('DatosSeguimientoPracticoComponent', () => {
  let component: DatosSeguimientoPracticoComponent;
  let fixture: ComponentFixture<DatosSeguimientoPracticoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosSeguimientoPracticoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosSeguimientoPracticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
