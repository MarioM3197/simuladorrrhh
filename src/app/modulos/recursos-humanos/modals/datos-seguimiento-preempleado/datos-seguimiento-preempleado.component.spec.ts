import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosSeguimientoPreempleadoComponent } from './datos-seguimiento-preempleado.component';

describe('DatosSeguimientoPreempleadoComponent', () => {
  let component: DatosSeguimientoPreempleadoComponent;
  let fixture: ComponentFixture<DatosSeguimientoPreempleadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosSeguimientoPreempleadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosSeguimientoPreempleadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
