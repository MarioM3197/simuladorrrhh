import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckDocumentosComponent } from './check-documentos.component';

describe('CheckDocumentosComponent', () => {
  let component: CheckDocumentosComponent;
  let fixture: ComponentFixture<CheckDocumentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckDocumentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
