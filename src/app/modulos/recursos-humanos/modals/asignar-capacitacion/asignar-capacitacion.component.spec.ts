import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarCapacitacionComponent } from './asignar-capacitacion.component';

describe('AsignarCapacitacionComponent', () => {
  let component: AsignarCapacitacionComponent;
  let fixture: ComponentFixture<AsignarCapacitacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarCapacitacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarCapacitacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
