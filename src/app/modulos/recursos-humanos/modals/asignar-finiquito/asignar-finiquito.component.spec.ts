import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarFiniquitoComponent } from './asignar-finiquito.component';

describe('AsignarFiniquitoComponent', () => {
  let component: AsignarFiniquitoComponent;
  let fixture: ComponentFixture<AsignarFiniquitoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarFiniquitoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarFiniquitoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
