import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogosSoporteRoutingModule } from './catalogos-soporte-routing.module';
import { CatalogosSoporteComponent } from './catalogos-soporte.component';
import { EstadoInventarioComponent } from './estado-inventario/estado-inventario.component';
import { ObservacionesInventarioComponent } from './observaciones-inventario/observaciones-inventario.component';
import { MarcasComponent } from './marcas/marcas.component';
import { ProcesadorComponent } from './procesador/procesador.component';
import {EstadoInventarioService} from '../../../@core/data/services/soporte/catalogos/estado-inventario.service';
import {MarcaService} from '../../../@core/data/services/soporte/catalogos/marca.service';
import {ObservacionesInventarioService} from '../../../@core/data/services/soporte/catalogos/observaciones-inventario.service';
import {ProcesadorService} from '../../../@core/data/services/soporte/catalogos/procesador.service';


@NgModule({
  declarations: [
    CatalogosSoporteComponent,
    EstadoInventarioComponent,
    ObservacionesInventarioComponent,
    MarcasComponent,
    ProcesadorComponent
  ],
  imports: [
    CommonModule,
    CatalogosSoporteRoutingModule
  ],

  providers: [
    EstadoInventarioService,
    MarcaService,
    ObservacionesInventarioService,
    ProcesadorService
  ],
})
export class CatalogosSoporteModule { }
