import { Component, OnInit } from '@angular/core';
import {ProcesadorService} from '../../../../@core/data/services/soporte/catalogos/procesador.service';

@Component({
  selector: 'app-procesador',
  templateUrl: './procesador.component.html',
  styleUrls: ['./procesador.component.scss']
})
export class ProcesadorComponent implements OnInit {

  constructor(private _ProcesadorService: ProcesadorService) { }

  ngOnInit(): void {
  }

}
