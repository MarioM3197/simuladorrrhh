import { Component, OnInit } from '@angular/core';
import {MarcaService} from '../../../../@core/data/services/soporte/catalogos/marca.service';

@Component({
  selector: 'app-marcas',
  templateUrl: './marcas.component.html',
  styleUrls: ['./marcas.component.scss']
})
export class MarcasComponent implements OnInit {

  constructor(private _MarcaService: MarcaService) { }

  ngOnInit(): void {
  }

}
