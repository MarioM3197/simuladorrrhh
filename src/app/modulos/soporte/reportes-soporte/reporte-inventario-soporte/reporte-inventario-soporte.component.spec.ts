import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteInventarioSoporteComponent } from './reporte-inventario-soporte.component';

describe('ReporteInventarioSoporteComponent', () => {
  let component: ReporteInventarioSoporteComponent;
  let fixture: ComponentFixture<ReporteInventarioSoporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteInventarioSoporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteInventarioSoporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
