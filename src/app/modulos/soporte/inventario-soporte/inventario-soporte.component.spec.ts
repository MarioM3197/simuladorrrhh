import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventarioSoporteComponent } from './inventario-soporte.component';

describe('InventarioSoporteComponent', () => {
  let component: InventarioSoporteComponent;
  let fixture: ComponentFixture<InventarioSoporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventarioSoporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventarioSoporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
