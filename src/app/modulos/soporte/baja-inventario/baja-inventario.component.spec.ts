import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BajaInventarioComponent } from './baja-inventario.component';

describe('BajaInventarioComponent', () => {
  let component: BajaInventarioComponent;
  let fixture: ComponentFixture<BajaInventarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BajaInventarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BajaInventarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
