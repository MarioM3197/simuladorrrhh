import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventarioRedesComponent } from './inventario-redes.component';

describe('InventarioRedesComponent', () => {
  let component: InventarioRedesComponent;
  let fixture: ComponentFixture<InventarioRedesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventarioRedesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventarioRedesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
