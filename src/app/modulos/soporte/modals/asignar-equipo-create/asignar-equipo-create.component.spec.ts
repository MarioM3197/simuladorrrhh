import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarEquipoCreateComponent } from './asignar-equipo-create.component';

describe('AsignarEquipoCreateComponent', () => {
  let component: AsignarEquipoCreateComponent;
  let fixture: ComponentFixture<AsignarEquipoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarEquipoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarEquipoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
