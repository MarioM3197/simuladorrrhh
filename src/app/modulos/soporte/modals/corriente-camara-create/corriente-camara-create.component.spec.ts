import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorrienteCamaraCreateComponent } from './corriente-camara-create.component';

describe('CorrienteCamaraCreateComponent', () => {
  let component: CorrienteCamaraCreateComponent;
  let fixture: ComponentFixture<CorrienteCamaraCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorrienteCamaraCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorrienteCamaraCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
