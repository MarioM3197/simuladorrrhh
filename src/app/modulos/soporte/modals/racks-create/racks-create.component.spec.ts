import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RacksCreateComponent } from './racks-create.component';

describe('RacksCreateComponent', () => {
  let component: RacksCreateComponent;
  let fixture: ComponentFixture<RacksCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RacksCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RacksCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
