import { Component, OnInit } from '@angular/core';
import {TabletService} from '../../../../@core/data/services/soporte/inventario-soporte/tablet.service';

@Component({
  selector: 'app-tablet-create',
  templateUrl: './tablet-create.component.html',
  styleUrls: ['./tablet-create.component.scss']
})
export class TabletCreateComponent implements OnInit {

  constructor(private _TabletService: TabletService) { }

  ngOnInit(): void {
  }

}
