import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabletCreateComponent } from './tablet-create.component';

describe('TabletCreateComponent', () => {
  let component: TabletCreateComponent;
  let fixture: ComponentFixture<TabletCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabletCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabletCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
