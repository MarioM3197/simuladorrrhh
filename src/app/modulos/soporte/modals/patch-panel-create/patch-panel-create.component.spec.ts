import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatchPanelCreateComponent } from './patch-panel-create.component';

describe('PatchPanelCreateComponent', () => {
  let component: PatchPanelCreateComponent;
  let fixture: ComponentFixture<PatchPanelCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatchPanelCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatchPanelCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
