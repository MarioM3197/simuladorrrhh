import { Component, OnInit } from '@angular/core';
import {TarjetaRedService} from '../../../../@core/data/services/soporte/inventario-soporte/tarjeta-red.service';

@Component({
  selector: 'app-tarjeta-create',
  templateUrl: './tarjeta-create.component.html',
  styleUrls: ['./tarjeta-create.component.scss']
})
export class TarjetaCreateComponent implements OnInit {

  constructor(private _TarjetaRedService: TarjetaRedService) { }

  ngOnInit(): void {
  }

}
