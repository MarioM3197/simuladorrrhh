import { Component, OnInit } from '@angular/core';
import {ServidoresService} from '../../../../@core/data/services/soporte/inventario-soporte/servidores.service';

@Component({
  selector: 'app-servidores-create',
  templateUrl: './servidores-create.component.html',
  styleUrls: ['./servidores-create.component.scss']
})
export class ServidoresCreateComponent implements OnInit {

  constructor(private _ServidoresService: ServidoresService) { }

  ngOnInit(): void {
  }

}
