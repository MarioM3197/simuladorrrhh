import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelefonoIpCreateComponent } from './telefono-ip-create.component';

describe('TelefonoIpCreateComponent', () => {
  let component: TelefonoIpCreateComponent;
  let fixture: ComponentFixture<TelefonoIpCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelefonoIpCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelefonoIpCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
