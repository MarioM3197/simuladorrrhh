import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchsCreateComponent } from './switchs-create.component';

describe('SwitchsCreateComponent', () => {
  let component: SwitchsCreateComponent;
  let fixture: ComponentFixture<SwitchsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwitchsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
