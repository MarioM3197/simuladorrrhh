import { Component, OnInit } from '@angular/core';
import {SwitchsService} from '../../../../@core/data/services/soporte/inventario-soporte/switchs.service';

@Component({
  selector: 'app-switchs-create',
  templateUrl: './switchs-create.component.html',
  styleUrls: ['./switchs-create.component.scss']
})
export class SwitchsCreateComponent implements OnInit {

  constructor(private _SwitchsService: SwitchsService) { }

  ngOnInit(): void {
  }

}
