import { Component, OnInit } from '@angular/core';
import {CpuService} from '../../../../@core/data/services/soporte/inventario-soporte/cpu.service';

@Component({
  selector: 'app-cpu-create',
  templateUrl: './cpu-create.component.html',
  styleUrls: ['./cpu-create.component.scss']
})
export class CpuCreateComponent implements OnInit {

  constructor(private _CpuService: CpuService) { }

  ngOnInit(): void {
  }

}
