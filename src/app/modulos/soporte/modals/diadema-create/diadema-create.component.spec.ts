import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiademaCreateComponent } from './diadema-create.component';

describe('DiademaCreateComponent', () => {
  let component: DiademaCreateComponent;
  let fixture: ComponentFixture<DiademaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiademaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiademaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
