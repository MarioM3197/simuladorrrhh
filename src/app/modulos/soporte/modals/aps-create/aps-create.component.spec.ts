import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApsCreateComponent } from './aps-create.component';

describe('ApsCreateComponent', () => {
  let component: ApsCreateComponent;
  let fixture: ComponentFixture<ApsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
