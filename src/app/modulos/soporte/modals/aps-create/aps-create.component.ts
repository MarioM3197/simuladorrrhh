import { Component, OnInit } from '@angular/core';
import {ApsService} from '../../../../@core/data/services/soporte/inventario-soporte/aps.service';

@Component({
  selector: 'app-aps-create',
  templateUrl: './aps-create.component.html',
  styleUrls: ['./aps-create.component.scss']
})
export class ApsCreateComponent implements OnInit {

  constructor(private _ApsService: ApsService) { }

  ngOnInit(): void {
  }

}
