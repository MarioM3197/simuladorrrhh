import { Component, OnInit } from '@angular/core';
import {DvrService} from '../../../../@core/data/services/soporte/inventario-soporte/dvr.service';

@Component({
  selector: 'app-dvr-create',
  templateUrl: './dvr-create.component.html',
  styleUrls: ['./dvr-create.component.scss']
})
export class DvrCreateComponent implements OnInit {

  constructor(private _DvrService: DvrService) { }

  ngOnInit(): void {
  }

}
