import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChromeBitCreateComponent } from './chrome-bit-create.component';

describe('ChromeBitCreateComponent', () => {
  let component: ChromeBitCreateComponent;
  let fixture: ComponentFixture<ChromeBitCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChromeBitCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChromeBitCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
