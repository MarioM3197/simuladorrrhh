import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MouseCreateComponent } from './mouse-create.component';

describe('MouseCreateComponent', () => {
  let component: MouseCreateComponent;
  let fixture: ComponentFixture<MouseCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MouseCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MouseCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
