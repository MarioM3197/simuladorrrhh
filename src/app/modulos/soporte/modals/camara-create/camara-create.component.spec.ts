import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamaraCreateComponent } from './camara-create.component';

describe('CamaraCreateComponent', () => {
  let component: CamaraCreateComponent;
  let fixture: ComponentFixture<CamaraCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamaraCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamaraCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
