import { Component, OnInit } from '@angular/core';
import {MonitorService} from '../../../../@core/data/services/soporte/inventario-soporte/monitor.service';

@Component({
  selector: 'app-monitor-create',
  templateUrl: './monitor-create.component.html',
  styleUrls: ['./monitor-create.component.scss']
})
export class MonitorCreateComponent implements OnInit {

  constructor(private _MonitorService: MonitorService) { }

  ngOnInit(): void {
  }

}
