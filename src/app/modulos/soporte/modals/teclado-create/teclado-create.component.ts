import { Component, OnInit } from '@angular/core';
import {TecladoService} from '../../../../@core/data/services/soporte/inventario-soporte/teclado.service';

@Component({
  selector: 'app-teclado-create',
  templateUrl: './teclado-create.component.html',
  styleUrls: ['./teclado-create.component.scss']
})
export class TecladoCreateComponent implements OnInit {

  constructor(private _TecladoService: TecladoService) { }

  ngOnInit(): void {
  }

}
