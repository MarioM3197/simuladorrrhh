import { Component, OnInit } from '@angular/core';
import {DiscoDuroExternoService} from '../../../../@core/data/services/soporte/inventario-soporte/disco-duro-externo.service';

@Component({
  selector: 'app-disco-duro-externo-create',
  templateUrl: './disco-duro-externo-create.component.html',
  styleUrls: ['./disco-duro-externo-create.component.scss']
})
export class DiscoDuroExternoCreateComponent implements OnInit {

  constructor(private _DiscoDuroExternoService: DiscoDuroExternoService) { }

  ngOnInit(): void {
  }

}
