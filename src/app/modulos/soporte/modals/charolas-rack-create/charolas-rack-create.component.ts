import { Component, OnInit } from '@angular/core';
import {CharolasRackService} from '../../../../@core/data/services/soporte/inventario-soporte/charolas-rack.service';

@Component({
  selector: 'app-charolas-rack-create',
  templateUrl: './charolas-rack-create.component.html',
  styleUrls: ['./charolas-rack-create.component.scss']
})
export class CharolasRackCreateComponent implements OnInit {

  constructor(private _CharolasRackService: CharolasRackService) { }

  ngOnInit(): void {
  }

}
