import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiometricosCreateComponent } from './biometricos-create.component';

describe('BiometricosCreateComponent', () => {
  let component: BiometricosCreateComponent;
  let fixture: ComponentFixture<BiometricosCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiometricosCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiometricosCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
