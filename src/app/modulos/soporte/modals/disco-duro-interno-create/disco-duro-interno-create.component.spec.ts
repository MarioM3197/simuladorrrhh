import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscoDuroInternoCreateComponent } from './disco-duro-interno-create.component';

describe('DiscoDuroInternoCreateComponent', () => {
  let component: DiscoDuroInternoCreateComponent;
  let fixture: ComponentFixture<DiscoDuroInternoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscoDuroInternoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscoDuroInternoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
