import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SoporteRoutingModule } from './soporte-routing.module';
import { SoporteComponent } from './soporte.component';
import { InventarioComponent } from './inventario/inventario.component';
import { InventarioRedesComponent } from './inventario-redes/inventario-redes.component';
import { InventarioSoporteComponent } from './inventario-soporte/inventario-soporte.component';
import { BajaInventarioComponent } from './baja-inventario/baja-inventario.component';
import { BajaInventarioRedesComponent } from './baja-inventario-redes/baja-inventario-redes.component';
import { BajaInventarioSoporteComponent } from './baja-inventario-soporte/baja-inventario-soporte.component';
import { ReporteInventarioUsuarioComponent } from './reportes-soporte/reporte-inventario-usuario/reporte-inventario-usuario.component';
import { ReporteInventarioRedesComponent } from './reportes-soporte/reporte-inventario-redes/reporte-inventario-redes.component';
import { ReporteInventarioSoporteComponent } from './reportes-soporte/reporte-inventario-soporte/reporte-inventario-soporte.component';
import { ApsCreateComponent } from './modals/aps-create/aps-create.component';
import { AsignarEquipoComponent } from './modals/asignar-equipo/asignar-equipo.component';
import { AsignarEquipoCreateComponent } from './modals/asignar-equipo-create/asignar-equipo-create.component';
import { BarraMulticontactoCreateComponent } from './modals/barra-multicontacto-create/barra-multicontacto-create.component';
import { BiometricosCreateComponent } from './modals/biometricos-create/biometricos-create.component';
import { CamaraCreateComponent } from './modals/camara-create/camara-create.component';
import { CharolasRackCreateComponent } from './modals/charolas-rack-create/charolas-rack-create.component';
import { ChromeBitCreateComponent } from './modals/chrome-bit-create/chrome-bit-create.component';
import { CorrienteCamaraCreateComponent } from './modals/corriente-camara-create/corriente-camara-create.component';
import { CpuCreateComponent } from './modals/cpu-create/cpu-create.component';
import { DiademaCreateComponent } from './modals/diadema-create/diadema-create.component';
import { DiscoDuroExternoCreateComponent } from './modals/disco-duro-externo-create/disco-duro-externo-create.component';
import { DiscoDuroInternoCreateComponent } from './modals/disco-duro-interno-create/disco-duro-interno-create.component';
import { DvrCreateComponent } from './modals/dvr-create/dvr-create.component';
import { LaptopCreateComponent } from './modals/laptop-create/laptop-create.component';
import { MonitorCreateComponent } from './modals/monitor-create/monitor-create.component';
import { MouseCreateComponent } from './modals/mouse-create/mouse-create.component';
import { PatchPanelCreateComponent } from './modals/patch-panel-create/patch-panel-create.component';
import { RacksCreateComponent } from './modals/racks-create/racks-create.component';
import { ServidoresCreateComponent } from './modals/servidores-create/servidores-create.component';
import { SwitchsCreateComponent } from './modals/switchs-create/switchs-create.component';
import { TabletCreateComponent } from './modals/tablet-create/tablet-create.component';
import { TarjetaCreateComponent } from './modals/tarjeta-create/tarjeta-create.component';
import { TecladoCreateComponent } from './modals/teclado-create/teclado-create.component';
import { TelefonoIpCreateComponent } from './modals/telefono-ip-create/telefono-ip-create.component';
import { VerDetalleComponent } from './modals/ver-detalle/ver-detalle.component';
import {ApsService} from '../../@core/data/services/soporte/inventario-soporte/aps.service';
import {BarrasMulticontactoService} from '../../@core/data/services/soporte/inventario-soporte/barras-multicontacto.service';
import {BiometricosService} from '../../@core/data/services/soporte/inventario-soporte/biometricos.service';
import {CamarasService} from '../../@core/data/services/soporte/inventario-soporte/camaras.service';
import {CharolasRackService} from '../../@core/data/services/soporte/inventario-soporte/charolas-rack.service';
import {ChromeBitService} from '../../@core/data/services/soporte/inventario-soporte/chrome-bit.service';
import {CpuService} from '../../@core/data/services/soporte/inventario-soporte/cpu.service';
import {DiademaService} from '../../@core/data/services/soporte/inventario-soporte/diadema.service';
import {DiscoDuroExternoService} from '../../@core/data/services/soporte/inventario-soporte/disco-duro-externo.service';
import {DiscoDuroInternoService} from '../../@core/data/services/soporte/inventario-soporte/disco-duro-interno.service';
import {DvrService} from '../../@core/data/services/soporte/inventario-soporte/dvr.service';
import {LaptopService} from '../../@core/data/services/soporte/inventario-soporte/laptop.service';
import {MouseService} from '../../@core/data/services/soporte/inventario-soporte/mouse.service';
import {MonitorService} from '../../@core/data/services/soporte/inventario-soporte/monitor.service';
import {PatchPanelService} from '../../@core/data/services/soporte/inventario-soporte/patch-panel.service';
import {RacksService} from '../../@core/data/services/soporte/inventario-soporte/racks.service';
import {ServidoresService} from '../../@core/data/services/soporte/inventario-soporte/servidores.service';
import {SwitchsService} from '../../@core/data/services/soporte/inventario-soporte/switchs.service';
import {TabletService} from '../../@core/data/services/soporte/inventario-soporte/tablet.service';
import {TarjetaRedService} from '../../@core/data/services/soporte/inventario-soporte/tarjeta-red.service';
import {TecladoService} from '../../@core/data/services/soporte/inventario-soporte/teclado.service';
import {TelefonoIpService} from '../../@core/data/services/soporte/inventario-soporte/telefono-ip.service';



// @ts-ignore
// @ts-ignore
// @ts-ignore
@NgModule({
  declarations: [
    SoporteComponent,
    InventarioComponent,
    InventarioRedesComponent,
    InventarioSoporteComponent,
    BajaInventarioComponent,
    BajaInventarioRedesComponent,
    BajaInventarioSoporteComponent,
    ReporteInventarioUsuarioComponent,
    ReporteInventarioRedesComponent,
    ReporteInventarioSoporteComponent,
    ApsCreateComponent,
    AsignarEquipoComponent,
    AsignarEquipoCreateComponent,
    BarraMulticontactoCreateComponent,
    BiometricosCreateComponent,
    CamaraCreateComponent,
    CharolasRackCreateComponent,
    ChromeBitCreateComponent,
    CorrienteCamaraCreateComponent,
    CpuCreateComponent,
    DiademaCreateComponent,
    DiscoDuroExternoCreateComponent,
    DiscoDuroInternoCreateComponent,
    DvrCreateComponent,
    LaptopCreateComponent,
    MonitorCreateComponent,
    MouseCreateComponent,
    PatchPanelCreateComponent,
    RacksCreateComponent,
    ServidoresCreateComponent,
    SwitchsCreateComponent,
    TabletCreateComponent,
    TarjetaCreateComponent,
    TecladoCreateComponent,
    TelefonoIpCreateComponent,
    VerDetalleComponent

  ],
  imports: [
    CommonModule,
    SoporteRoutingModule

  ],

  providers: [
    ApsService,
    BarrasMulticontactoService,
    BiometricosService,
    CamarasService,
    CharolasRackService,
    ChromeBitService,
    CpuService,
    DiademaService,
    DiscoDuroExternoService,
    DiscoDuroInternoService,
    DvrService,
    LaptopService,
    MonitorService,
    MouseService,
    PatchPanelService,
    RacksService,
    ServidoresService,
    SwitchsService,
    TabletService,
    TarjetaRedService,
    TecladoService,
    TelefonoIpService
  ],
})
export class SoporteModule { }
