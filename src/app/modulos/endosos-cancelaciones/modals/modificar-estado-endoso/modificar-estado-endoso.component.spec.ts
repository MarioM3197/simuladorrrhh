import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarEstadoEndosoComponent } from './modificar-estado-endoso.component';

describe('ModificarEstadoEndosoComponent', () => {
  let component: ModificarEstadoEndosoComponent;
  let fixture: ComponentFixture<ModificarEstadoEndosoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarEstadoEndosoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarEstadoEndosoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
