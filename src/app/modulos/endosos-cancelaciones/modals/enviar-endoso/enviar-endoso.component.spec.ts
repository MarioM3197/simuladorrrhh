import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnviarEndosoComponent } from './enviar-endoso.component';

describe('EnviarEndosoComponent', () => {
  let component: EnviarEndosoComponent;
  let fixture: ComponentFixture<EnviarEndosoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnviarEndosoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnviarEndosoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
