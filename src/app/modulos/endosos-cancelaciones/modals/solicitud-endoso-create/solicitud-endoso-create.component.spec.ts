import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudEndosoCreateComponent } from './solicitud-endoso-create.component';

describe('SolicitudEndosoCreateComponent', () => {
  let component: SolicitudEndosoCreateComponent;
  let fixture: ComponentFixture<SolicitudEndosoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudEndosoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudEndosoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
