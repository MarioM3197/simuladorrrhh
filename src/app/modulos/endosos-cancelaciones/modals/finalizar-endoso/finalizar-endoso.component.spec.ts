import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalizarEndosoComponent } from './finalizar-endoso.component';

describe('FinalizarEndosoComponent', () => {
  let component: FinalizarEndosoComponent;
  let fixture: ComponentFixture<FinalizarEndosoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalizarEndosoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalizarEndosoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
