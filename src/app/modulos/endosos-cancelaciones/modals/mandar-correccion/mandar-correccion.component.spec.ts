import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandarCorreccionComponent } from './mandar-correccion.component';

describe('MandarCorreccionComponent', () => {
  let component: MandarCorreccionComponent;
  let fixture: ComponentFixture<MandarCorreccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandarCorreccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandarCorreccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
