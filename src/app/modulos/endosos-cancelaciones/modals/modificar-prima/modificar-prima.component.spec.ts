import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarPrimaComponent } from './modificar-prima.component';

describe('ModificarPrimaComponent', () => {
  let component: ModificarPrimaComponent;
  let fixture: ComponentFixture<ModificarPrimaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarPrimaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarPrimaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
