import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoEndosoCreateComponent } from './estado-endoso-create.component';

describe('EstadoEndosoCreateComponent', () => {
  let component: EstadoEndosoCreateComponent;
  let fixture: ComponentFixture<EstadoEndosoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoEndosoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoEndosoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
