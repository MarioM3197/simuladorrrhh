import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoSolicitudEndosoCreateComponent } from './estado-solicitud-endoso-create.component';

describe('EstadoSolicitudEndosoCreateComponent', () => {
  let component: EstadoSolicitudEndosoCreateComponent;
  let fixture: ComponentFixture<EstadoSolicitudEndosoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoSolicitudEndosoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoSolicitudEndosoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
