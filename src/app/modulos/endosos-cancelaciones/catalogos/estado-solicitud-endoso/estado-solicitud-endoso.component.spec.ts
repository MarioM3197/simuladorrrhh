import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoSolicitudEndosoComponent } from './estado-solicitud-endoso.component';

describe('EstadoSolicitudEndosoComponent', () => {
  let component: EstadoSolicitudEndosoComponent;
  let fixture: ComponentFixture<EstadoSolicitudEndosoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoSolicitudEndosoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoSolicitudEndosoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
