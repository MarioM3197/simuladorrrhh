import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './modulos/Dashboard/dashboard/dashboard.component';
import {IconosComponent} from './modulos/nomenclaturaGrafica/iconos/iconos.component';
import {PdfComponent} from './modulos/nomenclaturaGrafica/pdf/pdf.component';
import {Error404Component} from './modulos/error404/error404.component';
import {LoginComponent} from './login/login.component';
import {EjemplosComponent} from './Ejemplos-de-Vista/ejemplos/ejemplos.component';
import {MensajesComponent} from './modulos/mensajes/mensajes.component';
import {MensajesTablaComponent} from './modulos/mensajes/mensajes-tabla/mensajes-tabla.component';
import {ModuloLlamadasComponent} from './modulos/modulo-llamadas/modulo-llamadas.component';
import {LoaderComponent} from './loader/loader.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {
    path: 'recursos-humanos',
    loadChildren: './modulos/recursos-humanos/recursos-humanos.module#RecursosHumanosModule',
  },
  {
    path: 'simulador',
    loadChildren: () => import('./modulos/simulador/simulador.module').then(m => m.SimuladorModule)
  },
  {path: 'dashboard', component: DashboardComponent},
  {path: 'chat', component: MensajesComponent},
  {path: 'mensajes', component: MensajesTablaComponent},
  {path: 'tablas', component: IconosComponent},
  {path: 'pdfs', component: PdfComponent},
  {path: 'ejemplos', component: EjemplosComponent},
  {path: 'llamadas', component: ModuloLlamadasComponent },
  {path: 'loader', component: LoaderComponent },
  {path: '**', component: Error404Component},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
