import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {Vacante} from '../../interfaces/reclutamiento/vacantes/vacante';

@Injectable({
  providedIn: 'root'
})
export class VacantesService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = environment.BACKEND + 'reclutamiento/vacantes';
  }

  get(): Observable<Vacante[]> {
    return this.http.get<Vacante[]>(this.url);
  }
}
