import { TestBed } from '@angular/core/testing';

import { CampanaSubareaService } from './campana-subarea.service';

describe('CampanaSubareaService', () => {
  let service: CampanaSubareaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampanaSubareaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
