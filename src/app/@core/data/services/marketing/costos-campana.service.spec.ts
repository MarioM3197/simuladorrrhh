import { TestBed } from '@angular/core/testing';

import { CostosCampanaService } from './costos-campana.service';

describe('CostosCampanaService', () => {
  let service: CostosCampanaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CostosCampanaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
