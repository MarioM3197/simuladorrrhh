import { TestBed } from '@angular/core/testing';

import { EstadoCostoService } from './estado-costo.service';

describe('EstadoCostoService', () => {
  let service: EstadoCostoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoCostoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
