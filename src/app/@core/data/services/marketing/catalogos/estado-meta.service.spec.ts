import { TestBed } from '@angular/core/testing';

import { EstadoMetaService } from './estado-meta.service';

describe('EstadoMetaService', () => {
  let service: EstadoMetaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoMetaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
