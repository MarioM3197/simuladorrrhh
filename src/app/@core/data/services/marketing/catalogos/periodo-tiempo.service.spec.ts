import { TestBed } from '@angular/core/testing';

import { PeriodoTiempoService } from './periodo-tiempo.service';

describe('PeriodoTiempoService', () => {
  let service: PeriodoTiempoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PeriodoTiempoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
