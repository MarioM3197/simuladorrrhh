import { TestBed } from '@angular/core/testing';

import { TipoMetaService } from './tipo-meta.service';

describe('TipoMetaService', () => {
  let service: TipoMetaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoMetaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
