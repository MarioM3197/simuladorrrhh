import { TestBed } from '@angular/core/testing';

import { TipoPresupuestoService } from './tipo-presupuesto.service';

describe('TipoPresupuestoService', () => {
  let service: TipoPresupuestoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoPresupuestoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
