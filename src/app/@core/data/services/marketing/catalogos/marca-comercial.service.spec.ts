import { TestBed } from '@angular/core/testing';

import { MarcaComercialService } from './marca-comercial.service';

describe('MarcaComercialService', () => {
  let service: MarcaComercialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MarcaComercialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
