import { TestBed } from '@angular/core/testing';

import { MedioTransporteService } from './medio-transporte.service';

describe('MedioTransporteService', () => {
  let service: MedioTransporteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MedioTransporteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
