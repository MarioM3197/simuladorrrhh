import { TestBed } from '@angular/core/testing';

import { AsignarUrlService } from './asignar-url.service';

describe('AsignarUrlService', () => {
  let service: AsignarUrlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AsignarUrlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
