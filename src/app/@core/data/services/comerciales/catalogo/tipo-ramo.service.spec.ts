import { TestBed } from '@angular/core/testing';

import { TipoRamoService } from './tipo-ramo.service';

describe('TipoRamoService', () => {
  let service: TipoRamoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoRamoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
