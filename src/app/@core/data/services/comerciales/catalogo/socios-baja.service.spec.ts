import { TestBed } from '@angular/core/testing';

import { SociosBajaService } from './socios-baja.service';

describe('SociosBajaService', () => {
  let service: SociosBajaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SociosBajaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
