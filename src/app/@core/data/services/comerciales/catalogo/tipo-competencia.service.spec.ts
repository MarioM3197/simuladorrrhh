import { TestBed } from '@angular/core/testing';

import { TipoCompetenciaService } from './tipo-competencia.service';

describe('TipoCompetenciaService', () => {
  let service: TipoCompetenciaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoCompetenciaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
