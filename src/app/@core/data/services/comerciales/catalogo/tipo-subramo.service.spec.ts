import { TestBed } from '@angular/core/testing';

import { TipoSubramoService } from './tipo-subramo.service';

describe('TipoSubramoService', () => {
  let service: TipoSubramoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoSubramoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
