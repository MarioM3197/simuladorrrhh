import { TestBed } from '@angular/core/testing';

import { RamoService } from './ramo.service';

describe('RamoService', () => {
  let service: RamoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RamoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
