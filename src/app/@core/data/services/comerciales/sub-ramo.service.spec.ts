import { TestBed } from '@angular/core/testing';

import { SubRamoService } from './sub-ramo.service';

describe('SubRamoService', () => {
  let service: SubRamoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubRamoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
