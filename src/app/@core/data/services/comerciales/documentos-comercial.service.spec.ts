import { TestBed } from '@angular/core/testing';

import { DocumentosComercialService } from './documentos-comercial.service';

describe('DocumentosComercialService', () => {
  let service: DocumentosComercialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocumentosComercialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
