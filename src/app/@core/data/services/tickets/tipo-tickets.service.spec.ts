import { TestBed } from '@angular/core/testing';

import { TipoTicketsService } from './tipo-tickets.service';

describe('TipoTicketsService', () => {
  let service: TipoTicketsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoTicketsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
