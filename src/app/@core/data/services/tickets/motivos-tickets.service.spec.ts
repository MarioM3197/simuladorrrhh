import { TestBed } from '@angular/core/testing';

import { MotivosTicketsService } from './motivos-tickets.service';

describe('MotivosTicketsService', () => {
  let service: MotivosTicketsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotivosTicketsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
