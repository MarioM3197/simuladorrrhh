import { TestBed } from '@angular/core/testing';

import { CalificacionesTicketsService } from './calificaciones-tickets.service';

describe('CalificacionesTicketsService', () => {
  let service: CalificacionesTicketsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalificacionesTicketsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
