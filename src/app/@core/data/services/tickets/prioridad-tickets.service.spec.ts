import { TestBed } from '@angular/core/testing';

import { PrioridadTicketsService } from './prioridad-tickets.service';

describe('PrioridadTicketsService', () => {
  let service: PrioridadTicketsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrioridadTicketsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
