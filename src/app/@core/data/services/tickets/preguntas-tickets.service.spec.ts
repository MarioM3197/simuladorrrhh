import { TestBed } from '@angular/core/testing';

import { PreguntasTicketsService } from './preguntas-tickets.service';

describe('PreguntasTicketsService', () => {
  let service: PreguntasTicketsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PreguntasTicketsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
