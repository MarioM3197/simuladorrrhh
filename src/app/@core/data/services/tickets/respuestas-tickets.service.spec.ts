import { TestBed } from '@angular/core/testing';

import { RespuestasTicketsService } from './respuestas-tickets.service';

describe('RespuestasTicketsService', () => {
  let service: RespuestasTicketsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RespuestasTicketsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
