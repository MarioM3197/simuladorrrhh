import { TestBed } from '@angular/core/testing';

import { EstadoTicketsService } from './estado-tickets.service';

describe('EstadoTicketsService', () => {
  let service: EstadoTicketsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoTicketsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
