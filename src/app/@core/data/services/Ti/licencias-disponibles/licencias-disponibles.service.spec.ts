import { TestBed } from '@angular/core/testing';

import { LicenciasDisponiblesService } from './licencias-disponibles.service';

describe('LicenciasDisponiblesService', () => {
  let service: LicenciasDisponiblesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LicenciasDisponiblesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
