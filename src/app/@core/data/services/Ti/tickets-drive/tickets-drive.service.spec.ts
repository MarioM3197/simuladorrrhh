import { TestBed } from '@angular/core/testing';

import { TicketsDriveService } from './tickets-drive.service';

describe('TicketsDriveService', () => {
  let service: TicketsDriveService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TicketsDriveService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
