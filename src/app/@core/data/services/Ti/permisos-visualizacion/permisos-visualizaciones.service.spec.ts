import { TestBed } from '@angular/core/testing';

import { PermisosVisualizacionesService } from './permisos-visualizaciones.service';

describe('PermisosVisualizacionesService', () => {
  let service: PermisosVisualizacionesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PermisosVisualizacionesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
