import { TestBed } from '@angular/core/testing';

import { AplicacionesViewCobranzaServiceService } from './aplicaciones-view-cobranza-service.service';

describe('AplicacionesViewCobranzaServiceService', () => {
  let service: AplicacionesViewCobranzaServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AplicacionesViewCobranzaServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
