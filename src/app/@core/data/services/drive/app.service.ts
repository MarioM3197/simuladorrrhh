import { Injectable } from '@angular/core';
import {UserdService} from './userd.service';
import {FilesService} from './files.service';

@Injectable()
export class AppService {
  constructor(
    private filesService: FilesService,
    private userdService: UserdService,
  ) {

  }
  get File(): FilesService {
    return this.filesService;
  }
  get User(): UserdService {
    return this.userdService;
  }
}
