import { TestBed } from '@angular/core/testing';

import { PrecandidatosService } from './precandidatos.service';

describe('PrecandidatosService', () => {
  let service: PrecandidatosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrecandidatosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
