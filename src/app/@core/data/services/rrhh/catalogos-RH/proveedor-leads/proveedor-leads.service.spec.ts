import { TestBed } from '@angular/core/testing';

import { ProveedorLeadsService } from './proveedor-leads.service';

describe('ProveedorLeadsService', () => {
  let service: ProveedorLeadsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProveedorLeadsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
