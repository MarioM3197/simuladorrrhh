import { TestBed } from '@angular/core/testing';

import { RazonSocialService } from './razon-social.service';

describe('RazonSocialService', () => {
  let service: RazonSocialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RazonSocialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
