import { TestBed } from '@angular/core/testing';

import { EstacionesLineasService } from './estaciones-lineas.service';

describe('EstacionesLineasService', () => {
  let service: EstacionesLineasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstacionesLineasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
