import { TestBed } from '@angular/core/testing';

import { CpPeruService } from './cp-peru.service';

describe('CpPeruService', () => {
  let service: CpPeruService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CpPeruService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
