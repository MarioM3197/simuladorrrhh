import { TestBed } from '@angular/core/testing';

import { TurnoEmpleadoService } from './turno-empleado.service';

describe('TurnoEmpleadoService', () => {
  let service: TurnoEmpleadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TurnoEmpleadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
