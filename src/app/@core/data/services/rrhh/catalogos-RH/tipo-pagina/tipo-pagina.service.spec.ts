import { TestBed } from '@angular/core/testing';

import { TipoPaginaService } from './tipo-pagina.service';

describe('TipoPaginaService', () => {
  let service: TipoPaginaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoPaginaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
