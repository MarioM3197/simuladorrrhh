import { TestBed } from '@angular/core/testing';

import { ProveedoresLeadService } from './proveedores-lead.service';

describe('ProveedoresLeadService', () => {
  let service: ProveedoresLeadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProveedoresLeadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
