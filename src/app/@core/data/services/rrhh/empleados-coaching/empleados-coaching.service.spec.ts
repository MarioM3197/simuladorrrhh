import { TestBed } from '@angular/core/testing';

import { EmpleadosCoachingService } from './empleados-coaching.service';

describe('EmpleadosCoachingService', () => {
  let service: EmpleadosCoachingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmpleadosCoachingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
