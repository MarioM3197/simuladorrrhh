import { TestBed } from '@angular/core/testing';

import { EndosoFinanzasService } from './endoso-finanzas.service';

describe('EndosoFinanzasService', () => {
  let service: EndosoFinanzasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndosoFinanzasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
