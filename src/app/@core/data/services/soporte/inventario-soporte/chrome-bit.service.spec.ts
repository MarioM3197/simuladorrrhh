import { TestBed } from '@angular/core/testing';

import { ChromeBitService } from './chrome-bit.service';

describe('ChromeBitService', () => {
  let service: ChromeBitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChromeBitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
