import { TestBed } from '@angular/core/testing';

import { BarrasMulticontactoService } from './barras-multicontacto.service';

describe('BarrasMulticontactoService', () => {
  let service: BarrasMulticontactoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BarrasMulticontactoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
