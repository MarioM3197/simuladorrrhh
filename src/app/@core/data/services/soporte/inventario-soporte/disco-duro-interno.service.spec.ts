import { TestBed } from '@angular/core/testing';

import { DiscoDuroInternoService } from './disco-duro-interno.service';

describe('DiscoDuroInternoService', () => {
  let service: DiscoDuroInternoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiscoDuroInternoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
