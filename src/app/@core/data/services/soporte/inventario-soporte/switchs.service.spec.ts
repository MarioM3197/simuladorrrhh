import { TestBed } from '@angular/core/testing';

import { SwitchsService } from './switchs.service';

describe('SwitchsService', () => {
  let service: SwitchsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SwitchsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
