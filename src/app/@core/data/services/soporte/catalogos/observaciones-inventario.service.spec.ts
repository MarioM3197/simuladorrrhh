import { TestBed } from '@angular/core/testing';

import { ObservacionesInventarioService } from './observaciones-inventario.service';

describe('ObservacionesInventarioService', () => {
  let service: ObservacionesInventarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObservacionesInventarioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
