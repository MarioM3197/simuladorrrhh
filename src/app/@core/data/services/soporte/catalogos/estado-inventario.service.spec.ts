import { TestBed } from '@angular/core/testing';

import { EstadoInventarioService } from './estado-inventario.service';

describe('EstadoInventarioService', () => {
  let service: EstadoInventarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoInventarioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
