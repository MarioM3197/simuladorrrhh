import { TestBed } from '@angular/core/testing';

import { ErroresAutorizacionRecibosService } from './errores-autorizacion-recibos.service';

describe('ErroresAutorizacionRecibosService', () => {
  let service: ErroresAutorizacionRecibosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErroresAutorizacionRecibosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
