import { TestBed } from '@angular/core/testing';

import { CorreccionesErroresRecibosService } from './correcciones-errores-recibos.service';

describe('CorreccionesErroresRecibosService', () => {
  let service: CorreccionesErroresRecibosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CorreccionesErroresRecibosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
