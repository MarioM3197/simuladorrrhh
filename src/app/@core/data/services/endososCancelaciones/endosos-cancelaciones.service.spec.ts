import { TestBed } from '@angular/core/testing';

import { EndososCancelacionesService } from './endosos-cancelaciones.service';

describe('EndososCancelacionesService', () => {
  let service: EndososCancelacionesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EndososCancelacionesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
