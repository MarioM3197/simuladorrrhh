import { TestBed } from '@angular/core/testing';

import { MotivoEndosoService } from './motivo-endoso.service';

describe('MotivoEndosoService', () => {
  let service: MotivoEndosoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotivoEndosoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
