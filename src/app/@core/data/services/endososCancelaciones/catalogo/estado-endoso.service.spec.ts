import { TestBed } from '@angular/core/testing';

import { EstadoEndosoService } from './estado-endoso.service';

describe('EstadoEndosoService', () => {
  let service: EstadoEndosoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoEndosoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
