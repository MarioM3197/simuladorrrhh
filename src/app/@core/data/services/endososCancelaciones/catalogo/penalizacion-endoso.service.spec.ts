import { TestBed } from '@angular/core/testing';

import { PenalizacionEndosoService } from './penalizacion-endoso.service';

describe('PenalizacionEndosoService', () => {
  let service: PenalizacionEndosoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PenalizacionEndosoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
