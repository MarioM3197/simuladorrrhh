import { TestBed } from '@angular/core/testing';

import { TipoEndosoService } from './tipo-endoso.service';

describe('TipoEndosoService', () => {
  let service: TipoEndosoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoEndosoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
