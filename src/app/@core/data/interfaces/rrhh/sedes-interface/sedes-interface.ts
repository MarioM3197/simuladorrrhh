/**
 * Declaración de Interfaz a usar en el menu principal generando la vista de Sedes.
 * //id: Genera un identificador unico por cada área
 * //nombre: Cadena que contiene el área a mostrar en la vista, actualmente es asignado directamente en la BD
 * //descripcion: Cadena de caracteres que asignan una descripcion a cada área
 */
export interface SedesInterface {
  id: any;
  nombre: string;
  descripcion?: string;
}

