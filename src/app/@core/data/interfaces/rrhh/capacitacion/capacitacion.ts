import {Observable} from 'rxjs';

export interface Capacitacion {

  id: number;
  idCandidato: number;
  calificacion: number;
  idCompetenciaCandidato: number;
  idCalificacionCompetencia: number;
  calificacionCompetencia: string;
  calificacionExamen: number;
  comentarios: string;
  idCapacitador: number;
  asistencias: string;
  fechaIngreso: string;
  fechaRegistro: string;
  idEtapa: number;
  idEstado: number;
  idPrecandidato: number;
  competenciaCandidato: string;
  competencias: string;
  nombre: string;
  sede: number;
  idCompetencia: number;
}


