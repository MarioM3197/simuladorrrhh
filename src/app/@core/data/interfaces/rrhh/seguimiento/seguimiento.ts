import {Observable} from 'rxjs';

export interface Seguimiento {
  id: number;
  fechaIngreso: Date;
  calificacionRollplay: number;
  idCapacitacion: number;
  idCompetencia: number;
  calificacion: number;
  idCoach: number;
  idSubarea: number;
  nombreCampana: string;
  asistencia: string;
  registro: number;
  idCliente: number;
  prima: number;
  comentarios: string;
  idEtapa: number;
  empleado: string;
  nombreSubarea: string;
  comentariosFaseDos: number;
  idEstadoRRHH: number;
  sede: number;
  nombre: string;
  fechaRegistro: string;

}

