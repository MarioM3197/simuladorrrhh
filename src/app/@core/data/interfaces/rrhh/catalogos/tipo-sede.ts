import {Observable} from 'rxjs';

export interface TipoSede {
  id: number;
  descripcion: string;
  activo: number;
}
export abstract class TipoSedeData {
  abstract get(): Observable<TipoSede[]>;

  abstract post(tipoSede): Observable<TipoSede>;

  // abstract postSocket(jsno): Observable<JSON>;
  //
  // abstract postSocketBaja(jsno): Observable<JSON>;

  abstract getTipoSedeById(idTipoSede): Observable<TipoSede>;

  abstract put(idTipoSede, tipoSede): Observable<TipoSede>;
}
