import {Observable} from 'rxjs';

export interface Subarea {
  id: number;
  idArea: number;
  subarea: string;
  descripcion: string;
  activo: number;
  codigo: number;
  nombreArea?: string;
}

export abstract class SubareaData {
  abstract get(): Observable<Subarea[]>;

  abstract findByIdArea(idArea): Observable<Subarea[]>;

  abstract  getByIdAreaView(idArea: number): Observable<Subarea[]>;

  abstract getById(idSubarea): Observable<Subarea>;

  abstract posting(subarea): Observable<Subarea>;

  abstract put(idSubarea, subarea): Observable<Subarea>;
}


