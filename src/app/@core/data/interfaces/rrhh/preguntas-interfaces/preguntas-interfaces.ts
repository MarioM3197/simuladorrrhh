/**
 *@description  Interfaz de las preguntas generadas asi como los campos que la conforman
 */
export interface PreguntasInterfaces {
  /**
   * Variables de la interfaz //idArea recibe un parametro number que hace referencia al área
   */
  id: number;
  pregunta: string;
  fechaInicioVigencia: Date;
  fechaFinVigencia: Date;
  idArea: number;
}

export class PreguntasInterfaces {
}
