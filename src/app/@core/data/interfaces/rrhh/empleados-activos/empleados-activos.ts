import {Observable} from 'rxjs';


export interface EmpleadosActivos {
  id: number;
  idCandidato: number;
  idPuesto: number;
  idTipoPuesto: number;
  idUsuario: number;
  idSede: number;
  idBanco: number;
  idSubarea: number;
  idArea: number;
  idGrupo: number;
  idTurnoEmpleado: number;
  fechaIngreso: string;
  fechaRegistro: string;
  subarea: string;
  area: string;
  sede: string;
  empresa: string;
  grupo: string;
  puesto: string;
  puestoTipo: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  fechaNacimiento: string;
  idEstadoRh: number;
  usuario: string;
  turno: string;
  horario: string;
  tarjetaSecundaria: string;
  idTipoSubarea: number;
  contador: number;
  permiso: string;
}

export abstract class EmpleadosActivosData {
  abstract get(): Observable<EmpleadosActivos[]>;
  abstract getEmpleadoById(idEmpleados): Observable<EmpleadosActivos>;
  abstract getBySubarea(idSubarea): Observable<EmpleadosActivos[]>;
  abstract getEjecutivosActivos (idSubarea: number): Observable<EmpleadosActivos[]>;
  abstract getEmpleadobySubareabyidUsuario(idSubarea, idUsuario): Observable<EmpleadosActivos>;
  abstract getActivarEmpleado(idEmpleado, permiso): Observable<EmpleadosActivos>;
}
