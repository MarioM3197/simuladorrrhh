export class Userd {

  Id: string;
  Email: string;

  static fromBasicProfile(profile: gapi.auth2.BasicProfile): Userd {
    const user: Userd = new Userd();
    user.Id = profile.getId();
    user.Email = profile.getEmail();
    return user;
  }
}
