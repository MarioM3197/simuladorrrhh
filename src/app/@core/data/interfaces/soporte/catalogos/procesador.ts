import {Observable} from 'rxjs';

export interface Procesador {
  id: number;
  nombreProcesador: string;
  activo: number;
}

export abstract class ProcesadorSoporteData {
  abstract get(): Observable<Procesador[]>;

  abstract post(procesador): Observable<Procesador>;
  abstract postSocket(json): Observable<JSON>;

  abstract getProcesadorSoporte(idProcesador): Observable<Procesador>;

  abstract put(idProcesador, procesador): Observable<Procesador>;
}
