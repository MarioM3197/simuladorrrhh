import {Observable} from 'rxjs';

export interface Laptop {
  id: number;
  idEmpleado?: number;
  idEstadoInventario: number;
  idObservacionesInventario: number;
  estadoInventario?: string;
  observacionesInventario?: string;
  numeroSerie: string;
  idProcesador: string;
  hdd: string;
  ram: string;
  idMarca: string;
  modelo: string;
  fechaRecepcion: Date;
  fechaSalida: Date;
  comentarios: string;
  medidaPantalla: string;
  activo: number;
  nombreMarca?: string;
  nombreProcesador?: string;
  idTipoSede?: number;
  origenRecurso?: string;
  sedeNombre?: string;
}

export abstract class LaptopData {
  abstract get(): Observable<Laptop[]>;
  abstract post(mouse): Observable<Laptop>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getLaptopById(idLaptop): Observable<Laptop>;
  abstract put(idLaptop, laptop): Observable<Laptop>;
}
