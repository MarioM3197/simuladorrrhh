import {Observable} from 'rxjs';


export interface CharolasRack {
  id: number;
  idEmpleado?: number;
  idObservacionesInventario: number;
  observacionesInventario?: string;
  idEstadoInventario: number;
  estadoInventario?: string;
  folio: number;
  idMarca: number;
  nombreMarca: string;
  idTipoSede?: number;
  origenRecurso?: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  observaciones?: string;
  sedeNombre?: string;
}
export abstract class CharolasRackData {
  abstract get(): Observable<CharolasRack[]>;
  abstract post(CharolasRack): Observable<CharolasRack>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getCharolasRackById(idCharolasRack): Observable<CharolasRack>;
  abstract put(idCharolasRack, charolasRack): Observable<CharolasRack>;

}

