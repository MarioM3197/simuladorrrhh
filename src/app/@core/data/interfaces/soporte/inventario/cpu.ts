import {Observable} from 'rxjs';

export interface Cpu {
  id: number;
  idEmpleado?: number;
  numeroSerie: string;
  idProcesador: string;
  hdd: string;
  ram: string;
  idMarca: string;
  modelo: string;
  idEstadoInventario: number;
  estadoInventario?: string;
  observacionesInventario?: string;
  fechaRecepcion: Date;
  fechaSalida: Date;
  idObservacionesInventario: number;
  comentarios: string;
  activo: number;
  nombreMarca: string;
  idTipoSede?: number;
  origenRecurso?: string;
  sedeNombre?: string;

}
export abstract class CpuData {
  abstract get(): Observable<Cpu[]>;
  abstract post(cpu): Observable<Cpu>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getCpuById(idCpu): Observable<Cpu>;
  abstract put(idCpu, cpu): Observable<Cpu>;
}
