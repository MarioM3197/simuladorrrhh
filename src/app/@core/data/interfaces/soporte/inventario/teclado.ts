import {Observable} from 'rxjs';

export interface Teclado {
  id: number;
  idEmpleado: number;
  idMarca: number;
  idEstadoInventario: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  idObservacionesInventario: number;
  estadoInventario?: string;
  observacionesInventario?: string;
  comentarios: string;
  activo: number;
  nombreMarca?: string;
  idTipoSede?: number;
  origenRecursos?: string;
  sedeNombre?: string;

}
export abstract class TecladoData {
  abstract get(): Observable<Teclado[]>;
  abstract post(teclado): Observable<Teclado>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getTecladoById(idTeclado): Observable<Teclado>;
  abstract put(idTeclado, teclado): Observable<Teclado>;
}
