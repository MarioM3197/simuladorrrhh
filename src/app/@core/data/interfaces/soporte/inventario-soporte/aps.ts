import {Observable} from 'rxjs';

export interface Aps {
  id: number;
  idEmpleado: number;
  idObservacionesInventario: number;
  idEstadoInventario: number;
  folio: string;
  idMarca: number;
  idTipoSede: number;
  origenRecurso: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  nombreMarca: string;
  estadoInventario?: string;
  observaciones?: string;
  sedeNombre?: string;
}

export abstract class ApsData {
  abstract get(): Observable<Aps[]>;

  abstract post(aps): Observable<Aps>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;

  abstract getApsById(idAps): Observable<Aps>;

  abstract put(idAps, aps): Observable<Aps>;
}
