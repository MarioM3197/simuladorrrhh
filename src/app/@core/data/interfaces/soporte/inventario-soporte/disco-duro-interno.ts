import {Observable} from 'rxjs';

export interface DiscoDuroInterno {
  id: number;
  idEmpleado: number;
  idObservacionesInventario: number;
  idEstadoInventario: number;
  folio: string;
  idMarca: number;
  idTipoSede: number;
  origenRecurso: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  nombreMarca: string;
  estadoInventario?: string;
  observaciones?: string;
  sedeNombre?: string;
}

export abstract class DiscoDuroInternoData {
  abstract get(): Observable<DiscoDuroInterno[]>;

  abstract post(discoInterno): Observable<DiscoDuroInterno>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;

  abstract getDiscoInternoById(idDiscoInterno): Observable<DiscoDuroInterno>;

  abstract put(idDiscoInterno, discoInterno): Observable<DiscoDuroInterno>;
}
