import {Observable} from 'rxjs';


export interface FuenteCamaras {
  id: number;
  idEmpleado: number;
  idObservacionesInventario: number;
  idEstadoInventario: number;
  folio: string;
  idMarca: number;
  idTipoSede: number;
  origenRecurso: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  nombreMarca: string;
  estadoInventario?: string;
  observaciones?: string;
  sedeNombre?: string;
}

export abstract class FuenteCamarasData {
  abstract get(): Observable<FuenteCamaras[]>;

  abstract post(fuenteCamara): Observable<FuenteCamaras>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;

  abstract getFuenteCamaraById(idFuenteCamara): Observable<FuenteCamaras>;

  abstract put(idFuenteCamara, fuenteCamara): Observable<FuenteCamaras>;
}
