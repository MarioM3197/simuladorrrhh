import {Observable} from 'rxjs';

export interface Camaras {
  id: number;
  idEmpleado: number;
  idObservacionesInventario: number;
  idEstadoInventario: number;
  folio: string;
  idMarca: number;
  idTipoSede: number;
  origenRecurso: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  nombreMarca: string;
  estadoInventario?: string;
  observaciones?: string;
  sedeNombre?: string;
}

export abstract class CamarasData {
  abstract get(): Observable<Camaras[]>;

  abstract post(camaras): Observable<Camaras>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;

  abstract getCamarasById(idCamara): Observable<Camaras>;

  abstract put(idCamara, camaras): Observable<Camaras>;
}
