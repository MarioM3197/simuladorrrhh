
export interface CotizarInterno {
  marca?: string;
  modelo?: string;
  descripcion?: string;
  subDescripcion?: string;
  detalle?: string;
}
