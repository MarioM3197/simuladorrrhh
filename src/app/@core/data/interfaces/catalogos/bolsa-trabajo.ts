import {Observable} from 'rxjs';

export interface BolsaTrabajo {
    id: number;
    nombre: string;
    tipo: string;
    activo: number;
}

export abstract class BolsaTrabajoData {
  abstract getBolsaTrabajo(): Observable<BolsaTrabajo[]>;

  abstract post(bolsaTrabajo): Observable<BolsaTrabajo>;
  abstract postSocket (json): Observable<JSON>;
  abstract getBolsaTrabajoByActivo(activo): Observable<BolsaTrabajo[]>;
  abstract put(idBolsaTrabajo, bolsaTrabajo): Observable<BolsaTrabajo>;

  abstract getBolsaTrabajoById(idBolsaTrabajo): Observable<BolsaTrabajo>;
}
