import {Observable} from 'rxjs';

export interface TipoSiniestro {
  id: number;
  descripcion: string;
  activo: number;
}
export abstract class TipoSiniestroData  {
  abstract get(): Observable<TipoSiniestro[]>;

  abstract post(tipoSiniestro: TipoSiniestro): Observable<TipoSiniestro>;

  abstract getTipoSiniestroById(idTipoSiniestro): Observable<TipoSiniestro>;

  abstract put(idTipoSiniestro, tipoSiniestro): Observable<TipoSiniestro>;
}
