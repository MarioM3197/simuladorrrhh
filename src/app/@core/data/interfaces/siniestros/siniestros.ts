import {Observable} from 'rxjs';

export interface Siniestros {
  id: number;
  idRegistro: number;
  idTipoRegistro: number;
  idTipoSiniestro: number;
  descripcion: string;
  fechaRegistro: string;
  fechaSiniestro: string;
  activo: number;
  tipoDescripcion?: string;
  idEmpleado?: number;
  nombre?: string;
  idCliente?: number;
}
export abstract class SiniestrosData {
  abstract get(): Observable<Siniestros[]>;

  abstract post(siniestros: Siniestros): Observable<Siniestros>;

  abstract getSiniestrosById(idSiniestros): Observable<Siniestros>;

  abstract put(idSiniestros, siniestros): Observable<Siniestros>;
}
