import {Observable} from 'rxjs';

export interface AutorizacionRecibos{
  id: number;
  idRegistro: number;
  idEstadoVerificacion: number;
  numero: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
  verificadoInspeccion: number;
  // Las variables que están abajo son exclusivas de la vista
  idEmpleado?: number;
  poliza?: string;
  idCliente?: number;
  nombreCliente?: string;
  apellidoPaternoCliente?: string;
  apellidoMaternoCliente?: string;
  carpetaCliente?: string;
  carpetaRegistro?: string;
  archivoPago?: string;
  estadoVerificacion?: string;
  idPago?: number;
  idProducto: number;
  idFlujoPoliza?: number;
}

export abstract class AutorizacionReciboData {
  abstract get(): Observable<AutorizacionRecibos[]>;
  abstract getById(id: number): Observable<AutorizacionRecibos>;
  abstract getByIdRegistro(idRegistro: number): Observable<AutorizacionRecibos>;
  abstract post(registro): Observable<AutorizacionRecibos>;
  abstract postConEstado(registro, estado): Observable<AutorizacionRecibos>;
  abstract documentoVerificado(idAutorizacionRegistro: number, documentoVerificado: number, estado: number): Observable<String>;
  abstract getIdFlujoPoliza(idFlujoPoliza: number): Observable<AutorizacionRecibos[]>;
}
