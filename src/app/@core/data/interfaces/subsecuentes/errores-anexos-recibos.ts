import {Observable} from 'rxjs';

export interface ErroresAnexosRecibos {
  id: number;
  idAutorizacionRecibo: number;
  idEmpleado: number;
  idEstadoCorreccion: number;
  idTipoDocumento: number;
  idEmpleadoCorreccion: number;
  correcciones: string;
}

export abstract class ErroresAnexosRecibosData {
  abstract get(): Observable<ErroresAnexosRecibos[]>;
  abstract getByIdAutorizacionRegitro(idAutorizacionRegistro: number, idTipoDocumento: number): Observable<ErroresAnexosRecibos>;
  abstract post(error: ErroresAnexosRecibos): Observable<string>;
  abstract put(idError: number, error: ErroresAnexosRecibos): Observable<any>;
  abstract putEstadoCorreccion(ErroresAnexosRecibos: number): Observable<any>;
}
