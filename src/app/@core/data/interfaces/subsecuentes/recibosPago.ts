export interface RecibosPago {
  id: number;
  idRegistro: number;
  idEmpleado: number;
  idEstadoRecibos: number;
  numero: number;
  cantidad: number;
  fechaVigencia: Date;
  fechaLiquidacion: Date;
  fechaPromesaPago: Date;
  fechaCierre: Date;
  idPago: number;
  idFormaPago: number;
  idEstadoPago: number;
  idEmpleadoPago: number;
  fechaPago: Date;
  fechaRegistro: Date;
  cantidadPago: number;
  archivo: string;
  poliza: string;
  estado: string;
  nombreEmpleadoPago: string;
  apellidoPaternoEmpleadoPago: string;
  apellidoMaternoEmpleadoPago: string;
}
