import {Observable} from 'rxjs';

export interface ErroresAutorizacionRecibos {
  id: number;
  idRegistro: number;
  idEstadoVerificacion: number;
  numero: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
  verificadoInspeccion: number;
  idErrorAutorizacion?: number;
  idErrorAnexo?: number;
  idEstadoCorreccion: number;
  idEmpleadoCorreccion: number;
  idEmpleado: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  poliza: string;
  idCliente: number;
  nombreCliente: string;
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
  carpetaCliente: string;
  carpetaRegistro: string;
  idPago: number;
  archivoPago: string;
  estadoVerificacion: string;
}

export interface ErroresVerificacionRecibos {
  id: number;
  idRegistro: number;
  idEstadoVerificacion: number;
  estadoVerificacion: string;
  numero: number;
  verificadoCliente: number;
  verificadoProducto: number;
  verificadoRegistro: number;
  verificadoPago: number;
  verificadoInspeccion: number;
  idErrorAnexo?: number;
  idErrorAutorizacion?: number;
  idEstadoCorreccion: number;
  idEmpleadoCorreccion: number;
  idEmpleado: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  poliza: string;
  idCliente: number;
  nombreCliente: string;
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
  carpetaCliente: string;
  carpetaRegistro: string;
  idPago: number;
  archivoPago: string;
}

export abstract class CorreccionErroresRecibosData {
  abstract getErroresDocumentosA (): Observable<ErroresAutorizacionRecibos[]>;
  abstract getErroresDatosA (): Observable<ErroresAutorizacionRecibos[]>;
  abstract getErroresDocumentosV (): Observable<ErroresVerificacionRecibos[]>;
  abstract getErroresDatosV (): Observable<ErroresVerificacionRecibos[]>;

  // Nuevo
  abstract getErroresDocumentosAFlujoPoliza (idFlujoPoliza: number): Observable<ErroresAutorizacionRecibos[]>;
  abstract getErroresDatosAFlujoPoliza (idFlujoPoliza: number): Observable<ErroresAutorizacionRecibos[]>;
  abstract getErroresDocumentosVFlujoPoliza (idFlujoPoliza: number): Observable<ErroresVerificacionRecibos[]>;
  abstract getErroresDatosVFlujoPoliza (idFlujoPoliza: number): Observable<ErroresVerificacionRecibos[]>;
}
