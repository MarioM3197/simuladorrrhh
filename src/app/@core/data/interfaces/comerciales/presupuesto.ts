import {Observable} from 'rxjs';

export interface Presupuesto {
  idSocio: number;
  presupuesto: string;
  idEstado: number;
  alias: string;
  idEstadoSocio: number;
  anio: string;
  activo: number;
}

export abstract class PresupuestoData {
  abstract get(): Observable<Presupuesto[]>;

  abstract post(presupuesto): Observable<Presupuesto>;
  abstract postSocket(json): Observable<JSON>;

  abstract getPresupuestoById(idPresupuesto): Observable<Presupuesto>;

  abstract put(idPresupuesto, presupuesto): Observable<Presupuesto>;
}
