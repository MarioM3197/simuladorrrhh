import {Observable} from 'rxjs';
export interface Ramo {
  id: number;
  idSocio: number;
  tipoRamo: string;
  idTipoRamo: number;
  descripcion: string;
  prioridad: number;
  prioridades: string;
  activo: number;
  alias: string;
  idEstado: number;
  idEstadoSocio: number;
  estado: string;
  nombreComercial: string;
}
export abstract class RamoData {
  abstract get(): Observable<Ramo[]>;
  abstract post(ramo): Observable<Ramo>;
  abstract postSocket(json): Observable<JSON>;
  abstract getRamoById(idRamo): Observable<Ramo>;
  abstract getRamoByIdSocio(idSocio: number): Observable<Ramo[]>;
  abstract put(idRamo, ramo): Observable<Ramo>;
}
