import {Observable} from 'rxjs';

export interface Contratos {
  idSocio: number;
  fechaInicio: Date;
  fechaFin: Date;
  referencia: string;
  activo: number;
  idEstado: number;
  idEstadoSocio: number;
  diasRestantes: number;
}

export abstract class ContratosData {
  abstract get(): Observable<Contratos[]>;

  abstract post(contratos, fechaInicio, fechaFin): Observable<Contratos>;
  abstract postSocket(json): Observable<JSON>;

  abstract getContratoById(idContrato): Observable<Contratos>;

  abstract put(idContrato, contratos): Observable<Contratos>;
}
