import {Observable} from 'rxjs';

export interface AsignarUrl {
  idSocio: number;
  url: string;
  descripcion: string;
  activo: number;
  idEstadoSocio: number;
}

export abstract class AsignarUrlData {
  abstract get(): Observable<AsignarUrl[]>;

  abstract post(asignarUrl): Observable<AsignarUrl>;
  abstract postSocket(json): Observable<JSON>;

  abstract getAsignarUrlById(idAsignarUrl): Observable<AsignarUrl>;

  abstract put(idAsignarUrl, asignarUrl): Observable<AsignarUrl>;
}
