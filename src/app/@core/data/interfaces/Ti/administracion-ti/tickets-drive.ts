import {Observable} from 'rxjs';

// tslint:disable-next-line:no-empty-interface
export interface TicketsDrive {
}

export abstract  class TicketDriveData {

  abstract postTicket(files: FileList, tipoArchivo: string, idTicket: number, idRegistro: number, idEmpleado: number): Observable<string>;
  abstract  getArchivo(idFolder: string, nombreDocumento: string): Observable<any[]>;

}
