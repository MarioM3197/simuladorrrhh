import {Observable} from 'rxjs';

export interface Drive {
  tipo: String;
  fecha: Date;
  numero: string;
  downloadLink: String;
}

export abstract class DriveData {
  abstract get(fechaInicio: Date, fechaFin: Date, ACD: boolean, manual: boolean, numero: string): Observable<Drive[]>;
  abstract getArchivo (idTipoDocumento: number, parametroComplementario: string): Observable<any[]>;
  abstract getAllDocuments(idTipoDocumento: number, parametroComplementario: string): Observable<any[]>;
  abstract post(files: FileList, tipoArchivo: string, idCliente: number, idRegistro: number): Observable<string>;
  abstract uploadByIdFolder (files: FileList, tipoArchivo: string, idRegistro: number, idCarpeta: string): Observable<string>;
}
