import {Observable} from 'rxjs';

export interface LicenciasDisponibles {
  id: number;
  cantidad: number;
  nombre: string;
}

export abstract class LicenciasDisponiblesData {
  abstract get(): Observable<LicenciasDisponibles[]>;
  abstract postSocket(json): Observable<JSON>;
  abstract put(id, cantidad): Observable<LicenciasDisponibles>;
  abstract getById(id): Observable<LicenciasDisponibles>;
}
