import {Observable} from 'rxjs';

export interface MetasCampana {
  id: number;
  idCampana: number;
  idPeriodoTiempo: number;
  idProveedorLead: number;
  idEstadoMeta: number;
  idTipoMeta: number;
  fecha: string;
  fechaRegistro: string;
  cantidad: number;
  activo: number;
  nombreCampana?: string;
  periodo?: string;
  nombre?: string;
  estado?: string;
  Tipo?: string;
}
export abstract class MetasCampanaData {
  abstract get(): Observable<MetasCampana[]>;

  abstract post(metasCampana): Observable<MetasCampana>;

  abstract getMetasCampanaById(idMetasCampana): Observable<MetasCampana>;

  abstract put(idMetasCampana, metasCampana): Observable<MetasCampana>;
}
