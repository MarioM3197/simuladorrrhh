import {Observable} from 'rxjs';

export interface MediosDifusion {
  id: number;
  idTipoSubarea: number;
  idProveedor: number;
  idPagina: number;
  descripcion: string;
  configuracion: string;
  activo: number;
  subarea?: string;
  descripcionPagina?: string;
  proveedorLeads?: string;
  url?: string;
  urlCompleta?: number;
  campana?: number;
}

export abstract class MediosDifusionData {
  abstract get(): Observable<MediosDifusion[]>;

  abstract post(mediosDifusion): Observable<MediosDifusion>;
  abstract postSocket(json): Observable<JSON>;

  abstract getMediosDifusionById(idMediosDifusion): Observable<MediosDifusion>;

  abstract put(idMediosDifusion, mediosDifusion): Observable<MediosDifusion>;
}
