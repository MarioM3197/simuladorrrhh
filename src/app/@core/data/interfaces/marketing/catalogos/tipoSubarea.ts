import {Observable} from 'rxjs';

export interface TipoSubarea {
  id: number;
  tipo: string;
  activo: number;
}
export abstract class TipoSubareaData {
  abstract get(): Observable<TipoSubarea[]>;

  abstract post(tipoSubarea): Observable<TipoSubarea>;

  abstract getTipoSubareaById(idTipoSubarea): Observable<TipoSubarea>;
}
