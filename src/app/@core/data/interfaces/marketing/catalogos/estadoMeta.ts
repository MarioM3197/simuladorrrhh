import {Observable} from 'rxjs';
import {Paises} from "../../catalogos/paises";

export interface EstadoMeta {
  id: number;
  estado: string;
  objetivo: string;
  activo: number;
}
export abstract class EstadoMetaData {
  abstract get(): Observable<EstadoMeta[]>;

  abstract getByActivo(activo): Observable<EstadoMeta[]>;

  abstract post(estadoMeta): Observable<EstadoMeta>;

  abstract getEstadoMetaById(idEstadoMeta): Observable<EstadoMeta>;

  abstract put(idEstadoMeta, estadoMeta): Observable<EstadoMeta>;
}
