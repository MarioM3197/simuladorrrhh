import {Observable} from 'rxjs';

export interface TipoMeta {
  id: number;
  tipo: string;
  objetivo: string;
  activo: number;
}
export abstract class TipoMetaData {
  abstract get(): Observable<TipoMeta[]>;

  abstract getByActivo(activo): Observable<TipoMeta[]>;

  abstract post(tipoMeta): Observable<TipoMeta>;

  abstract getTipoMetaById(idTipoMeta): Observable<TipoMeta>;

  abstract put(idTipoMeta, tipoMeta): Observable<TipoMeta>;
}
