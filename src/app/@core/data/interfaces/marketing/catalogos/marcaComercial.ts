import {Observable} from 'rxjs';

export interface MarcaComercial {
  id: number;
  marca: string;
  detalle: string;
  activo: number;
}
export abstract class MarcaComercialData {
  abstract get(): Observable<MarcaComercial[]>;

  abstract post(marcaComercial): Observable<MarcaComercial>;

  abstract getMarcaComercialById(idMarcaComercial): Observable<MarcaComercial>;

  abstract put(idMarcaComercial, marcaComercial): Observable<MarcaComercial>;

  abstract getByActivo(activo): Observable<MarcaComercial[]>;
}
