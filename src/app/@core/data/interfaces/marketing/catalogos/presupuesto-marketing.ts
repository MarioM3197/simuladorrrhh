import {Observable} from 'rxjs';

export interface PresupuestoMarketing {
  id: number;
  candidad: number;
  fechaPresupuesto: string;
  activo: number;
}
export abstract class PresupuestoMarketingData {
  abstract get(): Observable<PresupuestoMarketing[]>;

  abstract getByActivo(activo): Observable<PresupuestoMarketing[]>;

  abstract post(presupuestoMarketing): Observable<PresupuestoMarketing>;

  abstract getPresupuestoMarketingById(idPresupuestoMarketing): Observable<PresupuestoMarketing>;

  abstract put(idPresupuestoMarketing, presupuestoMarketing): Observable<PresupuestoMarketing>;
}
