import {Observable} from 'rxjs';

export interface CostosCampana {
  id: number;
  idCampana: number;
  idPeriodoTiempo: number;
  idProveedorLead: number;
  idEstadoCosto: number;
  idTipoCosto: number;
  fecha: string;
  fechaRegistro: string;
  cantidad: number;
  activo: number;
  nombre?: number;
  periodo?: number;
  nombreProveedorLeads?: number;
  estado?: number;
  tipo?: number;
}
export abstract class CostosCampanaData {
  abstract get(): Observable<CostosCampana[]>;

  abstract post(costosCampana): Observable<CostosCampana>;

  abstract getCostosCampanaById(idCostosCampana): Observable<CostosCampana>;

  abstract put(idCostosCampana, costosCampana): Observable<CostosCampana>;
}
