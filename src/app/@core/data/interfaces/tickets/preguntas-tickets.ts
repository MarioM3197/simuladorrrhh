import {Observable} from 'rxjs';

export interface PreguntasTickets {
  id: number;
  descripcion: string;
  idMotivoTicket: number;
  idEstadoPregunta: number;
  idRespuestaTickets: number;
  idOpcionTicket: number;
  descripcionPreguntasTickets?: string;
  descripcionMotivosTickets?: string;
  descripcionEstadoPregunta?: string;
  descripcionRespuestaTicket?: string;



}

export abstract  class PreguntasTicketsData {
  abstract get(): Observable<PreguntasTickets[]>;
  abstract getView(): Observable<PreguntasTickets[]>;
  abstract  getByIdMotivoTicket(idMotivoTicket: number): Observable<PreguntasTickets[]>;
  abstract  getByACtivo(activo: number): Observable<PreguntasTickets[]>;
  abstract getById(idPreguntasTicket: number): Observable<PreguntasTickets>;
  abstract post(preguntaticket): Observable<PreguntasTickets>;
  abstract put(idPreguntasTicket, preguntaticket): Observable<PreguntasTickets>;

}
