import {Observable} from "rxjs";

export interface OpcionTicket {
  id: number;
  descripcion: string;
  idSubarea: number;
  activo: number;
}

export abstract class OpcionTicketData {
  abstract get():  Observable<OpcionTicket[]>;
  abstract getById(idOpcionTicket: number): Observable<OpcionTicket>;
  abstract getByActivo(activo: number): Observable<OpcionTicket[]>;
  abstract post(opcionTicket): Observable<OpcionTicket>;
  abstract  put(idOpcionTicket: number, opcionTicket: OpcionTicket): Observable<OpcionTicket>;
}
