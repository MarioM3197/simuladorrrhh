import {Observable} from 'rxjs';

export interface MotivosTickets {
  id: number;
  descripcion: string;
  idOpcionticket: number;
  comentarios: number;
  idPrioridad: number;
  tiempoMinimo: string;
  tiempoMaximo: string;
  activo: number;
  motivosTicketsDescripcion?: string;
  opcionTicketDescripcion?: string;
  prioridadTicketDescripcion?: string;

}

export abstract class MotivosTicketsData {
  abstract get(): Observable<MotivosTickets []>;
  abstract getById(idMotivoTicket: number): Observable<MotivosTickets>;
  abstract getByActivo(activo: number): Observable<MotivosTickets []>;
  abstract getViewByActivo(activo: number): Observable<MotivosTickets[]>;
  abstract getByIdOpcion(id: number): Observable<MotivosTickets>;
  abstract  post(motivoTicket): Observable<MotivosTickets>;
  abstract  put(idMotivoTicket: number, motivoTicket: MotivosTickets): Observable<MotivosTickets>;
}
