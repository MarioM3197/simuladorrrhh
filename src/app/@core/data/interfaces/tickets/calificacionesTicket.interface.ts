import {Observable} from 'rxjs';

export interface CalificacionesTicket {
  id: number;
  descripcion: string;
  nombreCalificacion: string;
  activo: number;
}

export abstract class CalificacionesTicketsData {
  abstract get(): Observable<CalificacionesTicket[]>;
  abstract getById(idCalificacionesTickets: number): Observable<CalificacionesTicket>;
  abstract  getByActivo(activo: number): Observable<CalificacionesTicket[]>;
  abstract  post(calificacionesTickets): Observable<CalificacionesTicket>;
  abstract put(idCalificacionesTickets: number, calificacionTicket: CalificacionesTicket): Observable<CalificacionesTicket>;

}
