import {Observable} from 'rxjs';

export interface EstadoTickets {
  id: number;
  descripcion: string;
  activo: number;
}

export abstract class EstadoTicketsData {
  abstract get(): Observable<EstadoTickets[]>;
  abstract  getById(idEstadoTicekt: number): Observable<EstadoTickets>;
  abstract getByActivo(activo: number): Observable<EstadoTickets[]>;
  abstract post(estadoTicket): Observable<EstadoTickets>;
  abstract  put(idEstadoTicket: number, estadoTicket: EstadoTickets): Observable<EstadoTickets>;
}

