export interface Panel {
  idSocio: number;
  socio: string;
  polizas: number;
  vn: number;
  rn: number;
  sg: number;
  ot: number;
  ec: number;
  total: number;
  endosoPositivo: number;
  endosoNegativo: number;
}
