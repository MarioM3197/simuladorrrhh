export interface PolizasCobradas {
  id: number;
  numPoliza: string;
  inicioVigencia: string;
  fechaPago: string;
  compania: string;
  numeroSerie: string;
  nombreCliente: string;
  prima: number;
  temporalidad: string;
  formaPago: string;
  status: string;
  aplicacionesFinanzas: string;
  ejecutivo: string;
  area: string;
  campana: string;

}
