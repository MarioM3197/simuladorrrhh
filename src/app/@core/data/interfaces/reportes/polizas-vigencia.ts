export interface PolizasVigencia {
  id: number;
  numPòliza: string;
  fechaRegistro: string;
  inicioVigencia: string;
  compania: string;
  numeroSerie: string;
  nombreCliente: string;
  prima: number;
  temporalidad: string;
  formaPago: string;
  status: string;
  aplicacionesFinanzas: string;
  ejecutivo: string;
  area: string;
  campana: string;
}
