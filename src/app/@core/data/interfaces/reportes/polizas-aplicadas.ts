export interface PolizasAplicadas {
  id: number;
  numPoliza: string;
  fechaAplicacion: string;
  inicioVigencia: string;
  compania: string;
  numeroSerie: string;
  nombreCliente: string;
  prima: number;
  temporalidad: string;
  formaPago: string;
  status: string;
  aplicacionesFinanzas: string;
  ejecutivo: string;
  area: string;
  campana: string;

}
