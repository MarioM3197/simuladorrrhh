export interface Vacante {
  id: number;
  idSubarea: number;
  idPuesto: number;
  idTipoPuesto: number;
  nombre: string;
  activo: number;
  aprobadoADP: number;
  aprobadoFinanzas: number;
  aprobadoDireccionOperaciones: number;
  aprobadoDireccionGeneral: number;

  // Datos de la vista
  idArea?: number;
  subarea?: string;
  puesto?: string;
  puestoTipo?: string;
  sede?: string;
  area?: string;
  idSede?: number;
}
