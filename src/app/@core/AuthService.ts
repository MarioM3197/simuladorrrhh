import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(false); // {1}

  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }

  constructor(
    private router: Router
  ) {
    this.loggedIn.next(window.sessionStorage.getItem('logged') ? true : false);
  }

  login(){
    this.loggedIn.next(true);
    window.sessionStorage.setItem('logged', 'chi');
    this.router.navigate(['dashboard']);
  }

  logout() {                            // {4}
    this.loggedIn.next(false);
    window.sessionStorage.clear();
    this.router.navigate(['/login']);
  }
}
